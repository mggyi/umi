const countryOptions = [{
    value: 'Asia',
    label: 'Asia',
    children: [
        {
            value: 'Afghanistan',
            label: 'Afghanistan',
        },
        {
            value: 'Bangladesh',
            label: 'Bangladesh',
        },
        {
            value: 'Bhutan',
            label: 'Bhutan',
        },
        {
            value: 'Brunei',
            label: 'Brunei',
        },
        {
            value: 'Burma',
            label: 'Burma',
        },
        {
            value: 'Cambodia',
            label: 'Cambodia',
        },
        {
            value: 'China',
            label: 'China',
        },
        {
            value: 'East Timor',
            label: 'East Timor',
        },
        {
            value: 'Hong Kong',
            label: 'Hong Kong',
        },
        {
            value: 'India',
            label: 'India',
        },
        {
            value: 'Indonesia',
            label: 'Indonesia',
        },
        {
            value: 'Iran',
            label: 'Iran',
        },
        {
            value: 'Japan',
            label: 'Japan',
        },
        {
            value: 'Korea, North',
            label: 'Korea, North',
        },
        {
            value: 'Korea, South',
            label: 'Korea, South',
        },
        {
            value: 'Laos',
            label: 'Laos',
        },
        {
            value: 'Macau',
            label: 'Macau',
        },
        {
            value: 'Malaysia',
            label: 'Malaysia',
        },
        {
            value: 'Maldives',
            label: 'Maldives',
        },
        {
            value: 'Mongolia',
            label: 'Mongolia',
        },
        {
            value: 'Nepal',
            label: 'Nepal',
        },
        {
            value: 'Pakistan',
            label: 'Pakistan',
        },
        {
            value: 'Philippines',
            label: 'Philippines',
        },
        {
            value: 'Singapore',
            label: 'Singapore',
        },
        {
            value: 'Sri Lanka',
            label: 'Sri Lanka',
        },
        {
            value: 'Taiwan',
            label: 'Taiwan',
        },
        {
            value: 'Thailand',
            label: 'Thailand',
        },
        {
            value: 'Vietnam',
            label: 'Vietnam',
        },
    ],
  },

  ];


  export default countryOptions;