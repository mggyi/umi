import request from '../utils/request';
import {GetToken} from '../utils/authority';
import api from '../api'

export function query() {
  return request(`${api}/users`);
}

// export async function queryCurrent(params) {
//   var body = {
//     "token": params.token,
//   }
//   var obj = {
//     method: 'POST',
//     headers: {
//       'Content-Type': 'application/json',
//       'Origin': '',
//     },
//     body:JSON.stringify(body)
//   }
//   return request(`${api}/currentUser`,obj);
// }

export function login(params) {

  var body = {
    "email" : params.email,
    "password" : params.password,
  }

  var obj = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Origin': '',
    },
    body:JSON.stringify(body)
  }
	return request(`${api}/usr/login`,obj);
}


export function register(params) {
  var body = {
    "userName": params.userName,
    "displayName": params.displayName,
    "email" : params.email,
    "password" : params.password,
    "clientId" : params.clientId,
  }
  
  var obj = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Origin': '',
    },
    body:JSON.stringify(body)
  }
  return request(`${api}/usr/register`,obj);
}

export function GoogleSign(params){
  var obj = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/octet-stream; charset=utf-8',
      'Origin': '',
    },
    body:`idtoken=${params.id_token}`
  }

  return request(`${api}/tokensignin`, obj);
}

export function Request_oauth_token(params) {
  var obj = {
    method: 'POST',
    mode: 'no-cors',
    headers: {
      'Content-Type':'application/x-www-form-urlencoded',
      'Access-Control-Allow-Origin' : "no",
      'Authorization': 'OAuth oauth_consumer_key="oQTdAkhJTogQGbFyr322RKdqi",oauth_signature_method="HMAC-SHA1",oauth_timestamp="1530858000",oauth_nonce="1zA1iP",oauth_version="1.0",oauth_signature="X2LX%2FtXJ91EBrDZSjczcrGDSS3Y%3D"',
    },
    body: `oauth_callback=${params.oauth_callback}`
  }
  
  return request('https://api.twitter.com/oauth/request_token',obj)
}

export function TwitterLogin(params) {
  var body = {
    "oauth_token": params.oauth_token,
    "oauth_verifier": params.oauth_verifier,
  }
  
  var obj = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Origin': '',
    },
    body:JSON.stringify(body)
  }
  return request(`${api}/usr/twitterLogin`,obj);
}

export function SendGoogleIdToken(params) {
  var body = {
    "idtoken": params.id_token,
  }
  var obj = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Origin': '',
    },
    body:JSON.stringify(body)
  }

  return request(`${api}/tokensignin`, obj);
}

export function SendFacebookIdToken(params) {
  var body= {
    "accessToken": params.idtoken,
    "userID": params.userId,
  }
  var obj = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Origin': '',
    },
    body:JSON.stringify(body)
  }
  return request(`${api}/fbsignin`,obj)
}

export function userNameCheck (params) {
  var body = {
    "userName": params.userName,
  }
  var obj = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Origin': '',
    },
    body: JSON.stringify(body)
  }
  return request(`${api}/userNameCheck`,obj)
}

export function queryCurrent() {
    var token = GetToken()
    
    var obj = {
        method: 'GET',
        headers: {
          'Authorization':`token ${token}`
        }
    }
    
    return request(`${api}/currentUser`,obj)
}