import RenderAuthorized from 'components/Authorized';
import { getAuthority } from './authority';
import { routerRedux } from 'dva/router';

let Authorized = RenderAuthorized(getAuthority()); // eslint-disable-line

// Reload the rights component
const reloadAuthorized = () => {
  Authorized = RenderAuthorized(getAuthority());
};

Authorized.grant = (authority) => {
  if (!Authorized.check(authority, true, false) ) {
    routerRedux.push('/403');
  }
}

export { reloadAuthorized };
export default Authorized;
