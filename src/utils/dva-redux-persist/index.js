import {persistReducer, persistStore} from "redux-persist";
import localForage from "localforage";
import createEncryptor from 'redux-persist-transform-encrypt'
import createCompressor from 'redux-persist-transform-compress'


const compressor = createCompressor()
const key = 'global';
const encryptor = createEncryptor({
  secretKey: 'my-super-secret-key',
  onError: function(error) {
    // Handle the error.
  }
})
export function storageEnhancer(opts = { key, storage:localForage , transforms: [encryptor,compressor]}) {
  return createStore => (reducer, initialState, enhancer) => {
    const store = createStore(persistReducer(opts, reducer), initialState, enhancer);
    const persistor = persistStore(store);
    return {...store, persistor };
  }
}