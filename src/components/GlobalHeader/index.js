import React, { PureComponent } from 'react';
import { Button,Menu, Icon, Spin, Tag, Dropdown, Avatar, Divider } from 'antd';
import moment from 'moment';
import groupBy from 'lodash/groupBy';
import Link from 'umi/link';
import router  from 'umi/router';
import styles from './index.less';
import HeaderSearch from '../HeaderSearch';
import NoticeIcon from '../NoticeIcon';
import {FormattedMessage } from 'react-intl';
import messages from '../../translations/message';




export default class GlobalHeader extends PureComponent {

  getNoticeData() {
    const { notices = [] } = this.props;
    if (notices.length === 0) {
      return {};
    }
    const newNotices = notices.map((notice) => {
      const newNotice = { ...notice };
      if (newNotice.datetime) {
        newNotice.datetime = moment(notice.datetime).fromNow();
      }
      // transform id to item key
      if (newNotice.id) {
        newNotice.key = newNotice.id;
      }
      if (newNotice.extra && newNotice.status) {
        const color = ({
          todo: '',
          processing: 'blue',
          urgent: 'red',
          doing: 'gold',
        })[newNotice.status];
        newNotice.extra = <Tag color={color} style={{ marginRight: 0 }}>{newNotice.extra}</Tag>;
      }
      return newNotice;
    });
    return groupBy(newNotices, 'type');
  }




	render() {
		
		const {
			isLogin,
	      currentUser,  fetchingNotices, isMobile, logo,
	      onNoticeVisibleChange, onMenuClick, onNoticeClear,
	    } = this.props;

	    const noticeData = this.getNoticeData();
	    const menu = (
	      <Menu  className={styles.menu}  onClick={onMenuClick} defaultSelectedKeys={['1']}>
					<Menu.Item  key="newtopic" ><Icon type="solution" />
	        	new topic
	        </Menu.Item >
					<Menu.Item  key="newquestion" ><Icon type="question-circle-o" />
	        	new question
	        </Menu.Item >
					<Menu.Divider />
					<Menu.Item  key="topics" ><Icon type="solution" />
	        	my topics
	        </Menu.Item >
					<Menu.Item  key="questions" ><Icon type="question-circle-o" />
	        	my questions
	        </Menu.Item >
					<Menu.Divider />
	        <Menu.Item  key="profile" ><Icon type="user" />
	        	profile
	        </Menu.Item >
	        <Menu.Item key="setting"><Icon type="setting" />
						settings
					</Menu.Item>

	        <Menu.Divider />
	        	<Menu.Item key="logout">
							<Icon type="logout" />
							sign out
						</Menu.Item>
	      </Menu>


			);
			
		return (
	      <div className={styles.header}>
	        {isMobile && (
	          [
	            (
	              <Link to="/" className={styles.logo} key="logo">
	                <img src={logo} alt="logo" width="32" />
	              </Link>
	            ),
	          ]
	        )}
	        {!isMobile && (
	          [
	            (
	              <Link to="/" className={styles.logo} key="logo">
	                <img src={logo} alt="logo" width="32" />
	                <Divider type="vertical" key="line" />
	                <div style={{
											height:'100%',display:'inline-block',background:'#1890ff',color:'white'
										}}><span style={{
												paddingLeft:13,paddingRight:13,font:'14px arial, sans-serif'
											}}> ANDROID </span> </div>
	              </Link>

	            ),
	          ]
	        )}

	        <div className={styles.right}>
	        <HeaderSearch
	            className={`${styles.action} ${styles.search}`}
	            placeholder="Search"
	            dataSource={['topic:', 'ask:', 'users:','tags:']}
	            onSearch={(value) => {
	            }}
	            onPressEnter={(value) => {
	              alert(value)
	            }}
	        />
					
				
					
					{isLogin?(
						<>
	        	<NoticeIcon
		            className={`${styles.action} ${styles.account}`}

		            count={currentUser.notifyCount}
		            onItemClick={(item, tabProps) => {
		              
		            }}
		            onClear={onNoticeClear}
		            onPopupVisibleChange={onNoticeVisibleChange}
		            loading={fetchingNotices}
		            popupAlign={{ offset: [20, -16] }}
		          >
		            <NoticeIcon.Tab
		              list={noticeData['通知']}
		              title="通知"
		              emptyText="你已查看所有通知"
		              emptyImage="https://gw.alipayobjects.com/zos/rmsportal/wAhyIChODzsoKIOBHcBk.svg"
		            />
		            <NoticeIcon.Tab
		              list={noticeData['消息']}
		              title="消息"
		              emptyText="您已读完所有消息"
		              emptyImage="https://gw.alipayobjects.com/zos/rmsportal/sAuJeJzSKbUmHfBQRzmZ.svg"
		            />
		            <NoticeIcon.Tab
		              list={noticeData['待办']}
		              title="待办"
		              emptyText="你已完成所有待办"
		              emptyImage="https://gw.alipayobjects.com/zos/rmsportal/HsIsxMZiWKrNUavQUXqx.svg"
		            />
						</NoticeIcon>

						{currentUser.name ? (
							
		            <Dropdown overlay={menu}>
										<span className={`${styles.action} ${styles.account}`}>
										<Avatar shape="square" size="small" className={styles.avatar} src={currentUser.avatar} />
										<span className={styles.name}>{currentUser.name}</span>
										</span>
		            </Dropdown>
		          	) : <Spin size="small" style={{ marginLeft: 8 }} />}
						</>
	        	):(
							<Button size="small" onClick={()=>{ router.push('/profile');}}>Login</Button>
	        	)
	        }

	        </div>
	      </div>
		)
	}
}

	            //   <span className={`${styles.action} ${styles.account}`}>
		            //     <Avatar shape="square" size="small" className={styles.avatar} src={currentUser.avatar} />
		            //     <span className={styles.name}>{currentUser.name}</span>
		            //   </span>