import {Menu} from 'antd';
import {
 
    FacebookShareButton,
    GooglePlusShareButton,
    LinkedinShareButton,
    TwitterShareButton,
    PinterestShareButton,
    VKShareButton,
    OKShareButton,
    TelegramShareButton,
    WhatsappShareButton,
    RedditShareButton,
    EmailShareButton,
    TumblrShareButton,
    LivejournalShareButton,
    MailruShareButton,
    ViberShareButton,
  
    FacebookIcon,
    TwitterIcon,
    GooglePlusIcon,
    LinkedinIcon,
    PinterestIcon,
    VKIcon,
    OKIcon,
    TelegramIcon,
    WhatsappIcon,
    RedditIcon,
    TumblrIcon,
    MailruIcon,
    EmailIcon,
    LivejournalIcon,
    ViberIcon,
} from 'react-share';
import Media from "react-media";
import styles from './index.less'
// import exampleImage from '../../assets/profile.jpeg'
export default (props) => {
    const {shareUrl,title} = props;
    // const shareUrl = 'http://github.com';
    // const title = 'GitHub';
    return (
      
            <Menu >
              <Menu.Item>
                <FacebookShareButton
                    url={shareUrl}
                    quote={title}
                    // className="Demo__some-network__share-button"
                    >
                    <FacebookIcon
                    size={32}
                    round />
                </FacebookShareButton>
              </Menu.Item>
              <Menu.Item>
                <TwitterShareButton
                    url={shareUrl}
                    title={title}
                    className="Demo__some-network__share-button">
                    <TwitterIcon
                    size={32}
                    round />
                </TwitterShareButton>
              </Menu.Item>
              <Menu.Item>
                <TelegramShareButton
                    url={shareUrl}
                    title={title}
                    className="Demo__some-network__share-button">
                    <TelegramIcon size={32} round />
                </TelegramShareButton>
              </Menu.Item>
              <Menu.Item>
                <WhatsappShareButton
                    url={shareUrl}
                    title={title}
                    separator=":: "
                    className="Demo__some-network__share-button">
                    <WhatsappIcon size={32} round />
                </WhatsappShareButton>
              </Menu.Item>
              <Menu.Item>
                <GooglePlusShareButton
                    url={shareUrl}
                    className="Demo__some-network__share-button">
                    <GooglePlusIcon
                    size={32}
                    round />
                </GooglePlusShareButton>
              </Menu.Item>
                
              <Menu.Item>
                <LinkedinShareButton
                    url={shareUrl}
                    title={title}
                    windowWidth={750}
                    windowHeight={600}
                    className="Demo__some-network__share-button">
                    <LinkedinIcon
                    size={32}
                    round />
                </LinkedinShareButton>
              </Menu.Item>
                
              <Menu.Item>
                <PinterestShareButton
                    url={String(window.location)}
                    // media={`${String(window.location)}/${exampleImage}`}
                    windowWidth={1000}
                    windowHeight={730}
                    className="Demo__some-network__share-button">
                    <PinterestIcon size={32} round />
                </PinterestShareButton>
            </Menu.Item>

            <Menu.Item>
                <VKShareButton
                    url={shareUrl}
                    // image={`${String(window.location)}/${exampleImage}`}
                    windowWidth={660}
                    windowHeight={460}
                    className="Demo__some-network__share-button">
                    <VKIcon
                    size={32}
                    round />
                </VKShareButton>
            </Menu.Item>
            <Menu.Item>
                <OKShareButton
                    url={shareUrl}
                    // image={`${String(window.location)}/${exampleImage}`}
                    windowWidth={660}
                    windowHeight={460}
                    className="Demo__some-network__share-button">
                    <OKIcon
                    size={32}
                    round />
                </OKShareButton>
            </Menu.Item>

            <Menu.Item>
                <RedditShareButton
                    url={shareUrl}
                    title={title}
                    windowWidth={660}
                    windowHeight={460}
                    className="Demo__some-network__share-button">
                    <RedditIcon
                    size={32}
                    round />
                </RedditShareButton>
            </Menu.Item>

            <Menu.Item>
                <TumblrShareButton
                    url={shareUrl}
                    title={title}
                    windowWidth={660}
                    windowHeight={460}
                    className="Demo__some-network__share-button">
                    <TumblrIcon
                    size={32}
                    round />
                </TumblrShareButton>
            </Menu.Item>

            <Menu.Item>
                <LivejournalShareButton
                    url={shareUrl}
                    title={title}
                    description={shareUrl}
                    className="Demo__some-network__share-button"
                >
                    <LivejournalIcon size={32} round />
                </LivejournalShareButton>
            </Menu.Item>

            <Menu.Item>
                <MailruShareButton
                    url={shareUrl}
                    title={title}
                    className="Demo__some-network__share-button">
                    <MailruIcon
                    size={32}
                    round />
                </MailruShareButton>
            </Menu.Item>

            <Menu.Item>
                <EmailShareButton
                    url={shareUrl}
                    subject={title}
                    body="body"
                    className="Demo__some-network__share-button">
                    <EmailIcon
                    size={32}
                    round />
                </EmailShareButton>
            </Menu.Item>
            <Menu.Item>
                <ViberShareButton
                    url={shareUrl}
                    title={title}
                    body="body"
                    className="Demo__some-network__share-button">
                    <ViberIcon
                    size={32}
                    round />
                </ViberShareButton>
              </Menu.Item>
            </Menu>
    


                

                

                
                           
   


    )
}