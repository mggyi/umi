import Helmet from 'react-helmet';


/*
https://support.google.com/webmasters/answer/79812?hl=en
*/
function Social(props){
    return(
    <Helmet>
            <title>{props.title}</title>
            <meta name="Description" CONTENT={props.description}></meta>
            <meta property="og:url"          content={props.url} />
            <meta property="og:type"               content="article" />
            <meta property="og:title"              content={props.title} />
            <meta property="og:description"        content={props.description}/>
            <meta property="og:image"              content={props.image} />
            <meta name="twitter:card" content="summary" />
        </Helmet>
    )
}

export default Social;