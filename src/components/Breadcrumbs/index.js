import NavLink from 'umi/navlink';
import {Card} from 'antd';
import withBreadcrumbs from 'react-router-breadcrumbs-hoc';
const routes = [
    
    { path: '/', breadcrumb: 'Home'},
    { path: '/home', breadcrumb: null},
    { path: '/home/ask', breadcrumb: 'Question'},
    { path: '/home/reviews', breadcrumb: 'Reviews'},
    { path: '/home/unanswered', breadcrumb: 'Un Answered'},
    { path: '/news', breadcrumb: 'News' },
    { path: '/tags', breadcrumb: 'Tags' },
    { path: '/users', breadcrumb: 'User List' },
    { path: '/profile', breadcrumb: 'Profile' },
    { path: '/chgpswd', breadcrumb: 'Change Password' },
    { path: '/users/:id', breadcrumb: 'User Detail' },
    { path: '/topic/:id', breadcrumb: 'Topic Detail'},
    { path: '/edit-topic', breadcrumb: null},
    { path: '/edit-topic/:id', breadcrumb: 'Topic Edit Page'},
    { path: '/newtopic', breadcrumb: 'New Topic'},
  ];
  
  export default withBreadcrumbs(routes)(({ breadcrumbs }) => ( 
    
    <div style={{paddingLeft:'15px'}}>
      {breadcrumbs.map((breadcrumb, index) => (
        <span key={breadcrumb.key}>
          <NavLink to={breadcrumb.props.match.url}>
            {breadcrumb}
          </NavLink>
          {(index < breadcrumbs.length - 1) && <i > / </i>}
        </span>
      ))}
    </div>
    
    
  ));