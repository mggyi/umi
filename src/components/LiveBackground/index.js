import TweenOne from 'rc-tween-one';
function LiveBackground() {
    return (
        <svg style={{position:'absolute',right:1}}  viewBox="0 0 1200 800">
          <TweenOne
            component="circle"
            fill="rgba(161,174,245,.15)"
            r="130"
            cx="350"
            cy="350"
            animation={{
              y: 30, x: -10, repeat: -1, duration: 3000, yoyo: true,
            }}
          />
          <TweenOne
            component="circle"
            fill="rgba(161,174,245,.15)"
            r="60"
            cx="200"
            cy="500"
            animation={{
              y: 30, x: -10, repeat: -1, duration: 3000, yoyo: true,
            }}
          />
                    <TweenOne
            component="circle"
            fill="rgba(161,174,245,.15)"
            r="20"
            cx="300"
            cy="600"
            animation={{
              y: 30, x: -10, repeat: -1, duration: 3000, yoyo: true,
            }}
          />
                    <TweenOne
            component="circle"
            fill="rgba(161,174,245,.15)"
            r="60"
            cx="300"
            cy="670"
            animation={{
              y: 30, x: -10, repeat: -1, duration: 3000, yoyo: true,
            }}
          />
                    <TweenOne
            component="circle"
            fill="rgba(161,174,245,.15)"
            r="60"
            cx="1050"
            cy="100"
            animation={{
              y: 30, x: -10, repeat: -1, duration: 3000, yoyo: true,
            }}
          />
                    <TweenOne
            component="circle"
            fill="rgba(161,174,245,.15)"
            r="20"
            cx="1130"
            cy="100"
            animation={{
              y: 30, x: -10, repeat: -1, duration: 3000, yoyo: true,
            }}
          />
          <TweenOne
            component="circle"
            fill="rgba(120,172,254,.1)"
            r="80"
            cx="500"
            cy="420"
            animation={{
              y: -30, x: 10, repeat: -1, duration: 3000, yoyo: true,
            }}
          />
          <TweenOne
            component="circle"
            fill="rgba(161,174,245,.15)"
            r="100"
            cx="200"
            cy="200"
            animation={{
              y: 30, x: -10, repeat: -1, duration: 3000, yoyo: true,
            }}
          />
          <TweenOne
            component="circle"
            fill="rgba(120,172,254,.1)"
            r="30"
            cx="800"
            cy="420"
            animation={{
              y: -30, x: 10, repeat: -1, duration: 3000, yoyo: true,
            }}
          />
          <TweenOne
            component="circle"
            fill="rgba(120,172,254,.1)"
            r="70"
            cx="900"
            cy="250"
            animation={{
              y: -30, x: 10, repeat: -1, duration: 3000, yoyo: true,
            }}
          />
          <TweenOne
            component="circle"
            fill="rgba(120,172,254,.1)"
            r="150"
            cx="950"
            cy="480"
            animation={{
              y: -30, x: 10, repeat: -1, duration: 3000, yoyo: true,
            }}
          />
          <TweenOne
            component="circle"
            fill="rgba(120,172,254,.1)"
            r="40"
            cx="100"
            cy="480"
            animation={{
              y: -30, x: 10, repeat: -1, duration: 3000, yoyo: true,
            }}
          />
          <TweenOne
            component="circle"
            fill="rgba(120,172,254,.3)"
            r="50"
            cx="350"
            cy="100"
            animation={{
              y: -30, x: 10, repeat: -1, duration: 3000, yoyo: true,
            }}
          />
                              <TweenOne
            component="circle"
            fill="rgba(120,172,254,.3)"
            r="15"
            cx="450"
            cy="100"
            animation={{
              y: -30, x: 10, repeat: -1, duration: 3000, yoyo: true,
            }}
          />
        </svg>
    )
}

export default LiveBackground;