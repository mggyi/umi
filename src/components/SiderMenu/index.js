import 'rc-drawer-menu/assets/index.css';
import React ,{PureComponent}from 'react';
import { Layout, Menu, Icon ,Divider} from 'antd';
import DrawerMenu from 'rc-drawer-menu/lib';
// import SiderMenu from './SiderMenu';
import Link from 'umi/link';
import {menu} from '../../common/menu.json'
import styles from './index.less'
const { Sider } = Layout;
const { SubMenu } = Menu;



const MenuList = menu.map((menuItem,i) =>{
  if(menuItem.children === undefined || menuItem.children.length === 0){
    return(
    <Menu.Item  key={menuItem.key.toString()}>
      
       <Link to={menuItem.path}>
       <Icon style={{ 
        //  fontSize: 16, 
        //  color: '#1a73e8' ,fontWeight:'500',
         }}  type={menuItem.icon}/>
            <span 
            className="menu"
            // style={{
              // color: '#1a73e8' ,fontWeight:'500',
            // fontSize:'14px',fontWeight:'500',lineHeight:'19.6px'
          // }}
            >{menuItem.name}</span>
        </Link>
    </Menu.Item>
    )
  }else{
    return(
        <SubMenu key={menuItem.key.toString()}  title={
          <span  
          className="menu"
          // style={{
            // color: '#1a73e8' ,fontWeight:'500',
          // fontSize:'14px',fontWeight:'500',lineHeight:'19.6px'
          // }}
          >
            <Icon  style={{ 
              // fontSize: 16,
              //  color: '#1a73e8' ,fontWeight:'500',
               }}  type={menuItem.icon} />
              {menuItem.name}
          </span>
        }>
                {menuItem.children.map(p =>
                
                    <Menu.Item  key={p.key.toString()} >
                      <Link  to={p.path}>
                        <span  
                        className="menu"
                        // style={{
                          // color: '#1a73e8',fontWeight:'500',
                        // fontSize:'14px',fontWeight:'500',lineHeight:'19.6px'
                        // }}
                        >{p.name}</span>
                        </Link>
                    </Menu.Item >
                )}
        </SubMenu>
      );
    }
  }
)

export default class SiderMenu extends PureComponent {
  
    defaultOpenKeys = (pathname) => {
      var pathArr = []
      var path = pathname.split("/")
      if(path.length > 2){
        path ='/' + path[1]
        pathArr.push(path)
        pathArr.push(pathname)
        return pathArr
      }else{
        pathArr.push(pathname)
        return pathArr
      }

    }
    render() {
      const {logo,pathname} = this.props;
      console.log("SiderMenu",this.props)
      console.log("pathname", pathname)
      var dOKeys = this.defaultOpenKeys(pathname)
      console.log('Selected Key',dOKeys)
      if(this.props.isMobile === true) {
        return (
          <DrawerMenu
              level={null} >
               <Link to="/" className={styles.logo} key="logo">
                  <img src={logo} alt="logo" width="32" />
                  <Divider type="vertical" key="line" />
                  <span><b> Ant Design Pro </b> </span>
                </Link>
             <Menu
                style={{ width: 240 }}
                // defaultSelectedKeys={['1']}
                // defaultOpenKeys={['sub1']}
                defaultOpenKeys={dOKeys}
                selectedKeys={dOKeys}
                mode="inline"
              >
                {MenuList}
              </Menu>
            </DrawerMenu>
        )
      }else{
        return (
          <Sider 
            style={{background:'white',borderRight:'2px' , marginTop:'80px',overflow: 'auto', height: '100vh', position: 'fixed', left: 0 }} 
          >
            <Menu
                style={{height:"100vh"}}
                key="Menu"
                mode="inline"
                theme="white"
                // defaultSelectedKeys={['1']}
                defaultOpenKeys={dOKeys}
                selectedKeys={dOKeys}
                
            >
                {MenuList}
            </Menu>
          </Sider>
        )
      }
    }
}
