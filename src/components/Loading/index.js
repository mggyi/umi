
import {Spin} from 'antd'
import styles from './loading.less'
export default () => {
    return (
        <div className={styles.div}>
            <Spin size="large" />
        </div>
    )
}
