import {fetchTaglist,fetchTagSearch} from '../services/tags'
// import { routerRedux } from 'dva/router';
export default {

    namespace: 'tags',
  
    state: {
        tags : [],
        count: 0,
    },
    effects: {
        *fetchTaglist({payload},{call,put}){
            const response = yield call(fetchTaglist,payload)
            yield put({ 
                type: 'save',
                payload: {
                    res: response.data,
                }
            });
        },
        *fetchTagSearch({payload},{call,put}){
            const response = yield call(fetchTagSearch,payload)
            yield put({ 
                type: 'save',
                payload: {
                    res: response.data,
                }
            });
        },
    },
  
    reducers: {
      save(state, {payload}) {
        return { ...state,
            count: payload.res.count,
            tags : payload.res.tagList,
        };
      },
    },
  
};
  