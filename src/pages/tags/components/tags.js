import React from 'react'
import {Radio,Row,Col,Pagination, List, Card ,Spin,Tag,Input} from 'antd';
import { connect } from 'dva';
import moment from 'moment';
import { routerRedux } from 'dva/router';
import styles from './tags.less'
import {Helmet} from "react-helmet";
// import PageHeaderLayout from '../../../layouts/PageHeaderLayout';
const { Meta } = Card;
const RadioButton = Radio.Button;
const RadioGroup = Radio.Group;
const { Search } = Input;

@connect(({ tags,loading}) => ({
  tags,
  loading: loading.effects['tags/fetchTaglist'],
}))
export default class TagList extends React.Component {
    constructor(props) {
      super(props)
      this.state = {
        selectedRadio: '1',
        current: 1,
      }
    }

    paginatiOnChange = (e) =>{
      console.log(e);
      this.setState({
        current: e,
      });
      console.log("paginatiOnChange",e-1,this.state.selectedRadio)
      this.props.dispatch({
        type: 'tags/fetchTaglist',
        payload: {
          query: e-1,
          type: this.state.selectedRadio,
        }
      })
    }

    cardOnClick = (name) => {
      this.props.dispatch(routerRedux.push(`home/topic?tag=${name}`));
    }
    
    componentWillMount(){
      this.props.dispatch({
        type: 'tags/fetchTaglist',
        payload: {
          query: 0,
          type: 1,
        }
      })
    }

    fetch = (k) => {
      this.props.dispatch({
        type: 'tags/fetchTaglist',
        payload: {
          query: 0,
          type: k,
        }
      })
    }
    fetchSearch = (k) => {
      this.props.dispatch({
        type: 'tags/fetchTagSearch',
        payload: {
          keyword: k,
        }
      })
    }
    readioChange = (e) => {
      var k = e.target.value
      this.setState({
        selectedRadio: k,
        current: 1,
      })
      this.fetch(k)
    }
    onSearch = (e) => {
      this.fetchSearch(e)
    }
    render() {
      const {loading, tags:{count,tags}} = this.props;
      console.log(tags,loading)
      const extraContent = (
        <div className={styles.extraContent}>
          <RadioGroup buttonStyle="solid" style={{paddingBottom:10}} defaultValue="1" onChange={this.readioChange}>
            <RadioButton value="1">Latest</RadioButton>
            <RadioButton value="2">Most</RadioButton>
            <RadioButton value="3">A-Z</RadioButton>
          </RadioGroup>
          <Search
            className={styles.extraContentSearch}
            placeholder="Search"
            onSearch= {this.onSearch}
          />
        </div>
      );

        return (
          <div className={styles.normal}>
           
                <Helmet>
                  <meta property="og:url"          content="http://bits.blogs.nytimes.com/2011/12/08/a-twitter-for-my-sister/" />
                  <meta property="og:type"               content="article" />
                  <meta property="og:title"              content="A Twitter for My Sister" />
                  <meta property="og:description"        content="In the early days, Twitter grew so quickly that it was almost impossible to add new features because engineers spent their time trying to keep the rocket ship from stalling." />
                  <meta property="og:image"              content="http://graphics8.nytimes.com/images/2011/12/08/technology/bits-newtwitter/bits-newtwitter-tmagArticle.jpg" />

                  <meta name="twitter:card" content="summary" />
                  <meta name="twitter:site" content="@nytimesbits" />
                  <meta name="twitter:creator" content="@nickbilton" />
                  <meta property="og:url" content="http://bits.blogs.nytimes.com/2011/12/08/a-twitter-for-my-sister/" />
                  <meta property="og:title" content="A Twitter for My Sister" />
                  <meta property="og:description" content="In the early days, Twitter grew so quickly that it was almost impossible to add new features because engineers spent their time trying to keep the rocket ship from stalling." />
                  <meta property="og:image" content="http://graphics8.nytimes.com/images/2011/12/08/technology/bits-newtwitter/bits-newtwitter-tmagArticle.jpg" />
                </Helmet>
        
              <Card
                className={styles.listCard}
                bordered={false}
                title="Tag List"
                style={{ marginTop: 24 }}
                bodyStyle={{ padding: '0 32px 40px 32px' }}
                extra={extraContent}
              >
              <div style={{paddingTop:40}}>
              {loading? 
               [
                 <div style={{textAlign: 'center'}}>
                     <Spin size="large" />
                 </div>
               ]:[
                  <List
                    dataSource={tags}
                    grid={{ gutter: 16, xs: 1, sm: 2, md: 3, lg: 4, xl: 5, xxl: 6 }}
                    size="large"
                    renderItem={item => (
                        <List.Item key={item.uuid}>
                            <Card hoverable onClick={(e) => this.cardOnClick(item.Name)}>
                            <Meta
                                title={<><Tag>{item.Name}</Tag><a>{item.Count}</a></>}
                                description={<><em>{moment(item.CreateAt).format('YYYY-MM-DD HH:mm')}</em></>}
                                />
                            </Card>
                        </List.Item>
                    )}
                  />
                ]
                }
                </div>
            </Card>

              <Row type="flex" align="center">
                <Col>
                  {/* <Pagination  defaultCurrent={1} pageSize={10}   total={count} onChange={this.paginatiOnChange}  /> */}
                  <Pagination current={this.state.current} onChange={this.paginatiOnChange} total={count} />;
                </Col>
              </Row>
          </div>
         )
    }
}



