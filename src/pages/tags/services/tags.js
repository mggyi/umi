import request from '../../../utils/request';
// import {GetToken} from '../../../utils/authority';
import api from '../../../api'
export function fetchTaglist(params) {
    var obj = {
        method: 'GET',
    }
    return request(`${api}/taglist/${params.query}/${params.type}`,obj)
}

export function fetchTagSearch(params) {
    var obj = {
        method: 'GET',
    }
    return request(`${api}/tagsearch/0/${params.keyword}`,obj)
}
// export function updateProfile(params) {
//     var token = GetToken()

//     var body = {
//         "userid": params.userid,
//         "avatar": params.avatar,
//         "displayName": params.displayName,
//         "email": params.email,
//         "birthday": params.birthday,
//         "gender": params.gender,
//         "country": params.country,
//         "bio": params.bio,
//     }
//     var obj = {  
//       method: 'POST',
//       headers: {
//         'Content-Type': 'application/json',
//         'Origin': '',
//         'Authorization':`token ${token}`
//       },
//       body:JSON.stringify(body)
//     }
  
//     return request(`${api}/updateProfile`, obj);
// }