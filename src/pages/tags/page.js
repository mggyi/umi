// function FuncyBorder(props) {
//   return (
//     <div className={'FuncyBorder FuncyBorder-' + props.color}>
//       {props.children}
//     </div>
//   )
// }

// function Dialog(props) {
//   return (
//     <FuncyBorder color="blue">
//       <h1>
//         {props.title}
//       </h1>
//       <p>
//         {props.message}
//       </p>
//     </FuncyBorder>
//   )
// }
// function WelcomeDialog() {
//   return (
//     <Dialog
//       title="Welcome"
//       message="Thank you for visiting our spacecraft!"/>

//   )
// }

// export default WelcomeDialog;
// import React from 'react'
// class CustomTextInput extends React.Component {
//   constructor(props) {
//     super(props);

//     this.textInput = null;

//     this.setTextInputRef = element => {
//       this.textInput = element;
//     };

//     this.focusTextInput = () => {
//       // Focus the text input using the raw DOM API
//       if (this.textInput) this.textInput.focus();
//     };
//   }

//   componentDidMount() {
//     // autofocus the input on mount
//     this.focusTextInput();
//   }

//   render() {
//     // Use the `ref` callback to store a reference to the text input DOM
//     // element in an instance field (for example, this.textInput).
//     return (
//       <div>
//         <input
//           type="text"
//           ref={this.setTextInputRef}
//         />
//         <input
//           type="button"
//           value="Focus the text input"
//           onClick={this.focusTextInput}
//         />
//       </div>
//     );
//   }
// }
// export default CustomTextInput

// import React from 'react';
// import { connect } from 'dva';
// import styles from './index.css';

// function IndexPage() {
//   console.log("tags")
//   return (
//     <div className={styles.normal}>
//       <h1 className={styles.title}>Hello from Tags</h1>
//       <div className={styles.welcome} />
//       <ul className={styles.list}>
//         <li>To get started, edit <code>src/index.js</code> and save to reload.</li>
//         <li><a href="https://github.com/dvajs/dva">Getting Started</a></li>
//       </ul>
//     </div>
//   );
// }

// IndexPage.propTypes = {
// };

// export default connect()(IndexPage);
import Tag from './components/tags'
export default Tag;

