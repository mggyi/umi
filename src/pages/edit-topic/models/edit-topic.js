import {updateTopic} from '../services/edit-topic'
import { routerRedux } from 'dva/router';
import { message } from 'antd';

export default {

    namespace: 'edit-topic',
  
    state: {},
    effects: {
        *updateTopic({payload},{call,put}) {
            const response = yield call(updateTopic,payload)
            
            if(response.data.message !== "ok"){
                message.error(response.data.message);
            }else{
                message.success("Successfully Updated!");
                yield put(routerRedux.push(`/topic/${response.data.topicId}`));
            }
        },
    },
    reducers: {},
};
  