// import {Component} from 'react';
// export default class Demo extends Component {

//     render() {
//         return (
//             <h1>{this.props.match.params.id}</h1>
//         )
//     }
// }

import React from 'react'
import PropTypes from 'prop-types';
import {Popover,Input,Icon,Affix,BackTop ,Row,Col,Spin,Avatar,Divider,Select, Card,Button,Tag,Popconfirm,message,Menu,Dropdown} from 'antd';
import Social from '../../../components/Social';
import Loading from '../../../components/Loading';
import Shimmer from 'react-js-loading-shimmer';
import styles from './index.css'
import moment from 'moment';
import { connect } from 'dva';
import { routerRedux } from 'dva/router';
import Share from '../../../components/Share'
// import Comment from './Comment'
import ContentLoader, { Facebook } from 'react-content-loader';
import {fetchTopicById} from '../../topic/services/topic'
import loadingif from '../../../assets/loading.gif'
import {
  EditorState,
  convertToRaw,
  convertFromRaw,
  KeyBindingUtil,
  Modifier,
  AtomicBlockUtils,
} from 'draft-js';
import {
  Editor,
  StringToTypeMap,
  Block,
  keyBindingFn,
  createEditorState,
  addNewBlock,
  addNewBlockAt,
  beforeInput,
  getCurrentBlock,
  ImageSideButton,
  rendererFn,
  HANDLED,
  NOT_HANDLED
} from 'medium-draft';
import {
  setRenderOptions,
  blockToHTML,
  entityToHTML,
  styleToHTML
  } from 'medium-draft/lib/exporter';
import 'draft-js/dist/Draft.css';
import 'medium-draft/lib/index.css';
// import Like from './Like'
const Option = Select.Option;

const newTypeMap = StringToTypeMap;
newTypeMap['2.'] = Block.OL;


const { hasCommandModifier } = KeyBindingUtil;

/*
A demo for example editor. (Feature not built into medium-draft as too specific.)
Convert quotes to curly quotes.
*/
const DQUOTE_START = '“';
const DQUOTE_END = '”';
const SQUOTE_START = '‘';
const SQUOTE_END = '’';

const newBlockToHTML = (block) => {
  const blockType = block.type;
  if (block.type === Block.ATOMIC) {
    if (block.text === 'E') {
      return {
        start: '<figure class="md-block-atomic md-block-atomic-embed">',
        end: '</figure>',
      };
    } else if (block.text === '-') {
      return <div className="md-block-atomic md-block-atomic-break"><hr/></div>;
    }
  }
  return blockToHTML(block);
};

const newEntityToHTML = (entity, originalText) => {
  if (entity.type === 'embed') {
    return (
      <div>
        <a
          className="embedly-card"
          href={entity.data.url}
          data-card-controls="0"
          data-card-theme="dark"
        >Embedded ― {entity.data.url}
        </a>
      </div>
    );
  }
  return entityToHTML(entity, originalText);
};

const handleBeforeInput = (editorState, str, onChange) => {
  if (str === '"' || str === '\'') {
    const currentBlock = getCurrentBlock(editorState);
    const selectionState = editorState.getSelection();
    const contentState = editorState.getCurrentContent();
    const text = currentBlock.getText();
    const len = text.length;
    if (selectionState.getAnchorOffset() === 0) {
      onChange(EditorState.push(editorState, Modifier.insertText(contentState, selectionState, (str === '"' ? DQUOTE_START : SQUOTE_START)), 'transpose-characters'));
      return HANDLED;
    } else if (len > 0) {
      const lastChar = text[len - 1];
      if (lastChar !== ' ') {
        onChange(EditorState.push(editorState, Modifier.insertText(contentState, selectionState, (str === '"' ? DQUOTE_END : SQUOTE_END)), 'transpose-characters'));
      } else {
        onChange(EditorState.push(editorState, Modifier.insertText(contentState, selectionState, (str === '"' ? DQUOTE_START : SQUOTE_START)), 'transpose-characters'));
      }
      return HANDLED;
    }
  }
  return beforeInput(editorState, str, onChange, newTypeMap);
};


class SeparatorSideButton extends React.Component {
  constructor(props) {
    super(props);
    this.onClick = this.onClick.bind(this);
  }

  onClick() {
    let editorState = this.props.getEditorState();
    const content = editorState.getCurrentContent();
    const contentWithEntity = content.createEntity('separator', 'IMMUTABLE', {});
    const entityKey = contentWithEntity.getLastCreatedEntityKey();
    editorState = EditorState.push(editorState, contentWithEntity, 'create-entity');
    this.props.setEditorState(
      AtomicBlockUtils.insertAtomicBlock(
        editorState,
        entityKey,
        '-'
      )
    );
    this.props.close();
  }

  render() {
    return (
      
      <Button  style={{width:50,borderRadius:'0.8rem'}}  
        // className="md-sb-button md-sb-img-button"
        type="primary"
        title="Add a separator"
        onClick={this.onClick}
        icon="minus"
      >
          {/* <svg xmlns="http://www.w3.org/2000/svg"  viewBox="0 0 24 24"><path d="M19 13H5v-2h14v2z"/></svg> */}
      </Button>
      
      
    );
  }
}

class ImageButton extends React.Component  {
  static propTypes = {
    setEditorState: PropTypes.func,
    getEditorState: PropTypes.func,
    close: PropTypes.func,
  };

  constructor(props) {
    super(props);

    this.onClick = this.onClick.bind(this);
    this.onChange = this.onChange.bind(this);
  }

  onClick() {
    this.input.value = null;
    this.input.click();
  }


onChange = (e) => {
  
  const img = e.target.files[0];
    if (img.type.indexOf('image/') === 0) {
      const props = this.props;
      console.log(img)
      console.log(loadingif)
      const placeHolderBlock = this.createPlaceholderImage(loadingif)
      const placeholderData = placeHolderBlock.getData();
      const placeHolderBlockKey = placeHolderBlock.getKey();

      
      return new Promise(
          (resolve, reject) => {
            const xhr = new XMLHttpRequest(); // eslint-disable-line no-undef
            xhr.open('POST', 'https://api.imgur.com/3/image');
            xhr.setRequestHeader('Authorization', 'Client-ID c2f926d5f0b58e1');
            const data = new FormData(); // eslint-disable-line no-undef
            data.append('image', img);
            xhr.send(data);
            xhr.addEventListener('load', () => {
              const response = JSON.parse(xhr.responseText);
              console.log(response.data.link)
              const newData = placeholderData.set('src', response.data.link);
              props.setEditorState(ImageButton.updateDataOfBlock(
                  props.getEditorState(),
                  placeHolderBlockKey,
                  newData,
              ));
              resolve(response);  
            });
            xhr.addEventListener('error', () => {
              message.error("Can't upload photo!")
              reject();       
            });
          },
      )
    }
    this.props.close();
  }

    /**
     * Update the data in a block preserving the current selection state
     * @param editorState
     * @param blockKey
     * @param newData
     */
    static updateDataOfBlock(editorState, blockKey, newData) {
      const contentState = editorState.getCurrentContent();
      const selectionState = editorState.getSelection();
      const newBlock = contentState.getBlockForKey(blockKey).merge({
          data: newData,
      });
      const newContentState = contentState.merge({
          blockMap: contentState.getBlockMap().set(blockKey, newBlock),
          selectionAfter: selectionState,
      });
      return EditorState.push(editorState, newContentState, 'change-block-data');
  }

  // callback(err,src) {
  //   console.log("FILE2",src)
  //   // const src = URL.createObjectURL(file);
  //   const editorState = addNewBlock(
  //     this.props.getEditorState(),
  //     Block.IMAGE, {
  //         src,
  //     }
  //   );
  //   const block = getCurrentBlock(editorState);
  //   this.props.setEditorState(editorState);
  //   return block;
  // }
  /**
   * This will create a placeholder image block with blob data that will be replaced by the actual uploaded URL
   * //param file
   * returns {*}
   */
  createPlaceholderImage(src) {
    
    // const src = URL.createObjectURL(file); 

     const editorState = addNewBlock(
      this.props.getEditorState(),
      Block.IMAGE, {
          src,
      }
    );
    const block = getCurrentBlock(editorState);
    this.props.setEditorState(editorState);
    return block
  }

  render() {
    return (
      
      <Button  type="primary" 
        style={{width:50,borderRadius:'0.8rem'}}
        // className="md-sb-button md-sb-img-button"
        icon="camera"
        type="primary"
        onClick={this.onClick}
        title="Add an Image"
      >
      {/* <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><circle cx="12" cy="12" r="3.2"/><path d="M9 2L7.17 4H4c-1.1 0-2 .9-2 2v12c0 1.1.9 2 2 2h16c1.1 0 2-.9 2-2V6c0-1.1-.9-2-2-2h-3.17L15 2H9zm3 15c-2.76 0-5-2.24-5-5s2.24-5 5-5 5 2.24 5 5-2.24 5-5 5z"/></svg> */}
      <input
          type="file"
          accept="image/*"
          ref={(c) => { this.input = c; }}
          onChange={this.onChange}
          style={{ display: 'none' }}
        />
      </Button>
      
    );
  }
}

class EmbedSideButton extends React.Component {
  static propTypes = {
    setEditorState: PropTypes.func,
    getEditorState: PropTypes.func,
    close: PropTypes.func,
  };

  constructor(props) {
    super(props);
    this.onClick = this.onClick.bind(this);
    this.addEmbedURL = this.addEmbedURL.bind(this);
  }

  onClick() {
    const url = window.prompt('Enter a URL', '');
    this.props.close();
    if (!url) {
      return;
    }
    this.addEmbedURL(url);
  }

  addEmbedURL(url) {
    let editorState = this.props.getEditorState();
    const content = editorState.getCurrentContent();
    const contentWithEntity = content.createEntity('embed', 'IMMUTABLE', {url});
    const entityKey = contentWithEntity.getLastCreatedEntityKey();
    editorState = EditorState.push(editorState, contentWithEntity, 'create-entity');
    this.props.setEditorState(
      AtomicBlockUtils.insertAtomicBlock(
        editorState,
        entityKey,
        'E'
      )
    );
  }

  render() {
    return (
      <span style={{margin:2}}>
      <Button type="primary" style={{width:50,borderRadius:'0.8rem'}}
        
        // className="md-sb-button md-sb-img-button"
        // type="button"
        // style={{borderRadius:'1rem'}}
        title="Add an Embed"
        onClick={this.onClick}
        icon="play-circle"
      >
        {/* <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M9.4 16.6L4.8 12l4.6-4.6L8 6l-6 6 6 6 1.4-1.4zm5.2 0l4.6-4.6-4.6-4.6L16 6l6 6-6 6-1.4-1.4z"/></svg> */}
      </Button>
      </span>
    );
  }

}
  
  
class AtomicEmbedComponent extends React.Component {
  static propTypes = {
    data: PropTypes.object.isRequired,
  }

  constructor(props) {
    super(props);

    this.state = {
      showIframe: false,
    };

    this.enablePreview = this.enablePreview.bind(this);
  }

  componentDidMount() {
    this.renderEmbedly();
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.showIframe !== this.state.showIframe && this.state.showIframe === true) {
      this.renderEmbedly();
    }
  }

  getScript() {
    const script = document.createElement('script');
    script.async = 1;
    script.src = '//cdn.embedly.com/widgets/platform.js';
    script.onload = () => {
      window.embedly();
    };
    document.body.appendChild(script);
  }

  renderEmbedly() {
    if (window.embedly) {
      window.embedly();
    } else {
      this.getScript();
    }
  }

  enablePreview() {
    this.setState({
      showIframe: true,
    });
  }

  render() {
    const { url } = this.props.data;
    const innerHTML = `<div><a class="embedly-card" href="${url}" data-card-controls="0" data-card-theme="dark">Embedded ― ${url}</a></div>`;
    return (
      <div className="md-block-atomic-embed">
        <div dangerouslySetInnerHTML={{ __html: innerHTML }} />
      </div>
    );
  }
}

const AtomicSeparatorComponent = (props) => (
  <hr />
);

const AtomicBlock = (props) => {
  const { blockProps, block } = props;
  const content = blockProps.getEditorState().getCurrentContent();
  const entity = content.getEntity(block.getEntityAt(0));
  const data = entity.getData();
  const type = entity.getType();
  if (blockProps.components[type]) {
    const AtComponent = blockProps.components[type];
    return (
      <div className={`md-block-atomic-wrapper md-block-atomic-wrapper-${type}`}>
        <AtComponent data={data} />
      </div>
    );
  }
  return <p>Block of type <b>{type}</b> is not supported.</p>;
};

@connect(({ newtopic}) => ({newtopic}))
export default class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      title: '',
      tagSelect : [],
      visible: false,
      editorState: createEditorState(),
      editorEnabled: true,
      placeholder: 'Write here...',
      tagListChildren: [],
    };

    this.onChange = (editorState, callback = null) => {
      if (this.state.editorEnabled) {
        this.setState({ editorState }, () => {
          if (callback) {
            callback();
          }
        });
      }
    };

    this.sideButtons = [{
      title: 'Image',
      component: ImageButton,
    }, {
      title: 'Embed',
      component: EmbedSideButton,
    }, {
      title: 'Separator',
      component: SeparatorSideButton,
    }];


    this.exporter = setRenderOptions({
      styleToHTML,
      blockToHTML: newBlockToHTML,
      entityToHTML: newEntityToHTML,
    });

    this.getEditorState = () => this.state.editorState;

    // this.logData = this.logData.bind(this);
    this.renderHTML = this.renderHTML.bind(this);
    this.toggleEdit = this.toggleEdit.bind(this);
    this.fetchData = this.fetchData.bind(this);
    this.loadSavedData = this.loadSavedData.bind(this);
    this.keyBinding = this.keyBinding.bind(this);
    this.handleKeyCommand = this.handleKeyCommand.bind(this);
    this.handleDroppedFiles = this.handleDroppedFiles.bind(this);
    this.handleReturn = this.handleReturn.bind(this);
  }

  componentDidMount() {
    setTimeout(this.fetchData, 1000);
    this.props.dispatch({
      type : 'newtopic/fetchTags'
    })
  }
  handleChangeSelect = (value) =>{
    this.setState({
      tagSelect :value
    })
  }
  onVisibleChange = (visible) => {
    this.setState({ visible });
  };

  titleOnChange = (e) => {
    this.setState({title: e.target.value})
  };
  rendererFn(setEditorState, getEditorState) {
    const atomicRenderers = {
      embed: AtomicEmbedComponent,
      separator: AtomicSeparatorComponent,
    };
    const rFnOld = rendererFn(setEditorState, getEditorState);
    const rFnNew = (contentBlock) => {
      const type = contentBlock.getType();
      switch(type) {
        case Block.ATOMIC:
          return {
            component: AtomicBlock,
            editable: false,
            props: {
              components: atomicRenderers,
              getEditorState,
            },
          };
        default: return rFnOld(contentBlock);
      }
    };
    return rFnNew;
  }

  keyBinding(e) {
    if (hasCommandModifier(e)) {
      if (e.which === 83) {  /* Key S */
        return 'editor-save';
      }
      // else if (e.which === 74 /* Key J */) {
      //  return 'do-nothing';
      //}
    }
    if (e.altKey === true) {
      if (e.shiftKey === true) {
        switch (e.which) {
          /* Alt + Shift + L */
          case 76: return 'load-saved-data';
          /* Key E */
          // case 69: return 'toggle-edit-mode';
        }
      }
      if (e.which === 72 /* Key H */) {
        return 'toggleinline:HIGHLIGHT';
      }
    }
    return keyBindingFn(e);
  }

  handleKeyCommand(command) {
    if (command === 'editor-save') {
      window.localStorage['editor'] = JSON.stringify(convertToRaw(this.state.editorState.getCurrentContent()));
      window.ga('send', 'event', 'draftjs', command);
      return true;
    } else if (command === 'load-saved-data') {
      this.loadSavedData();
      return true;
    } else if (command === 'toggle-edit-mode') {
      this.toggleEdit();
    }
    return false;
  }

  fetchData() {
    window.ga('send', 'event', 'draftjs', 'load-data', 'ajax');
    this.setState({
      placeholder: 'Loading...',
    });
    var params = {
      topicid: this.props.match.params.id
    }
    fetchTopicById(params)
      .then(response => {
        console.log("response",response.data)
        console.log("title",response.data.topic.Title)
        this.setState({
          title: response.data.topic.Title,
          editorState: createEditorState(response.data.topic.Post),
          tagListChildren: response.data.topic.Tag,
        })
    })
  }

  // logData(e) {
  //   const currentContent = this.state.editorState.getCurrentContent();
  //   const es = convertToRaw(currentContent);
  //   console.log(es);
  //   console.log(this.state.editorState.getSelection().toJS());
  //   window.ga('send', 'event', 'draftjs', 'log-data');
  // }

  renderHTML(e) {
    const currentContent = this.state.editorState.getCurrentContent();
    const eHTML = this.exporter(currentContent);
    var newWin = window.open(
      `${window.location.pathname}rendered.html`,
      'windowName',`height=${window.screen.height},width=${window.screen.wdith}`);
    newWin.onload = () => newWin.postMessage(eHTML, window.location.origin);
  }

  loadSavedData() {
    const data = window.localStorage.getItem('editor');
    if (data === null) {
      return;
    }
    try {
      const blockData = JSON.parse(data);
      console.log(blockData);
      this.onChange( EditorState.push(this.state.editorState, convertFromRaw(blockData)), this._editor.focus);
    } catch(e) {
      console.log(e);
    }
    window.ga('send', 'event', 'draftjs', 'load-data', 'localstorage');
  }

  toggleEdit(e) {
    this.setState({
      editorEnabled: !this.state.editorEnabled
    }, () => {
      window.ga('send', 'event', 'draftjs', 'toggle-edit', this.state.editorEnabled + '');
    });
  }

  handleDroppedFiles(selection, files) {
    window.ga('send', 'event', 'draftjs', 'filesdropped', files.length + ' files');
    const file = files[0];
    if (file.type.indexOf('image/') === 0) {
      // eslint-disable-next-line no-undef
      const src = URL.createObjectURL(file);
      this.onChange(addNewBlockAt(
        this.state.editorState,
        selection.getAnchorKey(),
        Block.IMAGE, {
          src,
        }
      ));
      return HANDLED;
    }
    return NOT_HANDLED
  }

  handleReturn(e) {
    // const currentBlock = getCurrentBlock(this.state.editorState);
    // var text = currentBlock.getText();
    return NOT_HANDLED;
  }

  publish = (visible) => {
    const {title} = this.state;
    console.log(title)
    if(title != ''){
      this.setState({visible});
      const { title, editorState, tagSelect} = this.state;
        this.props.dispatch({
          type: 'edit-topic/updateTopic',
          payload:{
            id:  this.props.match.params.id,
            title: title,
            content : convertToRaw(editorState.getCurrentContent()),
            tag : tagSelect,
          }
        });
    }else{
      message.error('Publishing will become available after you start writing.');
    }
    
  }


  renderContent = () => {
    const {newtopic:{TagList}} = this.props;
    const {tagListChildren} = this.state;
    const children = [];

    TagList.map(m =>{
      children.push(<Option key={m.Name}>{m.Name}</Option>)
    })

    return (
      <div>
        <div>
            <h4>Ready to publish?</h4>
            <p style={{fontFamily:'Roboto, sans-serif',fontSize:'14px'}}>Add or change tags (up to 5) so readers know<br/>
              what your story is about:</p>

             <Select
                defaultValue= {tagListChildren}
                mode="tags"
                style={{paddingBottom:'10px', width: '300px' }}
                tokenSeparators={[',']}
                onChange={this.handleChangeSelect}
                getPopupContainer={triggerNode => triggerNode.parentNode} 
                placeholder="Add a tag..."
              >
                {children}
              </Select>
            <p style={{fontFamily:'Roboto, sans-serif',fontSize:'14px'}}>Tip: add a high resolution image to your story to<br/>
             capture people’s interest</p>
        </div>
        <Divider/>
        <div>
          <Button type="primary"  icon="cloud-upload" 
            onClick={() => this.publish(false)}
           >
            Publish
          </Button>
        </div>
      </div>
    ); 
  }

  render() {
    const { editorState, editorEnabled ,title} = this.state;
    console.log("thistitle",title)
    return (
      <div className={styles.normal}>
      <Row  type="flex" justify="end">
          <Popover
            visible={this.state.visible}
            onVisibleChange={this.onVisibleChange}
            placement="bottomRight"
            trigger="click"
            content={this.renderContent()}
          >
            <Button type="primary" icon="cloud-upload">Publish</Button>
          </Popover>
      </Row>
      <Row type="flex" justify="center">

        <Col xs={24} sm={24} md={24} lg={18} xl={12}>
        <Card 
                bordered={false}
                bodyStyle={{padding:'0'}}
              >
          <div className="headline">
            <Input.TextArea 
              value={title} 
              onChange={this.titleOnChange}  
              placeholder="Title" autosize 
              className={styles.textarea} />
          </div>
          <div className="paragraph">
          <Editor
              ref={(e) => {this._editor = e;}}
              editorState={editorState}
              onChange={this.onChange}
              editorEnabled={editorEnabled}
              handleDroppedFiles={this.handleDroppedFiles}
              handleKeyCommand={this.handleKeyCommand}
              placeholder={this.state.placeholder}
              keyBindingFn={this.keyBinding}
              beforeInput={handleBeforeInput}
              handleReturn={this.handleReturn}
              sideButtons={this.sideButtons}
              rendererFn={this.rendererFn}
          />
          </div>
          </Card>
        </Col>
      </Row>
      </div>
    );
  }
}

if (true) {
  window.ga = function() {
    console.log(arguments);
  };
}