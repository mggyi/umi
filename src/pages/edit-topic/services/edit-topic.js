import request from '../../../utils/request';
import {GetToken} from '../../../utils/authority';
import api from '../../../api'

export async function updateTopic(params) {
    var token = GetToken()
    var body = {
      "id" :params.id,
      "tag" :params.tag,
      "title" : params.title,
      "topic" : params.content
    }
    console.log(body)
    var obj = {  
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Origin': '',
        'Authorization':`token ${token}`
      },
      body:JSON.stringify(body)
    }
  
    return request(`${api}/updateTopic`, obj);
}