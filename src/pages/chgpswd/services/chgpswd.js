
import request from '../../../utils/request';
import {GetToken} from '../../../utils/authority';
import api from '../../../api'


export function chgpswd(params) {
    var token = GetToken()

    var body = {
        "password": params.password,
        "newpassword": params.newpassword,
    }
    var obj = {  
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Origin': '',
        'Authorization':`token ${token}`
      },
      body:JSON.stringify(body)
    }
  
    return request(`${api}/chgpswd`, obj);
}