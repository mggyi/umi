// import React from 'react'
// import { Form,DatePicker,Select,  Checkbox,Cascader,Timeline, Icon ,Card ,Avatar,Row,Col,Spin, Input,Tooltip,Button} from 'antd';
// import styles from './index.less'
// import api from '../../../api'
// import axios from 'axios';
// import { connect } from 'dva';

// const FormItem = Form.Item;

// @connect(({ chgpswd }) => ({
//     chgpswd,
// }))
// class ChangePasswordForm extends React.Component {
//     state = {
//         confirmDirty: false,
//         userNameStatus: '',
//         userNameMessage: '',
//     };

//     validateToNextPassword = (rule, value, callback) => {
//         const form = this.props.form;
//         if (value && this.state.confirmDirty) {
//           form.validateFields(['confirm'], { force: true });
//         }
//         callback();
//     }
//     compareToFirstPassword = (rule, value, callback) => {
//         const form = this.props.form;
//         if (value && value !== form.getFieldValue('newpassword')) {
//           callback('Two passwords that you enter is inconsistent!');
//         } else {
//           callback();
//         }
//     }
    
//     handleConfirmBlur = (e) => {
//         const value = e.target.value;
//         this.setState({ confirmDirty: this.state.confirmDirty || !!value });
//     }
    
//     handleSubmit = (e) => {
//         e.preventDefault();

       
//         this.props.form.validateFields((err, values) => {
//           if (!err) {
//             this.props.dispatch({
//                 type: 'chgpswd/change',
//                 payload: {
//                     password: values.password,
//                     newpassword: values.newpassword,
//                 }
//             })
//           }
//         });
//     }


//   render() {
//     const { getFieldDecorator } = this.props.form;
//     return (
//         <div style={{marginTop: '15px'}}>
//             <Card lg={12} sm={12} style={{marginBottom:'10px'}}>
//                 <Row Row type="flex" justify="center">
//                 <Form onSubmit={this.handleSubmit} style={{paddingTop:"30px"}}>
//                     <FormItem style={{paddingBottom:'10px'}}>
//                         {getFieldDecorator('password', {
//                             rules: [
//                                 { required: true, message: 'Please input your Current Password!' },
//                             ],
//                         })(
//                             <Input style={{width:'300px'}} size="large"  prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}  type="password"  placeholder="Current Password" />
//                         )}
//                     </FormItem>
                    

                    
//                     <FormItem  style={{paddingBottom:'10px',width:'300px'}}>
//                         {getFieldDecorator('newpassword', {
//                             rules: [
//                                 { required: true, message: 'Please input your New Password!' },
//                                 { validator: this.validateToNextPassword,},
//                                 { pattern: "(?=.*[a-z])(?=.*[A-Z]).{6,}", message:"Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters"}
//                             ],
//                         })(
//                             <Input  size="large"  prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder="New Password" />
//                         )}
//                     </FormItem>

//                     <FormItem  style={{paddingBottom:'10px'}}>
//                         {getFieldDecorator('confirm', {
//                             rules: [
//                                 { required: true, message: 'Please input your Comfirm Password!' },
//                                 { validator: this.compareToFirstPassword, }
//                             ],
//                         })(
//                             <Input style={{width:'300px'}} size="large"  prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder="Comfirm Password" onBlur={this.handleConfirmBlur} />
//                         )}
//                     </FormItem>

//                     <FormItem style={{paddingBottom:'10px'}}>
//                     <Row Row type="flex" justify="center">
//                         <Button size="large" type="primary" htmlType="submit" className={styles.loginformbutton}>
//                             Change Password
//                         </Button>
//                     </Row>
//                     </FormItem>
//                 </Form>
//                 </Row>
//             </Card>
//         </div>
//     );
//   }
// }

// const ChangePassword = Form.create()(ChangePasswordForm);
// export default ChangePassword;



import React from 'react'
import { Form,Icon ,Card ,Row, Input,Button} from 'antd';
// import styles from './index.less'
import api from '../../../api'
import axios from 'axios';
import { connect } from 'dva';
import styles from './index.css';

const FormItem = Form.Item;

@connect(({ chgpswd }) => ({
    chgpswd,
}))
class ChangePasswordForm extends React.Component {
        state = {
        confirmDirty: false,
        userNameStatus: '',
        userNameMessage: '',
    };

    validateToNextPassword = (rule, value, callback) => {
        const form = this.props.form;
        if (value && this.state.confirmDirty) {
          form.validateFields(['confirm'], { force: true });
        }
        callback();
    }
    compareToFirstPassword = (rule, value, callback) => {
        const form = this.props.form;
        if (value && value !== form.getFieldValue('newpassword')) {
          callback('Two passwords that you enter is inconsistent!');
        } else {
          callback();
        }
    }
    
    handleConfirmBlur = (e) => {
        const value = e.target.value;
        this.setState({ confirmDirty: this.state.confirmDirty || !!value });
    }
    
    handleSubmit = (e) => {
        e.preventDefault();

       
        this.props.form.validateFields((err, values) => {
          if (!err) {
            this.props.dispatch({
                type: 'chgpswd/change',
                payload: {
                    password: values.password,
                    newpassword: values.newpassword,
                }
            })
          }
        });
    }
    render(){
        const { getFieldDecorator } = this.props.form;
        return (
            <div className={styles.normal}>
             <Card lg={12} sm={12} style={{marginBottom:'10px'}}>
                 <Row Row type="flex" justify="center">
                 <Form onSubmit={this.handleSubmit} style={{paddingTop:"30px"}}>
                     <FormItem style={{paddingBottom:'10px'}}>
                         {getFieldDecorator('password', {
                             rules: [
                                 { required: true, message: 'Please input your Current Password!' },
                             ],
                         })(
                             <Input style={{width:'300px'}} size="large"  prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}  type="password"  placeholder="Current Password" />
                         )}
                     </FormItem>
                   

                    
                     <FormItem  style={{paddingBottom:'10px',width:'300px'}}>
                         {getFieldDecorator('newpassword', {
                             rules: [
                                 { required: true, message: 'Please input your New Password!' },
                                 { validator: this.validateToNextPassword,},
                                 { pattern: "(?=.*[a-z])(?=.*[A-Z]).{6,}", message:"Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters"}
                             ],
                         })(
                             <Input  size="large"  prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder="New Password" />
                         )}
                     </FormItem>

                     <FormItem  style={{paddingBottom:'10px'}}>
                         {getFieldDecorator('confirm', {
                            rules: [
                                 { required: true, message: 'Please input your Comfirm Password!' },
                                 { validator: this.compareToFirstPassword, }
                             ],
                         })(
                             <Input style={{width:'300px'}} size="large"  prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder="Comfirm Password" onBlur={this.handleConfirmBlur} />
                         )}
                     </FormItem>

                     <FormItem style={{paddingBottom:'10px'}}>
                     <Row Row type="flex" justify="center">
                         <Button size="large" type="primary" htmlType="submit" className={styles.loginformbutton}>
                             Change Password
                         </Button>
                     </Row>
                     </FormItem>
                 </Form>
                 </Row>
             </Card>
            </div>
          );
    }
}
const ChangePassword = Form.create()(ChangePasswordForm);
export default ChangePassword;