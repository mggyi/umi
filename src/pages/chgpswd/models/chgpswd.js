import {chgpswd} from '../services/chgpswd'
import { routerRedux } from 'dva/router';
import { message } from 'antd';

export default {

    namespace: 'chgpswd',
  
    state: {},
    effects: {
      *change({payload}, { call, put }) {
            const response = yield call(chgpswd,payload);
            
            if(response.data.status === 'ok'){
              
              yield put(routerRedux.push(`/profile`));
            }else {
              
              message.error(response.data.message);
            }
      },
    },
    reducers: {},
};
  