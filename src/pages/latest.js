import React from 'react';
import { List, Button, Spin ,Tag,Rate} from 'antd';
import reqwest from 'reqwest';
import { connect } from 'react-redux';

const fakeDataUrl = 'http://localhost:8080/getlatestpost'
@connect(({usr}) => ({
  usr,
}))
class Latest extends React.Component {
  state = {
    count : 0,
    loading: true,
    loadingMore: false,
    showLoadingMore: true,
    data: [],
  }
  componentDidMount() {
    this.getData((res) => {
      this.setState({
        loading: false,
        data: res.topicList,
      });
    });
  }

  getData = (callback) => {
    this.setState((prevState) => {
      return {count: prevState.count + 1};
    });

    console.log(this.state.count)
    reqwest({
      url: fakeDataUrl,
      type: 'json',
      crossOrigin: true,
      method: 'get',
      contentType: 'application/json',
      success: (res) => {
        console.log(res)
        callback(res);
      },
    });
  }

  onLoadMore = () => {
    this.setState({
      loadingMore: true,
    });

    this.getData((res) => {
      const data = this.state.data.concat(res.topicList);
      this.setState({
        data,
        loadingMore: false,
      },() => {
        // Resetting window's offsetTop so as to display react-virtualized demo underfloor.
        // In real scene, you can using public method of react-virtualized:
        // https://stackoverflow.com/questions/46700726/how-to-use-public-method-updateposition-of-react-virtualized
        window.dispatchEvent(new Event('resize'));
      });
    });
  }

  render() {
    // const dispatch = this.props.dispatch
    const { loading, loadingMore, showLoadingMore, data } = this.state;
    const loadMore = showLoadingMore ? (
      <div style={{ textAlign: 'center', marginTop: 12, height: 32, lineHeight: '32px' }}>
        {loadingMore && <Spin />}
        {!loadingMore && <Button onClick={this.onLoadMore}>loading more</Button>}
      </div>
    ) : null;

    return (
      <List
        className="demo-loadmore-list"
        loading={loading}
        itemLayout="horizontal"
        loadMore={loadMore}
        dataSource={data}
        renderItem={item => (
          <List.Item actions={[<Rate disabled defaultValue={2} />, <a>more</a>]}>
            <List.Item.Meta
              // avatar={<Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />}
              title={<a href={`#/pb/post/${item.Uuid}`} >{item.Title}</a>}
              // description="Ant Design, a design language for background applications, is refined by Ant UED Team"
            />
            <TagList  tag={item.Tag}/>
          </List.Item>
        )}
      />
    );
  }
}

function TagList(props){
  const listItems = props.tag != null? props.tag.map((num) =>
    <Tag color="#2db7f5">{num}</Tag>
  ):null;
  if(props.tag != null) {
    return <div>{listItems}</div>
  }else{
    return <div></div>
  }  
}
const mapStateToProps = (state) => {
  return { usr: state.usr };
}
export default connect(mapStateToProps)(Latest)
