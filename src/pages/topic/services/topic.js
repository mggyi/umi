import request from '../../../utils/request';
import {GetToken} from '../../../utils/authority';
import api from '../../../api'


export function fetchReply(params) {
    var token = GetToken()
    var obj = {
        method: 'GET',
        headers: {
            'Authorization':`token ${token}`
        }
    }
    return request(`${api}/fetchReply/${params.commentId}/${params.query}`,obj)
}

export function fetchComment(params) {
    var token = GetToken()
    var obj = {
        method: 'GET',
        headers: {
            'Authorization':`token ${token}`
        }
    }
    return request(`${api}/fetchComment/${params.postId}/${params.query}`,obj)
}

export function fetchTopicById(params) {
    var token = GetToken()
    var obj = {
        method: 'GET',
        headers: {
            'Authorization':`token ${token}`
        }
    }
    return request(`${api}/post/${params.topicid}`,obj)
}


export function addViewCount(id) {
    var obj = {
        method: 'GET'
    }
    return request(`${api}/addViewCount/${id}`,obj)
}
export function addLike(params) {
    var token = GetToken()
    var obj = {
        method: 'GET',
        headers: {
            'Authorization':`token ${token}`
        }
    }
    return request(`${api}/addLike/${params.topicid}`,obj)
}

export function addStar(params) {
    var token = GetToken()
    var obj = {
        method: 'GET',
        headers: {
            'Authorization':`token ${token}`
        }
    }
    return request(`${api}/addStar/${params.topicid}`,obj)  
}

export async function createComment(params) {
    var token = GetToken()
    var body = {
      "postId" : params.postId,
      "comment" : params.comment
    }
  
    var obj = {  
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Origin': '',
        'Authorization':`token ${token}`
      },
      body:JSON.stringify(body)
    }
    return request(`${api}/createComment`,obj)  
}

export async function createReply(params) {
    console.log(params.postId,params.commentId,params.reply)
    var token = GetToken()
    var body = {
      "postId" : params.postId,
      "commentId": params.commentId,
      "reply" : params.reply
    }
  
    var obj = {  
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Origin': '',
        'Authorization':`token ${token}`
      },
      body:JSON.stringify(body)
    }
    return request(`${api}/createReply`,obj)  
}
// export function updateProfile(params) {
//     var token = GetToken()

//     var body = {
//         "userid": params.userid,
//         "avatar": params.avatar,
//         "displayName": params.displayName,
//         "email": params.email,
//         "birthday": params.birthday,
//         "gender": params.gender,
//         "country": params.country,
//         "bio": params.bio,
//     }
//     var obj = {  
//       method: 'POST',
//       headers: {
//         'Content-Type': 'application/json',
//         'Origin': '',
//         'Authorization':`token ${token}`
//       },
//       body:JSON.stringify(body)
//     }
  
//     return request(`${api}/updateProfile`, obj);
// }