// import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';
import {Icon, Divider, Avatar,Row,Col, Card,Button} from 'antd';
import moment from 'moment';
import Reply from './Reply';
// import {Editor,EditorState}  from 'draft-js'

import {
  Editor,
  createEditorState,
} from 'medium-draft';
import 'medium-draft/lib/index.css';
import './index.css'
// const Panel = Collapse.Panel;
// var htmlencode = require('htmlencode');
const { Meta } = Card;
@connect(({topic}) => ({
    topic,
}))
class Comment extends React.Component{
  constructor(props){
    super(props)
    this.state = {
        liked: false,
        editorState: createEditorState(), // for empty content
    }
    this.onChange = (editorState) => {
      this.setState({ editorState });
    };
    this.handleClick = this.handleClick.bind(this);
  }
  componentWillUnmount() {
        this.props.dispatch({
          type: 'topic/fetchTopicByIdUnmount',
        })
  }

  componentDidMount() {
    this.refs.editor.focus();
  }
  handleClick() {
    this.setState({
      liked: !this.state.liked
    });
  }
  render(){
    const text = this.state.liked ? 'liked' : 'haven\'t liked';
    const label = this.state.liked ? 'Unlike' : 'Like'
    const { editorState } = this.state;
        return (
        <>
            <div className="customContainer">
                <button className="btn btn-primary" onClick={this.handleClick}>
                {label}</button>
                <p>
                    yo {text} this. Click to toggle.
                </p>
            </div>    
            <Row type="flex" justify="center">
                <Col xs={24} sm={24} md={24} lg={18} xl={12}>
                    <Divider/>
                    <div style={{paddingLeft:6}}>
                    <Editor
                      ref="editor"
                      placeholder="Write your response..."
                      editorState={editorState}
                      onChange={this.onChange} />
                    </div>
                    <Row type="flex" justify="end">
                        <Col>
                            <Button  style={{marginBottom:"5px"}}>Reply</Button>
                        </Col>
                    </Row>
                    <Divider/>
                </Col>
            </Row>
            <Row type="flex" justify="center">
                <Col xs={24} sm={24} md={24} lg={18} xl={12}>
                    <CommentList />
                </Col>
            </Row>
             </>
        )
    }
}
// const text = (
//     <p style={{ paddingLeft: 24 }}>
//       A dog is a type of domesticated animal.
//       Known for its loyalty and faithfulness,
//       it can be found as a welcome guest in many households across the world.
//     </p>
//   );
var commentList = [{
    avatar : "https://avatars1.githubusercontent.com/u/35128?s=88&v=4",
    name :"sorrycc",
    reply : "This is reply",
    dateTime : Date.now()
},{
    avatar : "https://avatars1.githubusercontent.com/u/35128?s=88&v=4",
    name :"sorrycc",
    reply : "This is reply",
    dateTime : Date.now()
}]

function CommentList(props) {
    return (
        <>
        <Row>
            <Col xs={24} sm={24} md={24} lg={18} xl={12}>
                <Row type="flex" justify="left" >
                    <Card  bordered={false} bodyStyle={{padding:0,paddingBottom:20}}>
                        <Meta
                            avatar={<Avatar size="medium" />}
                            title={ <a onClick={()=>{}} style={{fontSize:'12px'}} 
                            description={<p style={{fontStyle:'italic'}}>{moment(Date.now()).format('YYYY-MM-DD HH:mm')}</p>}
                            >
                            {"displayName"}
                            
                        </a>}/>
                    </Card>
                </Row>
            </Col>
        </Row>
        <Row type="flex" justify="left" >
            <Col>
                <p style={{textAlign:'left'}}>"We can confirm that two American cyclists were killed in the Danghara district on 29 July. Due to privacy concerns, we are unable to share further details."</p>
            </Col>
        </Row>
        <Row type="flex" justify="left" >
            <Col>
                <a style={{paddingRight:10}} onClick={()=>{alert('Like - 50')}}>
                    <Icon size="large" type="like" />50
                </a>
                <a onClick={()=> {alert('Comment - 30')}}>

                    <Icon size="large" type="message" />30
                </a>
            </Col>
        </Row>
        <Divider/>



        <Row>
            <Col xs={24} sm={24} md={24} lg={18} xl={12}>
                <Row type="flex" justify="left" >
                    <Card  bordered={false} bodyStyle={{padding:0,paddingBottom:20}}>
                        <Meta
                            avatar={<Avatar size="medium" />}
                            title={ <a onClick={()=>{}} style={{fontSize:'12px'}} >{"displayName"}
                        </a>}/>
                    </Card>
                </Row>
            </Col>
        </Row>
        <Row type="flex" justify="left" >
            <Col>
                <p style={{textAlign:'left'}}>US embassy in Dushanbe said:"</p>
            </Col>
        </Row>
        <Row type="flex" justify="left" >
            <Col>
                <a style={{paddingRight:10}} onClick={()=>{alert('Like - 50')}}>
                    <Icon size="large" type="like-o" />50
                </a>
                <a onClick={()=> {alert('Comment - 30')}}>
                    <Icon type="message" />30
                </a>
            </Col>
        </Row>
        <Row>
            <Reply list ={commentList}/>
        </Row>
        </>
    )
}
export default Comment;