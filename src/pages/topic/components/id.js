import React from 'react'
import PropTypes from 'prop-types';
import {Tooltip,Icon,Affix,BackTop ,Row,Col,Spin,Avatar,Divider,Select, Card,Button,Tag,Popconfirm,message,Menu,Dropdown} from 'antd';
import Social from '../../../components/Social';
import Loading from '../../../components/Loading';
import Shimmer from 'react-js-loading-shimmer';
import styles from './index.css'
import moment from 'moment';
import { connect } from 'dva';
import { routerRedux } from 'dva/router';
import Share from '../../../components/Share'
import Comment from './Comment'
import ContentLoader, { Facebook } from 'react-content-loader';
import Media from 'react-media';
import QueueAnim from 'rc-queue-anim';
import CommentList from './CommentList'
import {
    EditorState,
} from 'draft-js';
import {
    Editor,
    Block,
    createEditorState,
    rendererFn,
} from 'medium-draft';
import {
    setRenderOptions,
    blockToHTML,
    entityToHTML,
    styleToHTML,
  } from 'medium-draft/lib/exporter';
import 'draft-js/dist/Draft.css';
import 'medium-draft/lib/index.css';
import Like from './Like'
const { Meta } = Card;

const AtomicSeparatorComponent = (props) => (<hr />);
  
const AtomicBlock = (props) => {
  const { blockProps, block } = props;
  const content = blockProps.getEditorState().getCurrentContent();
  const entity = content.getEntity(block.getEntityAt(0));
  const data = entity.getData();
  const type = entity.getType();
  if (blockProps.components[type]) {
    const AtComponent = blockProps.components[type];
    return (
      <div className={`md-block-atomic-wrapper md-block-atomic-wrapper-${type}`}>
        <AtComponent data={data} />
      </div>
    );
  }
  return <p>Block of type <b>{type}</b> is not supported.</p>;
};
  
 
class AtomicEmbedComponent extends React.Component {
  static propTypes = {
    data: PropTypes.object.isRequired,
  }

  constructor(props) {
    super(props);

    this.state = {
      showIframe: false,
    };

    this.enablePreview = this.enablePreview.bind(this);
  }

  componentDidMount() {
    this.renderEmbedly();
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.showIframe !== this.state.showIframe && this.state.showIframe === true) {
      this.renderEmbedly();
    }
  }

  getScript() {
    const script = document.createElement('script');
    script.async = 1;
    script.src = '//cdn.embedly.com/widgets/platform.js';
    script.onload = () => {
      window.embedly();
    };
    document.body.appendChild(script);
  }

  renderEmbedly() {
    if (window.embedly) {
      window.embedly();
    } else {
      this.getScript();
    }
  }

  enablePreview() {
    this.setState({
      showIframe: true,
    });
  }
  // 
// Embedded ― ${url}
  render() {
    const { url } = this.props.data;
    const innerHTML = 
    `<div><a class="embedly-card" href="${url}" 
    data-card-controls="0" data-card-theme="dark"
    >
    <photo class="shine"></photo>
    </a></div>`;
    return (
      <div className="md-block-atomic-embed">
        <div dangerouslySetInnerHTML={{ __html: innerHTML }} />
      </div>
    );
  }
}
  
@connect(({ topic,loading }) => ({
    topic,
    loading: loading.effects['topic/fetchTopicById'],
    // loading: loading.models.topic,
  }))
class Demo extends React.Component {
  static contextTypes = {
    login: PropTypes.bool,
  };
  constructor(props) {
    super(props);
    this.state = {
      commentBoxShow: false,
      liked: false,
      stared: false,
      titleEditorState: EditorState.createEmpty(),
      editorState: createEditorState(),
    }
      this.getEditorState = () => this.state.editorState;
  }



  componentDidMount() {
        this.props.dispatch({
        type: 'topic/fetchTopicById',
        payload: {
            topicid: this.props.match.params.id,
        }
    })
  }
  commentOnClick = () => {
      this.setState({
        commentBoxShow: !this.state.commentBoxShow
      });    
  }
  rendererFn(setEditorState, getEditorState) {
    const atomicRenderers = {
      embed: AtomicEmbedComponent,
      separator: AtomicSeparatorComponent,
    };
    const rFnOld = rendererFn(setEditorState, getEditorState);
    const rFnNew = (contentBlock) => {
      const type = contentBlock.getType();
      switch(type) {
        case Block.ATOMIC:
          return {
            component: AtomicBlock,
            editable: false,
            props: {
              components: atomicRenderers,
              getEditorState,
            },
          };
        default: return rFnOld(contentBlock);
      }
    };
    return rFnNew;
  }
  likeHandleClick = () => {
    // alert( this.props)
    console.log(this.props)
    this.props.dispatch({
      type: 'topic/like',
      payload: {
          topicid: this.props.match.params.id,
          pathname: this.props.location.pathname,
      }
    })
  }
  starHandleClick = () => {
    this.props.dispatch({
      type: 'topic/star',
      payload: {
        topicid: this.props.match.params.id,
        pathname: this.props.location.pathname,
      }
    })
  }
  confirm = () => {
    message.info('Clicked on Yes.');
  }
  render () {
    console.log("this.Context", this.context)
    const text = 'Are you sure to delete this task?';
    console.log("this.props",this.props)
    const {loading,topic:{Stared,Liked,Title ,Post,Tag,User=[],CreateAt,StarCount=0,LikeCount=0,CommentCount=0,TitleImg,Description}} = this.props
    const likeicon = Liked?"like":"like-o"
    const staricon = Stared?"star":"star-o"
    const data = User.length > 0 ? true: false;
    console.log(loading)
    return (
     <Card
        bodyStyle={{padding:0,margin:0}}
        style={{border:'none',padding:0,margin:0}}
        loading={loading}>
          <Social 
            title={Title}
            url={`https://www.droidfo.tk/topic/${this.props.match.params.id}`}
            description={Description}
            image={TitleImg}
          />     
          <Row type="flex" justify="center">
            <Col xs={24} sm={24} md={24} lg={18} xl={12}>
              <Card 
                bordered={false}
                bodyStyle={{padding:'0'}}
              >
               <BackTop >
                <div style={{background:'#1890ff',borderRadius: 3,
                            textAlign:'center',color:'#fff'
                            }}><Icon type="up" /></div>
              </BackTop>
              {/* <div className="headline"> */}
              <h1> {data?Title:<box class="shine"></box>} </h1>
              {/* </div> */}




               <div className="paragraph">
                {data?<Editor
                    placeholder=""
                    ref={(e) => {this._editor = e;}}
                    editorState={createEditorState(Post)}
                    editorEnabled={false}
                    rendererFn={this.rendererFn}
                  />:
                  <div>
                    <lines class="shine"></lines>
                    <lines class="shine"></lines>
                    <lines class="shine"></lines>
                    <lines class="shine"></lines>
                    <lines class="shine"></lines>
                    <lines class="shine"></lines>
                    <lines class="shine"></lines>
                    <lines class="shine"></lines>
                    <lines class="shine"></lines>
                  </div>}
                </div>
                
                {data?<Row type="flex" justify="left"  style={{paddingLeft:'0px',paddingTop:'20px'}}>
                    <Card  bordered={false} bodyStyle={{padding:'0'}}>
                      <Meta
                        avatar={<Avatar type="square" src={User[0].avatar} style={{width:'50px',height:'50px'}} />}
                        title={ <a onClick={()=>{
                          this.props.dispatch(
                            routerRedux.push(`/users/${User[0].uuid}`)
                          )
                        }} className="smallheadline" >{User[0].displayName}
                      </a>}
                        description={<p className="date">{moment(CreateAt).format('YYYY-MM-DD HH:mm')}</p>}
                      />
                      </Card> 
                    </Row> 
                    :
                    <ContentLoader height={50}>
                      <rect x="57" y="9" rx="4" ry="4" width="117" height="6.4" /> 
                      <rect x="59" y="27" rx="3" ry="3" width="85" height="6.4" /> 
                      <circle cx="24" cy="24" r="24" />
                    </ContentLoader>       
               }



                <Row type="flex" justify="left"  style={{paddingTop:'30px'}} >
                  {data?<TagList  tag={Tag}/>:<lines class="shine"></lines>}
                </Row>
                {/* <Divider style={{margin:'10px 0 0 0',padding:0}}/> */}
                <Card bodyStyle={{padding:0,margin:0}} style={{marginTop:10,marginBottom:10,borderTop:'3px solid #1890ff',borderLeft:'none',borderRight:'none',borderBottom:'none'}}>
                <Media query="(max-width: 1200px)">
                    {matches =>
                        matches ? (
                          <Row  >
                            <Col span={6} onClick={this.starHandleClick} style={{cursor:'pointer'}}>
                                <Tooltip placement="top" title="Give me star!">
                                  <span style={{marginLeft:8,verticalAlign:'middle',fontSize:24,cursor: 'pointer'}}>
                                    <Icon size="large" type={staricon} style={{color:'#FFD700'}}  />
                                  </span>
                                  <span style={{verticalAlign:'middle',marginLeft:5}}>
                                    <p style={{display:'inline-block'}}>{StarCount}</p>
                                  </span>
                                </Tooltip>
                              </Col>
                              <Col span={6} onClick={this.likeHandleClick} style={{cursor:'pointer'}}>
                                <Tooltip placement="top" title="Give me like!">
                                  <span style={{marginLeft:8,verticalAlign:'middle',fontSize:24,cursor: 'pointer'}}>
                                    <Icon size="large" type={likeicon} style={{color:'#3b5998'}}  />
                                  </span>
                                  <span style={{verticalAlign:'middle',marginLeft:5}}>
                                    <p style={{display:'inline-block'}}>{LikeCount}</p>
                                  </span>
                                </Tooltip>
                              </Col>
                              <Col span={6} onClick={this.commentOnClick} style={{cursor:'pointer'}}>
                                <Tooltip placement="top" title="Comment me!">
                                  <span style={{marginLeft:8,verticalAlign:'middle',fontSize:24,cursor: 'pointer'}}>
                                    <Icon size="large" type="message" />
                                  </span>
                                  <span style={{verticalAlign:'middle',marginLeft:5}}>
                                    <p style={{display:'inline-block'}}>{CommentCount}</p>
                                  </span>
                                </Tooltip>
                              </Col>
                              <Col span={6} onClick={this.likeHandleClick} style={{cursor:'pointer'}} >
                                <Tooltip placement="top" title="Share me!">
                                  <span style={{marginLeft:8,verticalAlign:'middle',fontSize:24,cursor: 'pointer'}}>
                                    <Dropdown overlay={<Share shareUrl={`https://www.droidfo.tk/topic/${this.props.match.params.id}`} title={Title}/>} placement="topRight">
                                      <Icon size="large" type="share-alt" style={{fontSize:24,cursor: 'pointer'}} />
                                    </Dropdown>
                                  </span>
                                  <span style={{verticalAlign:'middle',marginLeft:5}}>
                                    <p style={{display:'inline-block'}}></p>
                                  </span>
                                </Tooltip>
                              </Col>
                          </Row>
                        ):(
                          <Row type="flex">
                              <Col style={{width:100}} onClick={this.starHandleClick}>
                                <Tooltip placement="top" title="Give me star!">
                                  <span style={{marginLeft:8,verticalAlign:'middle',fontSize:24,cursor: 'pointer'}}>
                                    <Icon size="large" type={staricon} style={{color:'#FFD700'}}  />
                                  </span>
                                  <span style={{verticalAlign:'middle',marginLeft:5}}>
                                    <p style={{display:'inline-block'}}>{StarCount}</p>
                                  </span>
                                </Tooltip>
                              </Col>
                              <Col style={{width:100}}>
                              <Tooltip placement="top" title="Give me like!">
                                <span style={{marginLeft:8,verticalAlign:'middle',fontSize:24,cursor: 'pointer'}}>
                                  <Icon size="large" type={likeicon} style={{color:'#3b5998'}}onClick={this.likeHandleClick}  />
                                </span>
                                <span style={{verticalAlign:'middle',marginLeft:5}}>
                                  <p style={{display:'inline-block'}}>{LikeCount}</p>
                                </span>
                              </Tooltip>
                              </Col>
                              <Col style={{width:100}}>
                              <Tooltip placement="top" title="Comment me!">
                                <span style={{marginLeft:8,verticalAlign:'middle',fontSize:24,cursor: 'pointer'}}>
                                  <Icon size="large" type="message" onClick={this.commentOnClick} />
                                </span>
                                <span style={{verticalAlign:'middle',marginLeft:5}}>
                                  <p style={{display:'inline-block'}}>{CommentCount}</p>
                                </span>
                              </Tooltip>
                              </Col>
          
          
                              <Col style={{width:100}}>
                                <Tooltip placement="top" title="Share me!">
                                  <span style={{verticalAlign:'middle',fontSize:24,cursor: 'pointer'}}>
                                    <Dropdown overlay={<Share shareUrl={`https://www.droidfo.tk/topic/${this.props.match.params.id}`} title={Title}/>} placement="topRight">
                                      <Icon size="large" type="share-alt" style={{fontSize:24,cursor: 'pointer'}}onClick={this.likeHandleClick}  />
                                    </Dropdown>
                                  </span>
                                  <span style={{verticalAlign:'middle',marginLeft:5}}>
                                    <p style={{display:'inline-block'}}></p>
                                  </span>
                                </Tooltip>
                              </Col>
                          </Row>                        
                        )}
                  </Media>
         
                </Card>

         {/* <Divider style={{padding:0,margin:0}}/> */}
         <QueueAnim className="demo-content">
          {this.state.commentBoxShow ? [
            <div className="demo-thead" key="a">
              {this.context.login?<Comment  postId={this.props.match.params.id}/> :null}
            </div>,
            <div className="demo-thead" key="b">
              <CommentList postId={this.props.match.params.id}/>
            </div>
          ] : null}
        </QueueAnim>
              </Card>
            </Col>
        </Row>
      </Card>
    )
  }
}

export default Demo;

function TagList(props){
  const listItems = props.tag != null? props.tag.map((num) =>
    <Tag >{num}</Tag>
  ):null;
  if(props.tag != null) {
    return <div>{listItems}</div>
  }else{
    return <div></div>
  }  
}
