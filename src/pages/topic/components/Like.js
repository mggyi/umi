import {Component} from 'react'
import {Icon,Row,Col} from 'antd';
import styles from './Like.less'


export default class LikeComponent extends Component {
    constructor(props){
        super(props)
        this.state = {
            liked: false,
            stared: false,
        }
    }

    // likeHandleClick = () => {
    //     this.setState({
    //       liked: !this.state.liked,
    //     });
    // }
    // starHandleClick = () => {
    //     this.setState({
    //       stared: !this.state.stared
    //     });
    // }
    render () {
        const { likeCount ,starCount,stared,liked ,starHandleClick} = this.props;
        const star = stared ? "star"  : "star-o";
        const like = liked ? "like"  : "like-o";
        const IconStar = ({ type, text ,onClick}) => (
            <span style={{paddingRight:5}}>
              <Icon onClick={onClick} type={type} style={{cursor: 'pointer',fontSize: 24, color: '#fadb14',marginRight:5}}/>
              {text}
            </span>
        );
        const IconLike = ({ type, text ,onClick}) => (
            <span style={{paddingRight:5}}>
              <Icon onClick={onClick} type={type} style={{cursor: 'pointer',fontSize: 24, color: '#46CEFF',marginRight: 5}}/>
              {text}
            </span>
        );
        return (
            <Row type="flex" align="left">
                <Col>
                    <div style={{paddingTop:20}}>
                        {/* <IconStar onClick={starHandleClick} type={star} text={starCount}/> */}
                        <IconLike onClick={this.props.likeHandleClick} type={like} text={likeCount}/>
                    </div>
                </Col>
            </Row>
        )
    }
}