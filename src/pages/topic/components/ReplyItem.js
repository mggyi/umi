import React, {Component} from 'react'
import PropTypes from 'prop-types';
import QueueAnim from 'rc-queue-anim';

import {Card,List,Avatar, Button, Spin,Collapse,Row,Col,Icon,Menu, Dropdown} from 'antd'
import { connect } from 'dva';
import {
    Editor,
    Block,
    createEditorState,
    rendererFn,
} from 'medium-draft';
import {
    setRenderOptions,
    blockToHTML,
    entityToHTML,
    styleToHTML,
  } from 'medium-draft/lib/exporter';
import 'draft-js/dist/Draft.css';
import 'medium-draft/lib/index.css';
import Moment from 'react-moment';
import Reply from './Reply';
import ReplyList from './ReplyList';


const Panel = Collapse.Panel;
const AtomicSeparatorComponent = (props) => (<hr />);
  
const AtomicBlock = (props) => {
  const { blockProps, block } = props;
  const content = blockProps.getEditorState().getCurrentContent();
  const entity = content.getEntity(block.getEntityAt(0));
  const data = entity.getData();
  const type = entity.getType();
  if (blockProps.components[type]) {
    const AtComponent = blockProps.components[type];
    return (
      <div className={`md-block-atomic-wrapper md-block-atomic-wrapper-${type}`}>
        <AtComponent data={data} />
      </div>
    );
  }
  return <p>Block of type <b>{type}</b> is not supported.</p>;
};
  
 
class AtomicEmbedComponent extends React.Component {
  static propTypes = {
    data: PropTypes.object.isRequired,
  }

  constructor(props) {
    super(props);

    this.state = {
      showIframe: false,
    };

    this.enablePreview = this.enablePreview.bind(this);
  }

  componentDidMount() {
    this.renderEmbedly();
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.showIframe !== this.state.showIframe && this.state.showIframe === true) {
      this.renderEmbedly();
    }
  }

  getScript() {
    const script = document.createElement('script');
    script.async = 1;
    script.src = '//cdn.embedly.com/widgets/platform.js';
    script.onload = () => {
      window.embedly();
    };
    document.body.appendChild(script);
  }

  renderEmbedly() {
    if (window.embedly) {
      window.embedly();
    } else {
      this.getScript();
    }
  }

  enablePreview() {
    this.setState({
      showIframe: true,
    });
  }
  // 
// Embedded ― ${url}
  render() {
    const { url } = this.props.data;
    const innerHTML = 
    `<div><a class="embedly-card" href="${url}" 
    data-card-controls="0" data-card-theme="dark"
    >
    <photo class="shine"></photo>
    </a></div>`;
    return (
      <div className="md-block-atomic-embed">
        <div dangerouslySetInnerHTML={{ __html: innerHTML }} />
      </div>
    );
  }
}

@connect(({ topic,loading }) => ({
    topic,
    loading: loading.effects['topic/fetchComment'],
}))
export default class CommentItem extends Component {
    static contextTypes = {
      login: PropTypes.bool,
    };
    state = {
        count : 1,
        loadingMore: false,
        showLoadingMore: true,
        data: [],
        commentBoxShow: false,
    }

  replyOnClick = () => {
      this.setState({
        commentBoxShow: !this.state.commentBoxShow
      });    
  }

  rendererFn(setEditorState, getEditorState) {
    const atomicRenderers = {
      embed: AtomicEmbedComponent,
      separator: AtomicSeparatorComponent,
    };
    const rFnOld = rendererFn(setEditorState, getEditorState);
    const rFnNew = (contentBlock) => {
      const type = contentBlock.getType();
      switch(type) {
        case Block.ATOMIC:
          return {
            component: AtomicBlock,
            editable: false,
            props: {
              components: atomicRenderers,
              getEditorState,
            },
          };
        default: return rFnOld(contentBlock);
      }
    };
    return rFnNew;
  }

    render () {
        const {item,postId} = this.props;
        console.log("REPLYITEM",item)
        const menu = (
          <Menu>
            <Menu.Item>
              <a target="_blank" rel="noopener noreferrer" href="http://www.alipay.com/">Edit</a>
            </Menu.Item>
            <Menu.Item>
              <a target="_blank" rel="noopener noreferrer" href="http://www.taobao.com/">Delete</a>
            </Menu.Item>
          </Menu>
        );

        return(
            <Card  
                bodyStyle={{padding:0,margin:0}} 
                style={{border:'none',marginTop:'20px',marginLeft:'40px'}}>
              {/* <div style={{background: '#1890ff',color:'white',paddingLeft:5, position: 'absolute',top: 0,right: 0,}}>
                <Icon  size="small" type="rollback"/>&nbsp;&nbsp;
              </div> */}
                <Card.Meta
                    avatar={<Avatar shape="square" size="small" src={item.User[0].avatar} />}
                    title={
                    <span>
                        {item.User[0].displayName}
                        <small style={{paddingLeft:5,color:'#bfbfbf'}}>
                        <Moment fromNow ago>{item.CreateAt}</Moment> ago
                        </small>
                    </span>                                      
                    }
                    description={
                      <div className="replydisplaybox">
                        <Editor 
                          placeholder=""
                          ref={(e) => {this._editor = e;}}
                          editorState={createEditorState(item.Reply)}
                          editorEnabled={false}
                          rendererFn={this.rendererFn}
                        />    
                      </div>
                    }
                    />
                    <Row  type="flex" justify="end">
                    <Col>
                        <Dropdown overlay={menu}>
                          <a className="ant-dropdown-link" href="#">
                            <Icon type="ellipsis" />
                          </a>
                        </Dropdown>
                    </Col>
                    </Row>
            </Card>

        )
    }
}
