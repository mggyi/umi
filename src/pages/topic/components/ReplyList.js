import React, {Component} from 'react'
import PropTypes from 'prop-types';
import QueueAnim from 'rc-queue-anim';

import {Card,List,Avatar, Button, Spin,Collapse,Row,Col,Icon} from 'antd'
import { connect } from 'dva';
import {
    Editor,
    Block,
    createEditorState,
    rendererFn,
} from 'medium-draft';
import {
    setRenderOptions,
    blockToHTML,
    entityToHTML,
    styleToHTML,
  } from 'medium-draft/lib/exporter';
import 'draft-js/dist/Draft.css';
import 'medium-draft/lib/index.css';
import Moment from 'react-moment';
import ReplyItem from './ReplyItem';


@connect(({ topic,loading }) => ({
    topic,
    loading: loading.effects['topic/fetchReply'],
}))
export default class ReplyList extends Component {
    state = {
        count : 1,
        loadingMore: false,
        showLoadingMore: true,
        data: [],
        commentBoxShow: false,
    }
    onLoadMore = () => {
        this.setState({
          loadingMore: true,
        });
        this.setState((prevState) => {
            return {count: prevState.count + 1};
        });
        this.props.dispatch({
            type: 'topic/fetchReply',
            payload: {
                commentId : this.props.commentId,
                query : this.state.count * 3
            }
        })
        this.setState({
            loadingMore: false,
          });
      }

    componentDidMount(){
        this.setState((prevState) => {
            return {count: prevState.count + 1};
        });
        this.props.dispatch({
            type: 'topic/fetchReply',
            payload: {
                commentId : this.props.commentId,
                query : 3
            }
        })

    }



    render () {
        const {loading,postId,topic:{Reply=[]}} = this.props;
        console.log("REply =====>", Reply)
        const {  loadingMore, showLoadingMore, data } = this.state;
        const loadMore = showLoadingMore ? (
          <div style={{ textAlign: 'center', marginTop: 12, height: 32, lineHeight: '32px' }}>
            {loadingMore && <Spin />}
            {!loadingMore && Reply.length > 0 && <a size="small" onClick={this.onLoadMore}>View more replys <Icon type="reload" /></a>}
          </div>
        ) : null;
        console.log('commentList123',Reply,loading)
        return(
            <Card bodyStyle={{padding:10,margin:0}} style={{width:'100%',marginTop:0,marginBottom:0,borderTop:'none'/*'1px solid #1890ff'*/,borderLeft:'none',borderRight:'none',borderBottom:'none'}}>
                {Reply!== null && Reply.length > 0 ?[
              
                     <List
                        className="demo-loadmore-list"
                        loading={loading}
                        itemLayout="horizontal"
                        loadMore={loadMore}
                        dataSource={Reply}
                        renderItem={item => (
                          <ReplyItem item={item} postId={postId} />
                        )}
                    />
                ]:[null]}
            </Card>
        )
    }
}
