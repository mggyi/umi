import React, {Component} from 'react'
import PropTypes from 'prop-types';
import QueueAnim from 'rc-queue-anim';

import {Card,List,Avatar, Button, Spin,Collapse,Row,Col,Icon} from 'antd'
import { connect } from 'dva';
import {
    Editor,
    Block,
    createEditorState,
    rendererFn,
} from 'medium-draft';
import {
    setRenderOptions,
    blockToHTML,
    entityToHTML,
    styleToHTML,
  } from 'medium-draft/lib/exporter';
import 'draft-js/dist/Draft.css';
import 'medium-draft/lib/index.css';
import Moment from 'react-moment';
import CommentItem from './CommentItem';


@connect(({ topic,loading }) => ({
    topic,
    loading: loading.effects['topic/fetchComment'],
}))
export default class CommentList extends Component {
    state = {
        count : 1,
        loadingMore: false,
        showLoadingMore: true,
        data: [],
        commentBoxShow: false,
    }
    onLoadMore = () => {
        this.setState({
          loadingMore: true,
        });
        this.setState((prevState) => {
            return {count: prevState.count + 1};
        });
        this.props.dispatch({
            type: 'topic/fetchComment',
            payload: {
                postId : this.props.postId,
                query : this.state.count * 3
            }
        })
        this.setState({
            loadingMore: false,
          });
      }

    componentDidMount(){
        this.setState((prevState) => {
            return {count: prevState.count + 1};
        });
        this.props.dispatch({
            type: 'topic/fetchComment',
            payload: {
                postId : this.props.postId,
                query : 3
            }
        })

    }



    render () {
        const {loading,postId,topic:{Comment=[]}} = this.props;
        const {  loadingMore, showLoadingMore, data } = this.state;
        const loadMore = showLoadingMore ? (
          <div style={{ textAlign: 'center', marginTop: 12, height: 32, lineHeight: '32px' }}>
            {loadingMore && <Spin />}
            {!loadingMore && Comment.length>0 && <a size="small" onClick={this.onLoadMore}>View more comments <Icon type="reload" /></a>}
          </div>
        ) : null;
        console.log('commentList123',Comment,loading)
        return(
            <Card bodyStyle={{padding:10,margin:0}} style={{width:'100%',marginTop:10,marginBottom:10,/*borderTop:'3px solid #1890ff',*/borderTop:'none',borderLeft:'none',borderRight:'none', borderBottom:'none'}}>
                {Comment!== null && Comment.length>0 ?[
              
                     <List
                        className="demo-loadmore-list"
                        loading={loading}
                        itemLayout="horizontal"
                        loadMore={loadMore}
                        dataSource={Comment}
                        renderItem={item => (
                          <CommentItem item={item} postId={postId} />
                        )}
                    />

                ]:[null]}
            </Card>
        )
    }
}
