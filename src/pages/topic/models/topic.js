import {fetchTopicById,addLike,
    addViewCount,addStar,createComment,
    fetchComment,createReply,fetchReply} from '../services/topic'
import {setSessionStorage,getSessionStorage,isLogin} from '../../../routes/authority'
import { routerRedux } from 'dva/router';
import Redirect from 'umi/redirect';

import {message} from 'antd'
export default {

    namespace: 'topic',
  
    state: {
        Comment: [],
        Reply: [],
        ReplyCount: 0,
        CreatedAt: '',
        ModifiedAt: '',
        Post: {},
        Tag: [],
        Title: '',
        TitleImg: '',
        Description: '',
        User: [],
        Uuid: '',
        Stared: false,
        StarCount: 0,
        Liked: false,
        LikeCount: 0,
        CommentCount: 0,
        CommentBox: {},
        topic: {},
    },
    effects: {
        *fetchReply({payload},{call,put}) {
            const response = yield call(fetchReply,payload)
            console.log("fetchReply",response)
            if(response.data.message !== "ok"){
                message.error("Cannot get topic data!!!")
            }else{
                yield put ({
                    type: 'saveReply',
                    payload: {
                        res: response.data.replyList,
                        ReplyCount: response.data.replyCount,
                    }
                });
            }
        },
        *fetchComment({payload},{call,put}) {
            const response = yield call(fetchComment,payload)
            console.log("fetchComment",response)
            if(response.data.message !== "ok"){
                message.error("Cannot get topic data!!!")
            }else{
                yield put ({
                    type: 'saveComment',
                    payload: {
                        res: response.data.commentList,
                        CommentCount: response.data.commentCount,
                    }
                });
            }
        },
        *createComment({payload},{call,put}) {
            const response = yield call(createComment,payload)
            console.log("createComment",response)
            if(response.data.message !== "ok"){
                message.error("Cannot get topic data!!!")
            }else{
                message.info("Your comment successfully posted!")
                yield put ({
                    type: 'saveComment',
                    payload: {
                        res: response.data.commentList,
                        CommentCount: response.data.commentCount,
                    }
                });
            }
        },
        *createReply({payload},{call,put}) {
            const response = yield call(createReply,payload)
            console.log("createReply",response)
            if(response.data.message !== "ok"){
                message.error("Cannot get topic data!!!")
            }else{
                message.info("Your comment successfully posted!")
                yield put ({
                    type: 'saveReply',
                    payload: {
                        res: response.data.replyList,
                        ReplyCount: response.data.replyCount,
                        CommentId : payload.commentId,
                    }
                });
            }
        },
        *fetchTopicById({payload},{call,put}) {
            const response = yield call(fetchTopicById,payload)
            console.log('getSessionStorage',getSessionStorage())
            
            console.log("myresponse",response)
            if(response.data.message !== "ok"){
                message.error("Cannot get topic data!!!")
            }else{
                yield put ({
                    type: 'saveTopic',
                    payload: {
                        res: response.data,
                    }
                });
                if(getSessionStorage() !== payload.topicid) {
                    addViewCount(payload.topicid)
                    setSessionStorage(payload.topicid)
                }
            }

        },
        *fetchTopicByIdUnmount(_,{put}){
            console.log("Clleee mmee")
            yield put ({
                type: 'cleartopic',
            })
        },
        *like({payload}, { call, put }) {
            var chkLogin = isLogin()   
            if(chkLogin){
                const response = yield call(addLike,payload)
                yield put ({
                    type: 'addLike',
                    payload: {
                        res: response.data,
                    }
                })
            } else {
                yield put(routerRedux.push(`/login?query=${payload.pathname.substr(1)}`));
            }
        },
        *star({payload}, { call, put }) {
            var chkLogin = isLogin()   
            if(chkLogin){
                const response = yield call(addStar,payload)
                yield put ({
                    type: 'addStar',
                    payload: {
                        res: response.data,
                    }
                });
            }else{
                yield put(routerRedux.push(`/login?query=${payload.pathname.substr(1)}`));
            }
            
        },
    },

    reducers: {
        saveComment(state,{payload}) {
            console.log("SaveComment",payload)
            return {
                ...state,
                Comment:payload.res,
                CommentCount:payload.CommentCount,
            }
        },
        saveReply(state,{payload}) {
            console.log("SaveReply",payload)
            console.log("State...",state.Comment)
            var newComment = state.Comment

            for (var i in newComment) {
                if (newComment[i].Uuid == payload.CommentId) {
                    newComment[i].ReplyCount = payload.ReplyCount;
                   break;
                }
            }
            console.log("NEWCOMMENT",newComment)

            return {
                ...state,
                Reply: payload.res,
                Comment: newComment,
            }
        },
        addLike(state,{payload}) { 
            return {
                ...state,
                LikeCount: payload.res.like,
                Liked: payload.res.liked
            }
        },
        addStar(state,{payload}) { 
            return {
                ...state,
                StarCount: payload.res.star,
                Stared: payload.res.stared
            }
        },
        saveTopic(state,{payload}) {
            // const commentSort = payload.res.topic.Comment.sort(function(a, b) {
            //     var dateA = new Date(a.CreateAt), dateB = new Date(b.CreateAt);
            //     return dateB - dateA;
            // });
            // console.log('settopic',payload.res.topic.Title)
            // console.log('comment',JSON.stringify(payload.res.topic.Comment))
            return { 
                ...state,
                Comment: payload.res.topic.Comment,
                CreatedAt: payload.res.topic.CreateAt,
                ModifiedAt: payload.res.topic.ModifiedAt,
                Post:  payload.res.topic.Post,
                Tag:  payload.res.topic.Tag,
                Title: payload.res.topic.Title,
                TitleImg :payload.res.topic.TitleImg,
                Description: payload.res.topic.Description,
                User: payload.res.topic.User,
                Uuid: payload.res.topic.Uuid,
                StarCount: payload.res.topic.StarCount,
                LikeCount: payload.res.topic.LikeCount,
                CommentCount: payload.res.topic.CommentCount,
                Stared: payload.res.topiclike.Stared,
                Liked: payload.res.topiclike.Liked,
                // Comment: payload.res.message === "ok" ? commentSort: [],
                // CommentBox:payload.res.topic.Comment.map(obj => {
                //     var box = {};
                //     box[obj.Uuid] = false;
                //     return box;
                // }),
                
            };
        },


        cleartopic(state,{_}){
            return {
                ...state,
                Uuid: '',
                Title: '',
                Post: '',
                Tag: [],
                CreatedAt: '',
                ModifiedAt: '',        
                Comment: [],
                CommentBox: {},
                message: '',
                topic: {},
                loading: false,
            }
        }
    },
  
};
  