import React from 'react'
import { Card ,Avatar,Row,Col,Spin, Tooltip} from 'antd';
import { connect } from 'dva';
import * as FontAwesome from 'react-icons/lib/fa'
import styles from './index.less'
import './index.css'
import DescriptionList from '../../components/DescriptionList';
const { Description } = DescriptionList;
@connect(({ users,profile,loading }) => ({
  users,profile,
  loading: loading.models.users,
}))
class Users extends React.Component {

    componentDidMount() {
        this.props.dispatch({
        type: 'users/userById',
            payload: {
                userid: this.props.match.params.id,
            }
        })
    }


    render() {
      console.log("user props", this.props)
        return (
            <div className={styles.normal}>
              {this.props.loading ? [
                <div style={{textAlign: 'center'}}>
                    <Spin size="large" />
                </div>
              ]:[
                <>
                <Card bordered={false} lg={12} sm={12} style={{marginBottom:'10px'}}>
                    <Row style={{marginBottom: '10px',}} type="flex" justify="center" align="top">
                        <Col>
                        <Card hoverable >
                            <Avatar shape="square" style={{width:'200px',height:'200px'}} 
                              src={this.props.users.avatar}
                             />
                             </Card>
                        </Col>
                    </Row>
                    <Row type="flex" justify="center" align="top">
                        <Col style={{textAlign: 'center'}}>
                            <div className={styles.displayDiv}>
                                <Tooltip title="Display Name">
                                    <Card hoverable bodyStyle={{padding:'8px'}} style={{width:'250px',borderRadius:'1rem',marginBottom:'5px'}}>
                                        <FontAwesome.FaUser className={styles.displayIconName}/>
                                        <span className={styles.displayTextName}>
                                            {this.props.users.name}
                                        </span>
                                    </Card>
                                </Tooltip>
                            </div>

                            {this.props.users.email !== ''?[
                                <div className={styles.displayDiv}>
                                    <Tooltip title="Email">
                                    <Card hoverable bodyStyle={{padding:'8px'}} style={{borderRadius:'1rem',marginBottom:'5px'}}>
                                        <FontAwesome.FaEnvelope className={styles.displayIcon} />
                                        <span className={styles.displayText}>
                                            {this.props.users.email}
                                        </span>
                                    </Card>
                                    </Tooltip>
                                </div>
                            ]:[
                                null
                            ]}


                            {new Date(this.props.users.birthday).getFullYear() !== '0001'?[
                                <div className={styles.displayDiv}>
                                    <Tooltip title="Birth Date">
                                    <Card hoverable bodyStyle={{padding:'8px'}} style={{borderRadius:'1rem',marginBottom:'5px'}}>
                                        <FontAwesome.FaCalendar className={styles.displayIcon} />
                                        <span className={styles.displayText}>
                                           {new Date(this.props.users.birthday).toLocaleDateString("en-US")}
                                        </span>
                                    </Card>
                                    </Tooltip>
                                </div>
                            ]:[
                                null
                            ]}


                            {this.props.users.gender!==''?[
                                <div className={styles.displayDiv}>
                                    <Tooltip title="Gender">
                                    <Card hoverable bodyStyle={{padding:'8px'}} style={{borderRadius:'1rem',marginBottom:'5px'}}>
                                        <FontAwesome.FaTransgenderAlt className={styles.displayIcon} />
                                        <span className={styles.displayText}>
                                            {this.props.users.gender}
                                        </span>
                                    </Card>
                                    </Tooltip>
                                </div>
                            ]:[
                                null
                            ]}


                            {this.props.users.country.length!== 0?[
                            <div className={styles.displayDiv}>
                                <Tooltip title="Country">
                                <Card hoverable bodyStyle={{padding:'8px'}} style={{borderRadius:'1rem',marginBottom:'5px'}}>
                                    <FontAwesome.FaGlobe className={styles.displayIcon} />
                                    <span className={styles.displayText}>
                                        {this.props.users.country[0]}/{this.props.users.country[1]}
                                    </span>
                                </Card>
                                </Tooltip>
                            </div>
                            ]:[
                                null
                            ]}


                            {this.props.users.bio!==''?[
                                <div className={styles.displayDiv}>
                                    <Tooltip title="Bio">
                                    <Card hoverable bodyStyle={{padding:'8px'}} style={{borderRadius:'1rem',marginBottom:'5px'}}>
                                        <FontAwesome.FaInfoCircle className={styles.displayIcon} />
                                        <span className={styles.displayText}>
                                            {this.props.users.bio!==''?this.props.users.bio:"-"}
                                        </span>
                                    </Card>
                                    </Tooltip>
                                </div>
                            ]:[
                                null
                            ]}

                        </Col>
                    </Row> 
                </Card >
                <Card style={{ marginBottom: 10 }}>
                    <DescriptionList size="large" title="Activities" style={{ marginBottom: 32 }}>
                        <Description term="Post" style={{fontSize:'18px',fontWeight:'500px',fontFamily:'Roboto'}}>{this.props.users.post}</Description>
                        <Description term="Ask" style={{fontSize:'18px',fontWeight:'500px',fontFamily:'Roboto'}}>{this.props.users.ask}</Description>
                        <Description term="Answer" style={{fontSize:'18px',fontWeight:'500px',fontFamily:'Roboto'}}>{this.props.users.answer}</Description>
                    </DescriptionList>
                </Card>
                </>

              ]}
            </div>
        )
    }
}
export default Users


