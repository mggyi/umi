import {fetchUser,fetchUserslist} from '../services/users'
// import { routerRedux } from 'dva/router';
export default {

    namespace: 'users',
  
    state: {
        users : [],
        count: 0,
        name: '',
        avatar:'',
        email: '',
        birthday: '',
        gender: '',
        country: [],
        bio: '',
        userid: '',
        post: '',
        ask: '',
        answer: '',
    },
    effects: {
        *fetchUserslist({payload},{call,put}){
            const response = yield call(fetchUserslist,payload)
            yield put({ 
                type: 'save',
                payload: {
                    res: response.data,
                }
            });
        },
        *userById({payload},{call,put}) {
            const response = yield call(fetchUser, payload)
            yield put({
                type: 'reduUserById',
                payload: {
                    res: response.data,
                }
            })
            // yield put(routerRedux.push(`/users/${payload.userid}`));

        }
    },
  
    reducers: {
      save(state, {payload}) {
        return { ...state,
            count: payload.res.count,
            users : payload.res.userList,
        };
      },
      reduUserById(state, {payload}) {
          return { ...state,
            name:payload.res.user.displayName,
            avatar:payload.res.user.avatar,
            email: payload.res.user.email,
            birthday: payload.res.user.birthday,
            gender: payload.res.user.gender,
            country: payload.res.user.country,
            bio: payload.res.user.bio,
            userid:payload.res.user.userid,
            post: payload.res.user.post,
            ask: payload.res.user.ask,
            answer: payload.res.user.answer,
          }
      }
    },
  
};
  