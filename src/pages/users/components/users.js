import React from 'react'
import {Pagination, List, Card ,Avatar,Spin} from 'antd';
import { connect } from 'dva';
import { routerRedux } from 'dva/router';
import styles from './index.css'
const { Meta } = Card;

@connect(({ users,loading}) => ({
  users,
  loading: loading.effects['userslist/fetchUserslist'],
}))
class UsersList extends React.Component {


    onChange=(e)=>{
      this.props.dispatch({
        type: 'users/fetchUserslist',
        payload: {
          query: e-1,
        }
      })
    }
    cardOnClick = (id,e) => {
      this.props.dispatch(routerRedux.push(`users/${id}`));
    }
    UNSAFE_componentWillMount(){
      this.props.dispatch({
        type: 'users/fetchUserslist',
        payload: {
          query: 0,
        }
      })
    }

    render() {
      const {loading, users} = this.props;
      console.log(users)

        return (
          <div className={styles.normal}>
               {loading? 
               [
                 <div style={{textAlign: 'center'}}>
                     <Spin size="large" />
                 </div>
               ]:[
                 
                  <List
                    dataSource={users.users}
                    grid={{ gutter: 16, xs: 1, sm: 2, md: 3, lg: 4, xl: 5, xxl: 6 }}
                    size="large"
                    renderItem={item => (
                        <List.Item key={item.uuid}>
                            <Card hoverable onClick={(e) => this.cardOnClick(item.uuid, e)}>
                            <Meta
                                avatar={<Avatar src={item.avatar} shape="square"  />}
                                title={item.displayName}
                                />
                            </Card>
                        </List.Item>
                    )}
                  />
              ]
              }
              <Pagination pageSize={20}   total={users.count} onChange={this.onChange}  />
          </div>
         )
    }
}

export default UsersList;


