import React from 'react';
import {Tabs,Card } from 'antd';
import styles from  './index.less'
import RegisterForm from './register'
import LoginForm from './login'
import { connect } from 'react-redux'
import { routerRedux } from 'dva/router';
import './index.css'
const TabPane = Tabs.TabPane;

@connect(({user}) => ({
    user,
}))
class LoginBox extends React.Component {

    componentWillMount() {
        if(this.props.location.query.oauth_verifier){
            this.props.dispatch(routerRedux.push('/'));
        }
        
    }

    render() {
        
        return (
            <Card className={styles.main}>
                <div className="card-container">
                    <Tabs 
                        animated={false}
                        defaultActiveKey="1"
                        className={styles.tabs}>
                        <TabPane tab={<b>Login</b>} key="1">
                            <LoginForm />
                        </TabPane>
                        <TabPane tab={<b>Register</b>} key="2">
                            <RegisterForm />
                        </TabPane>
                    </Tabs>
                </div>
            </Card>
       )
    }
}

export default LoginBox;