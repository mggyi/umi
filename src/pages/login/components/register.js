import React from 'react';
import axios from 'axios';
import {Form,Input,Icon,Button} from 'antd'
import styles from  './index.less'
import { connect } from 'react-redux'
import api from '../../../api'
import Fingerprint from 'fingerprintjs2';

const fpInstance = new Fingerprint();

const FormItem = Form.Item;


@connect(({user,loading}) => ({
    user,
    loading: loading.effects['user/userNameCheck'],
}))
class Register extends React.Component {
    constructor(props){
        super(props)

        this.textInput = null;

        this.setTextInputRef = element => {
          this.textInput = element;
        };
    
        this.focusTextInput = () => {
          // Focus the text input using the raw DOM API
          if (this.textInput) this.textInput.focus();
        };
    }
    state = {
        fingerprintId: '',
        confirmDirty: false,
        userNameStatus: '',
        userNameMessage: '',
    };
    compareToFirstPassword = (rule, value, callback) => {
        const form = this.props.form;
        if (value && value !== form.getFieldValue('password')) {
          callback('Two passwords that you enter is inconsistent!');
        } else {
          callback();
        }
    }

    validateToNextPassword = (rule, value, callback) => {
        const form = this.props.form;
        if (value && this.state.confirmDirty) {
          form.validateFields(['confirm'], { force: true });
        }
        callback();
    }
    compareToFirstPassword = (rule, value, callback) => {
        const form = this.props.form;
        if (value && value !== form.getFieldValue('password')) {
          callback('Two passwords that you enter is inconsistent!');
        } else {
          callback();
        }
    }
    
    handleConfirmBlur = (e) => {
        const value = e.target.value;
        this.setState({ confirmDirty: this.state.confirmDirty || !!value });
    }
    
    handleSubmit = (e) => {
        e.preventDefault();

        // console.log(e.target.userName.value)            
            if(e.target.userName.value === ''){
                this.setState({
                    userNameStatus: "error",
                    userNameMessage: "Please input User Name!"
                })

                // console.log('message',this.state.userNameMessage)
            }
            
            
        this.props.form.validateFields((err, values) => {
          if (!err) {

            this.props.dispatch({
                type: 'user/register',
                payload: {
                    userName: values.userName,
                    displayName: values.displayName,
                    email: values.email,
                    password: values.password,
                    clientId: this.state.fingerprintId,
                }
            })
          }
        });
    }
    mysetState = () => {
            this.setState({
                userNameStatus:'error',
                userNameMessage: 'Please input User Name!'
            }, () => {
            //   console.log(this.state.userNameMessage, 'userNameMessage');
            }); 
    }
    mysetStateinvalide = () => {
            this.setState({
                userNameStatus:'error',
                userNameMessage: 'User name must be letter ,number and at least 3 characters!'
            }, () => {
            //   console.log(this.state.userNameMessage, 'userNameMessage');
            }); 
    }
    mysetStateinvalideclear = () => {
            this.setState({
                userNameStatus:'success',
                userNameMessage: ''
            }, () => {
            //   console.log(this.state.userNameMessage, 'userNameMessage');
            }); 
    }
    userNameOnChange = (e) => {
        var userName = e.target.value
        // console.log('userName',userName)
        if(userName === ""){
            setTimeout(this.mysetState, 500);
        }else{
            if(userName.match(/^([a-zA-Z0-9]{3,})$/)){
                setTimeout(this.mysetStateinvalideclear, 0);
                //   console.log('check',userName)
                 axios.post(`${api}/userNameCheck`, {
                    "userName": e.target.value,
                  })
                  .then(this.userNameStatusChange)
                  .catch(function (error) {
                    // console.log('error',error);
                  });  
            }else{
                // console.log('nook',userName)
                setTimeout(this.mysetStateinvalide, 0);
            }
         
        }
    }
    userNameStatusChange = (response) =>{
        this.setState({
            userNameStatus: response.data.userNameAvaliable?"success":"error",
            userNameMessage: response.data.userNameAvaliable?"":"User Name is already exist!",
        })
    }

    emailOnBlur = (e) => {
        // console.log(e.target.value)
    }

    componentWillMount(){
		fpInstance.get((result)=> {
            console.log("fingerPrintId", result)
            this.setState({
                fingerprintId: result
            })
		});


	}
    componentDidMount() {
        // autofocus the input on mount
        this.focusTextInput();
      }
    render() {
        const { getFieldDecorator } = this.props.form;
        // console.log(this.props.user.userNameStatus)
        return (
            <Form onSubmit={this.handleSubmit} className={styles.loginform}>
                <FormItem 
                  hasFeedback
                  validateStatus={this.state.userNameStatus}
                  help={this.state.userNameMessage}
                  >
                    {getFieldDecorator('userName', {
                        rules: [
                            { required: true, message: 'Please input your User Name' },
                        ],

                    })(
                        <Input  ref={this.setTextInputRef} size="large" onChange={this.userNameOnChange} prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="User Name" />
                    )}
                </FormItem>
                <FormItem>
                    {getFieldDecorator('displayName', {
                        rules: [
                            { required: true, message: 'Please input your Display Name' },
                            { pattern: '[a-zA-Z]{3,}', message: 'Display name at least 3 or more characters!'}
                        ],
                    })(
                        <Input size="large" title="Minimum 5 letters" prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Display Name" />
                    )}
                </FormItem>
                <FormItem>
                    {getFieldDecorator('email', {
                        rules: [
                            { required: true, message: 'Please input your email!' },
                            { type: 'email', message: 'The input is not valid E-mail!'},
                        ],
                    })(
                        <Input size="large" onBlur={this.emailOnBlur} prefix={<Icon type="mail" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Email" />
                    )}
                </FormItem>
                

                
                <FormItem>
                    {getFieldDecorator('password', {
                        rules: [
                            { required: true, message: 'Please input your Password!' },
                            { validator: this.validateToNextPassword,},
                            { pattern: "(?=.*[a-z])(?=.*[A-Z]).{6,}", message:"Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters"}
                        ],
                    })(
                        <Input size="large"  prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder="Password" />
                    )}
                </FormItem>
                <FormItem>
                    {getFieldDecorator('confirm', {
                        rules: [
                            { required: true, message: 'Please input your Password!' },
                            { validator: this.compareToFirstPassword, }
                        ],
                    })(
                        <Input size="large"  prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder="Comfirm Password" onBlur={this.handleConfirmBlur} />
                    )}
                </FormItem>
                <FormItem>
                    <Button size="large" type="primary" htmlType="submit" className={styles.loginformbutton}>
                        Register
                    </Button>
                </FormItem>
            </Form>
        )
    }
}

const RegisterForm = Form.create()(Register);

const mapStateToProps = (state) => {
  return {
        user: state.user,
        loading: state.loading.global,
    };
}

export default connect(mapStateToProps)(RegisterForm)