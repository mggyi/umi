import React from 'react';
import {Form,Input,Icon,Button,Checkbox,Row,Col,Tooltip} from 'antd'
import styles from  './index.less'
import { connect } from 'react-redux'
// import axios from 'axios'
// import tc from './twitter-client.js'
// import qs from 'querystring';
// import { Link, Redirect } from 'react-router-dom';
// import api from '../../../api'


const FormItem = Form.Item;

var GoogleAuth;
// var twitterrequestToken;
@connect(({user}) => ({
    user,
}))

class Login extends React.Component {
    constructor(props){
        super(props)

        this.state ={
            twitterrequestToken: '',
            fbToken:'',
        }
        this.textInput = null;
        this.setTextInputRef = element => {
            this.textInput = element;
        };
      
        this.focusTextInput = () => {
            // Focus the text input using the raw DOM API
            if (this.textInput) this.textInput.focus();
        };
    }

    initGoogleAPI = () => {
    // https://developers.google.com/identity/sign-in/web/reference#gapiauth2clientconfig
    // https://developers.google.com/api-client-library/javascript/samples/samples
    // https://developers.google.com/identity/sign-in/web/backend-auth
        const script = document.createElement('script');
        script.src = 'https://apis.google.com/js/api.js';
        script.onload = () => {

            window.gapi.load('client:auth2', () => {
                window.gapi.client
                    .init({
                        clientId: '266586599022-fk6ujgls8pilf9f836g6s3em0kn50pba.apps.googleusercontent.com',
                        scope: 'profile',
                    })
                .then(() => {
                    GoogleAuth = window.gapi.auth2.getAuthInstance();
                    // Listen for sign-in state changes.
                    // window.gapi.auth2.getAuthInstance().isSignedIn.listen(this.updateSigninStatus);
                    // // Handle the initial sign-in state.
                    // this.updateSigninStatus(window.gapi.auth2.getAuthInstance().isSignedIn.get());
              });
          });
        };
        document.body.appendChild(script);
    };


    // updateSigninStatus = (isSignedIn) => {
    //     // When signin status changes, this function is called.
    //     // If the signin status is changed to signedIn, we make an API call.
    //     if (isSignedIn) {
    //       this.makeApiCall();
    //     }
    // }

    handleGoogleAuthClick = (event) => {

        // console.log(window.gapi.auth2.getAuthInstance().currentUser.get().getAuthResponse().id_token)
        // Ideally the button should only show up after gapi.client.init finishes, so that this
        // handler won't be called before OAuth is initialized.

        //GoogleAuth.signIn().then(this.makeApiCall);
        if(GoogleAuth != null){
            // console.log(GoogleAuth)
            GoogleAuth.grantOfflineAccess().then(this.googleSignInCallback);    
        }
        
    }

    googleSignInCallback = (authResult) => {
        // console.log('authResultcode',authResult['code'])
        // console.log("GoogleAuth",GoogleAuth)
        
        // var id_token = window.gapi.auth2.getAuthInstance().currentUser.get().getAuthResponse().id_token
        // console.log('google Call')
        this.props.dispatch({
            type: 'user/gLogin',
            payload: {
                id_token:authResult['code'],
            }
        })
    }

    handleSignOutClick = (event) => {
        window.gapi.auth2.getAuthInstance().signOut(this.makeApiCall);
    }

    // makeApiCall = () => {
    //     var id_token = window.gapi.client.getToken().id_token;
    //     console.log('makecall',id_token)
    //     this.props.dispatch({
    //         type: 'user/gLogin',
    //         payload: {
    //             id_token:id_token,
    //         }
    //     })
    // }


    initFacebookAPI = () => {
        window.fbAsyncInit = function () {
            window.FB.init({
                appId : '1913950198626788',
                cookie: true,
                xfbml: true,
                version: 'v3.0'
            });

            window.FB.Event.subscribe('auth.statusChange',(response) => {
                if(response.authResponse) {
                    // console.log(response)
                }else{
                    // console.log(response)
                }
            });
        }.bind(this);
        (function(d, s, id){
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {return;}
            js = d.createElement(s); js.id = id;
            js.src = "https://connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
          }(document, 'script', 'facebook-jssdk'));
    }

    // initTwitterAPI = () => {
    //     axios.get(`${api}/gettoauthtoken`)
    //         .then((result) => {          
    //         this.setState({
    //             twitterrequestToken: result.data.twitterrequestToken
    //         })
    //         console.log('twitter',twitterrequestToken)
    //     })
    // }

    handleSubmit = (e) => {
        e.preventDefault();
        this.set
        
        this.props.form.validateFields((err, values) => {
          if (!err) {
            
            this.props.dispatch({
                type: 'user/login',
                payload: {
                    email: values.email,
                    password: values.password,
                    remember: values.remember,
                    query: window.g_history.location.query.query,
                }
            })
          }
        });
    }

    //https://developers.google.com/identity/sign-in/web/server-side-flow
    // googleSignInCallback = (authResult)=> {
    //     console.log(authResult)
    //     if (authResult['code']) {

    //         this.props.dispatch({
    //             type: 'user/googleSign',
    //             payload : {
    //                 code : authResult['code']
    //             }
    //         })
    //     } else {
    //       alert("Google Sign Error")
    //     }
    // }


    // handleGoogleAuthClick = () => {
    //     // console.log(GoogleAuth)
    //     // GoogleAuth.grantOfflineAccess().then(this.googleSignInCallback);
    //     // console.log(GoogleAuth.AuthResponse())
    //     window.gapi.auth2.getAuthInstance().signIn();
    // }

    handleFacebookAuthClick = () => {
        // console.log("click", this.props)
        window.FB.login(this.statusChangeCallback);

    }
    statusChangeCallback = (response) => {
        // console.log('callback',response)
        // console.log('callback',response.authResponse.accessToken)
        // console.log('callback',response.authResponse.userID)
        this.props.dispatch({
            type: 'user/fLogin',
            payload: {
                idtoken: response.authResponse.accessToken,
                userId: response.authResponse.userID,
            }
        })
    }
     
    componentWillMount() {
        this.initGoogleAPI();
        this.initFacebookAPI();
    }
    componentDidMount(){
        // autofocus the input on mount
        this.focusTextInput();
    }

    render() {
        const { getFieldDecorator } = this.props.form;
        console.log(this.props)

        return (
            <Form onSubmit={this.handleSubmit} className={styles.loginform}>
                <FormItem>
                    {getFieldDecorator('email', {
                        rules: [
                            { required: true, message: 'Please input your username or email!' }
                        ],
                    })(
                        <Input ref={this.setTextInputRef} size="large" prefix={<Icon type="mail" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="User Name or Email" />
                    )}
                    </FormItem>
                    <FormItem>
                    {getFieldDecorator('password', {
                        rules: [{ required: true, message: 'Please input your Password!' }],
                    })(
                        <Input size="large"  prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder="Password" />
                    )}
                    </FormItem>
                    <FormItem>
                    {getFieldDecorator('remember', {
                        valuePropName: 'checked',
                        initialValue: true,
                    })(
                        <Checkbox>Remember me</Checkbox>
                    )}
                    <a className={styles.loginformforgot} href="">Forgot password</a>
                    <Button size="large" type="primary" htmlType="submit" className={styles.loginformbutton}>
                        Log in
                    </Button>
                </FormItem>
                <Row type="flex" justify="center" align="top">
                    <Col>
                        {/* <Button size="large" onClick={this.handleFacebookAuthClick}  className={styles.facebookbutton}>
                            <Icon style={{ padding: 5,fontSize: 24, color: '#48649f' }}type="facebook" />
                        </Button>
                        
                        <Button size="large"  onClick={this.handleGoogleAuthClick} className={styles.googlebutton}>
                            <Icon style={{ padding: 5,fontSize: 24, color: '#e94a43' }}type="google" />
                        </Button> */}
                        <Tooltip title="Login with Facebook">
                            <Icon onClick={this.handleFacebookAuthClick} style={{ padding: 5, fontSize: 32, color: '#375a95' }} type="facebook" />
                        </Tooltip>
                        <Tooltip title="Login with Google">
                            <Icon onClick={this.handleGoogleAuthClick} style={{ padding: 5,fontSize: 32, color: '#e94a43' }}type="google" />
                        </Tooltip>
                        {/* <Tooltip title="Login with Twitter">
                            <a href={`https://api.twitter.com/oauth/authenticate?oauth_token=${this.state.twitterrequestToken}`}>
                                <Icon style={{ padding: 5,fontSize: 24, color: '#00a0ef' }}type="twitter" /> 
                            </a>
                        </Tooltip> */}
                    </Col>
                </Row>
            </Form>
        )
    }
}

const LoginForm = Form.create()(Login);

const mapStateToProps = (state) => {
  return { user: state.user };
}

export default connect(mapStateToProps)(LoginForm)