import request from '../../../utils/request';
import {GetToken} from '../../../utils/authority';
import api from '../../../api'

export async function createTopic(params) {
    var token = GetToken()
    var body = {
      "tag" :params.tag,
      "title" : params.title,
      "topic" : params.content
    }
    console.log(body)
    var obj = {  
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Origin': '',
        'Authorization':`token ${token}`
      },
      body:JSON.stringify(body)
    }
  
    return request(`${api}/createTopic`, obj);
}

export async function fetchTags() {
  var token = GetToken()
  var obj = {
      method: 'GET',
      headers: {
          'Authorization':`token ${token}`
      }
  }
  return request(`${api}/fetchTags`,obj)
}
