import {createTopic,fetchTags} from '../services/newtopic'
import { routerRedux } from 'dva/router';
import { message } from 'antd';

export default {

    namespace: 'newtopic',
  
    state: {
        TagList: [],
    },
    effects: {
        *createTopic({payload},{call,put}) {
            const response = yield call(createTopic,payload)
            
            if(response.data.message !== "ok"){
                message.error(response.data.message);
            }else{
                message.success("Success!");
                yield put(routerRedux.push(`/topic/${response.data.topicId}`));
            }
            
        },

        *fetchTags({payload},{call,put}) {
            const response = yield call(fetchTags,payload)
            console.log("fetchTags_response",response)
            yield put ({
                type: 'saveTagList',
                payload: {
                    res: response.data,
                }
            });
        },

    },
    reducers: {
        saveTagList(state,{payload}){
            return { 
                ...state,
                TagList: payload.res.tagList,
            }
        },
    },
};
  