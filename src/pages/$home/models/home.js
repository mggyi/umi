import {fetchLatestTopiclist,getTopicByStar,getTopicByView} from '../services/home'
import { routerRedux } from 'dva/router';
export default {
    namespace: 'home',
    state: {
        topiclist:[],
        starlist:[],
        viewlist:[],
        count:0,
    },
    effects: {
        *fetchTopiclist({payload},{call,put}){
            const response = yield call(fetchLatestTopiclist,payload)
            console.log('Call me',response)
            yield put({ 
                type: 'save',
                payload: {
                    res: response,
                }
            });
        },
        *getTopicByStar({payload},{call,put}){
            const response = yield call(getTopicByStar,payload)
            console.log('Stared',response)
            yield put({ 
                type: 'saveTopicByStar',
                payload: {
                    res: response,
                }
            });
        },
        *getTopicByView({payload},{call,put}){
            const response = yield call(getTopicByView,payload)
            console.log('View',response)
            yield put({ 
                type: 'saveTopicByView',
                payload: {
                    res: response,
                }
            });
        },
    },
  
    reducers: {
      save(state, {payload}) {
        const topiclistSort = payload.res.data.topicList.sort(function(a, b) {
            var dateA = new Date(a.CreateAt), dateB = new Date(b.CreateAt);
            return dateB - dateA;
        });
        return { ...state,
          topiclist: topiclistSort,
          count: payload.res.data.count,
        };
      },
      saveTopicByStar(state, {payload}) {
        return { ...state,
            starlist: payload.res.data.topicList,
          };
      },
      saveTopicByView(state, {payload}) {
        return { ...state,
            viewlist: payload.res.data.topicList,
          };
      },
    },
};
  