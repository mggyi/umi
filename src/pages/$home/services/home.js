
import request from '../../../utils/request';
// import {GetToken} from '../../../utils/authority';
import api from '../../../api'

export function fetchTopiclist(params) {
    var obj = {
        method: 'GET',
    }
    return request(`${api}/getlatestpost/${params.query}`,obj);
}

export function fetchLatestTopiclist(params) {
    var obj = {
        method: 'GET',
    }
    return request(`${api}/getLatestTopic/${params.query}`,obj)
}

export function getTopicByStar() {
    var obj = {
        method: 'GET',
    }
    return request(`${api}/getTopicByStar`,obj)
}

export function getTopicByView() {
    var obj = {
        method: 'GET',
    }
    return request(`${api}/getTopicByView`,obj)
}
