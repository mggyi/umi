import React, { Component } from 'react';
import { routerRedux } from 'dva/router';
import { connect } from 'dva';
import { Input } from 'antd';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';

@connect()
export default class SearchList extends Component {
  handleTabChange = key => {
    const { dispatch, match } = this.props;
    switch (key) {
      case '/home/topic':
        dispatch(routerRedux.push(`${match.url}/topic`));
        break;
      case '/home/ask':
        dispatch(routerRedux.push(`${match.url}/ask`));
        break;
      case '/home/review':
        dispatch(routerRedux.push(`${match.url}/review`));
        break;
      default:
        break;
    }
  };

  onSearch = k => {
    const { dispatch ,location} = this.props;
    dispatch(routerRedux.push(`${location.pathname}?search=${k}`));
  }

  render() {
    const tabList = [
      {
        key: '/home/topic',
        tab: 'TOPICS',
      },
      {
        key: '/home/ask',
        tab: 'QUESTIONS',
      },
      {
        key: '/home/review',
        tab: 'REVIEWS'
      }
    ];

    const mainSearch = (
      <div style={{ textAlign: 'center' }}>
        <Input.Search
          placeholder="Search Keyword"
          onSearch={this.onSearch}
          style={{ maxWidth: 422 }}
        />
      </div>
    );

    const { match,  location,children } = this.props;
    console.log('$home',this.props)
    return (
      <PageHeaderLayout
        title="Home"
        content={mainSearch}
        handleFormSubmit={(e)=>{console.log(e)}}
        tabList={tabList}
        tabActiveKey = {location.pathname}
        onTabChange={this.handleTabChange}
      >
        {children}
      </PageHeaderLayout>
    );
  }
}
