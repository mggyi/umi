import {Component} from 'react'
import { connect } from 'dva'
import moment from 'moment';
import styles from './topic.less';
import Media from "react-media";
import { routerRedux } from 'dva/router';
import {Spin, Card,  List, Tag, Icon, Avatar,  Button,Row,Col,Divider } from 'antd';
import {
    Editor,
    Block,
    createEditorState,
    rendererFn,
  } from 'medium-draft';
  import {
    setRenderOptions,
    blockToHTML,
    entityToHTML,
    styleToHTML,
  } from 'medium-draft/lib/exporter';
const { Meta } = Card;


const newBlockToHTML = (block) => {
    const blockType = block.type;
    if (block.type === Block.ATOMIC) {
      if (block.text === 'E') {
        return {
          start: '<figure class="md-block-atomic md-block-atomic-embed">',
          end: '</figure>',
        };
      } else if (block.text === '-') {
        return <div className="md-block-atomic md-block-atomic-break"><hr/></div>;
      }
    }
    return blockToHTML(block);
};
    
const newEntityToHTML = (entity, originalText) => {
    if (entity.type === 'embed') {
      return (
        <div>
          <a>
          </a>
        </div>
      );
    }
    return entityToHTML(entity, originalText);
};


@connect(({topic, home,loading}) => ({
    home,topic,
    loading: loading.effects['home/getTopicByStar'],
}))
export default class StaredTopic extends Component {
    constructor(props){
        super(props)
        this.exporter = setRenderOptions({
            styleToHTML,
            blockToHTML: newBlockToHTML,
            entityToHTML: newEntityToHTML,
        });
    }

    componentWillMount(){
        this.props.dispatch({
          type: 'home/getTopicByStar',
        })
    }

    strip = (post) =>
    {
        var editorState = createEditorState(post)
    
        var text;
        //  = editorState.getCurrentContent().getPlainText();
        var currentContent = editorState.getCurrentContent();
        const eHTML = this.exporter(currentContent);
        var tmp = document.createElement("DIV");
        tmp.innerHTML = eHTML;
        var rt = tmp.textContent || tmp.innerText || "";
        var words=rt.split(" ");
        if(words.length>1){
            text=words.splice(0,20).join(" ");
        }
        return text
    }

    render() {

        const {loading, home:{starlist}} = this.props;
        
        console.log(starlist,loading)

        const IconText = ({ type, text }) => (
            <span>
              <Icon type={type} style={{ marginRight: 8 }} />
              {text}
            </span>
        );
        
        const ListContent = ({ data: {Post,Uuid }}) => {
        return(
            
            <div 
            className={styles.description} style={{paddingLeft:10}}
            >{this.strip(Post)}<b><a onClick={()=>{ 
                                this.props.dispatch(
                                routerRedux.push(`/topic/${Uuid}`)
                                )}}>...more</a></b></div>
    
    
        )
        }
        const ListExtra = ({ data: {User, CreateAt} }) => {
        return(
            <div style={{paddingTop:10,paddingLeft:10}}>
                <Card  bordered={false} style={{background:'transparent'}} bodyStyle={{padding:'0'}}>
                <Meta
                    avatar={<Avatar shape="square" size="small" src={User[0].avatar} style={{padding:0}} />}
                    title={ <a onClick={()=>{
                    this.props.dispatch(
                        routerRedux.push(`/users/${User[0].uuid}`)
                    )
                    }}
                    ><><p className="date">{User[0].displayName}&nbsp; <Icon type="clock-circle-o" />&nbsp;{moment(CreateAt).format('YYYY-MM-DD HH:mm')}</p></>
                </a>}/>
                </Card>  
            </div>
            )
        }
        return (
            <>
                <div class="heading">
                  <h1>Mosted Stared</h1>
                      <div class="sub-heading">
                            <hr/>
                        </div>

                    </div>
    
                <Card
                // style={{marginTop: 24 ,paddingTop:20}}
                bordered={false}
                bodyStyle={{ padding: '0' }}>

                    <List
                        size="large"
                        rowKey="id"
                        loading={loading}
                        itemLayout="vertical"
                        dataSource={starlist}
                        renderItem={item => (
                        <List.Item 
                            style={{padding:0,margin:0,border:'none'}}
                            key={item.id}>
                            <Media query="(max-width: 1200px)">
                                {matches =>
                                    matches ? (
                                        <Row>
                                        <Col style={{maxWidth:200}} span={12}>
                                        {/* {item.TitleImg != "" ? */}
                                            <Card hoverable onClick={()=>{this.props.dispatch(
                                                    routerRedux.push(`/topic/${item.Uuid}`)
                                                    )}} bordered={true} bodyStyle={{padding:0,margin:0}} style={{marginRight:10,border:'1px solid #939393'}} cover={
                                                <div style={{position: 'relative',
                                                        textAlign: 'center',
                                                        color: 'white'}}>
                                                <img src={item.TitleImg !=="" ?item.TitleImg:"https://i.imgur.com/qrJ1JHn.png"} alt="Snow" style={{width:'100%'}}/>
                                                <div style={{width:'100%',textAlign: 'center', background: 'rgba(0,0,0,0.75)', position: 'absolute',bottom: 0}}>
                                                    <IconText size="small" type="star" text={180} />&nbsp;&nbsp;
                                                    <IconText size="small" type="like" text={200} />&nbsp;&nbsp;
                                                    <IconText size="small" type="message" text={160} />&nbsp;
                                                </div>
                                                
                                                </div>
                                            }>
                                            </Card>
                                            {/* :null
                                        } */}
                                        </Col>
                                        <Col>
                                                <a onClick={()=>{ 
                                                    this.props.dispatch(
                                                    routerRedux.push(`/topic/${item.Uuid}`)
                                                    )}}><h4>{item.Title}</h4></a>
                                                {/* <div style={{height:65}}>
                                                <ListContent data={item} />
                                                </div> */}
                                                <ListExtra  data={item} />
                                        </Col>
                                    </Row>
                                    ):(

                                        <Row >
                                        <Col span={24} style={{paddingTop:0,marginTop:0}}>
                                            {/* {item.TitleImg != "" ? */}
                                            <Card hoverable onClick={()=>{this.props.dispatch(
                                                    routerRedux.push(`/topic/${item.Uuid}`)
                                                    )}} style={{width:'100%',padding:0,margin:0}} bodyStyle={{padding:0,margin:0}} cover={
                                                <div style={{position: 'relative',
                                                            textAlign: 'center',
                                                            color: 'white'}}>
                                                    <img src={item.TitleImg !=="" ?item.TitleImg:"https://i.imgur.com/qrJ1JHn.png"} alt="Snow" style={{width:'100%'}}/>
                                                    <div style={{background: 'rgba(0,0,0,0.75)',padding:4, position: 'absolute',top: 0,left: 0,}}>
                                                    <IconText size="small" type="star" text={item.StarCount} />&nbsp;&nbsp;
                                                    {/* <IconText size="small" type="like" text={200} />&nbsp;&nbsp;
                                                    <IconText size="small" type="message" text={160} />&nbsp; */}
                                                    </div>
                                                    
                                                </div>
                                                }>
                                            </Card>
                                            {/* :null
                                            } */}
                                        </Col>
                                        <Col >
                                            <a onClick={()=>{ 
                                                    this.props.dispatch(
                                                    routerRedux.push(`/topic/${item.Uuid}`)
                                                    )}}><h4>{item.Title}</h4></a>
                                                
                                            {/* <ListContent data={item} /> */}
                                            {/* <ListExtra  data={item} /> */}
                                            <Divider style={{margin:2}}/>
                                        </Col>
                                        </Row>
                                        
                                    )
                                }
                            </Media>
                        </List.Item>
                        )}
                    />

            </Card>
            </>
        )

    }


}