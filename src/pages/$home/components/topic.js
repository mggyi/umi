import React from 'react';
import {Row,Col } from 'antd';
import './index.css'
import LatestTopic from './LastedTopic';
import StaredTopic from './StaredTopic';
import MostView from './MostView';
import Media from "react-media";

export default class TopicList extends React.Component{

render(){
  return (
      <Row style={{height:2000}}>
        <Col xs={24} sm={24} md={24} lg={18} style={{maxWidth:900}}>
          <Row>
            <div >
              <LatestTopic/>
            </div>
            
          </Row>
          <Row>
          <Media query="(max-width: 1200px)">
                  {matches =>
                      matches ? (
                        <>
                        <MostView/> 

                        <StaredTopic/> 
                        </>
                        // <LatestTopic/> 
                      ):(
                        null
                      )}
                </Media>
          </Row>
        </Col>
        <Col xs={0} sm={0} md={0}lg={6}>
              <div style={{paddingLeft:30,width:'100%',height:'100vh'}}>
              <Media query="(max-width: 1200px)">
                  {matches =>
                      matches ? (
                        null
                      ):(
                        <div style={{width:'230px'}}>
                          <MostView/>
                          <div style={{height:'300px'}}/>
                          <StaredTopic/>
                        </div>
                        
                      )}
                </Media>
              </div>
        </Col>
      </Row>
    );
  }
}
