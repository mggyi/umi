
export default {

    namespace: 'list',
  
    state: 0,
  
    subscriptions: {
      setup({ dispatch, history }) {
      },
    },
  
    effects: {
      *fetch({ payload }, { call, put }) {
        yield put({ type: 'save' });
      },
      *addAfter1Second(action, { call, put }) {
          
        yield put({ type: 'add' });
      },
    },
  
    reducers: {
      save(state, action) {
        return { ...state, ...action.payload };
      },
      add(state) { return state + 1 },
    },
  
  };
  