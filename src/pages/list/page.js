import React from 'react';
// import router from 'umi/router'
import { Button } from 'antd';
import { connect } from 'dva';
// import { routerRedux } from 'dva/router';


@connect(({list}) => ({
    list,
  }))
class List extends React.Component{
    static isPrivate = true
    render() {
        console.log(this.props)
        const list = this.props.list;
        
        return ( 
            <div>
			<h1>
				List Private Page!!!
			</h1>
            <h1>{list}</h1>
            <Button onClick={() => {
					this.props.dispatch({
                        type: 'list/addAfter1Second',
                    })
				}}>Count</Button>
		</div>
        )
    }
}

export default List;
