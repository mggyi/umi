import React from 'react'
// import ReactDom from 'react-dom'
import { Form,DatePicker,Select, Cascader,Timeline, Icon ,Card ,Avatar,Row,Col,Spin, Input,Tooltip,Button} from 'antd';
import DescriptionList from '../../components/DescriptionList';
import profile from '../../assets/profile.jpeg'
import styles from './index.less'
import { connect } from 'dva';
// import { TransitionGroup, CSSTransition } from "react-transition-group";
import * as FontAwesome from 'react-icons/lib/fa'
import countryOptions from '../../common/counties.js'
import moment from 'moment';
import Cropper from 'react-cropper';
import 'cropperjs/dist/cropper.css';
import MyPost from './mypost';
import Loading from '../../components/Loading'


const FormItem = Form.Item;
const { TextArea } = Input;
const Option = Select.Option;
const { Description } = DescriptionList;
// const { Meta } = Card;
// const gridStyle = {
//     textAlign: 'center',
// };
@connect(({ user,profile,loading }) => ({
    user,profile,
    loading: loading.effects['profile/fetchProfile'],
}))
class Profile extends React.Component {
    constructor(props){
        super(props)
        this.state = {
            loading: false,
        };
    }
    editToggleEdit =() => {
        this.props.dispatch({
            type: 'profile/eidtState'
        })
    }

    componentDidMount(){
		this.props.dispatch({
			type: 'profile/fetchProfile',
		})
	}

    render() {

        
        const {loading, profile } = this.props;
        const {avatar} = profile;
        const name = profile.name;
        const email = profile.email;
        const birthday = profile.birthday;
        const gender = profile.gender;
        const country = profile.country;
        const bio = profile.bio;
        const post = profile.Post;
        const ask = profile.Ask;
        const answer = profile.Answer;
        
        return (
            <div className={styles.normal}>
            {loading ? [
                   <Loading/>
            ]:[
                    <div>
                    <ProfileCardForm
                        editOnClick={this.editToggleEdit}
                        // saveOnClick={this.editToggleEdit}
                        avatar={avatar}
                        name={name} 
                        email={email}
                        birthday={birthday}
                        gender={gender}
                        country={country}
                        bio={bio}
                        editable={this.props.profile.isEditing}/>

                    
                    
                    <Card style={{ marginBottom: 10 }}>
                        <DescriptionList size="large" title="Activities" style={{ marginBottom: 32 }}>
                            <Description term="Post" style={{fontSize:'18px',fontWeight:'500px',fontFamily:'Roboto'}}>{post}</Description>
                            <Description term="Ask" style={{fontSize:'18px',fontWeight:'500px',fontFamily:'Roboto'}}>{ask}</Description>
                            <Description term="Answer" style={{fontSize:'18px',fontWeight:'500px',fontFamily:'Roboto'}}>{answer}</Description>
                        </DescriptionList>
                    </Card>

                    <MyPost />
                    
                    {/* <Card title="Histories">
                    <Timeline>
                        <Timeline.Item>Create a services site 2015-09-01</Timeline.Item>
                        <Timeline.Item>Solve initial network problems 2015-09-01</Timeline.Item>
                        <Timeline.Item dot={<Icon type="clock-circle-o" style={{ fontSize: '16px' }} />} color="red">Technical testing 2015-09-01</Timeline.Item>
                        <Timeline.Item>Network problems being solved 2015-09-01</Timeline.Item>
                    </Timeline>
                    </Card> */}
                    </div>
            ]}
            </div>
        )
    }
}

function mapStateToProps(state, ownProps) {
    return {
      loading: state.loading.global,
      profile: state.profile,
    };
  }

export default connect(mapStateToProps)(Profile);



// function getBase64(img, callback) {
//     const reader = new FileReader();
//     reader.addEventListener('load', () => callback(reader.result));
//     reader.readAsDataURL(img);
//   }
  
//   function beforeUpload(file) {
//     const isJPG = file.type === 'image/jpeg';
//     if (!isJPG) {
//       message.error('You can only upload JPG file!');
//     }
//     const isLt2M = file.size / 1024 / 1024 < 2;
//     if (!isLt2M) {
//       message.error('Image must smaller than 2MB!');
//     }
//     return isJPG && isLt2M;
//   }


// const src = profile;
@connect(({ user }) => ({
    user
}))
class ProfileCard extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            photoUploadLoading: false,
            loading: false,
            imgFile: null,
            src: null,
            cropEnable:true,
        };

    }

    mapPropsToFields(props) {
        return {
            name: Form.createFormField({
            ...props.name,
            value: props.name.value,
          }),
        };
    }
      
    _onChange =(e) => {
        e.preventDefault();
        let files;
        if (e.dataTransfer) {
          files = e.dataTransfer.files;
        } else if (e.target) {
          files = e.target.files;
        }
        const reader = new FileReader();
        reader.onload = () => {
          this.setState({ 
              src: reader.result,
              imgFile: files[0],
             });
        };
        
        reader.readAsDataURL(files[0]);
    }

    _cropImage = () => {
        if (typeof this.refs.cropper.getCroppedCanvas() === 'undefined') {
            return;
        }
        var data = this.refs.cropper.getCroppedCanvas().toDataURL();
        var file = dataURLtoFile(data, 'profile.png');
        this.setState({
            src: this.refs.cropper.getCroppedCanvas().toDataURL(),
            imgFile: file,
            cropEnable: true,
        });
    }

    uploadImage = (img) => {

        return new Promise(
            (resolve, reject) => {
              const xhr = new XMLHttpRequest(); // eslint-disable-line no-undef
              xhr.open('POST', 'https://api.imgur.com/3/image');
              xhr.setRequestHeader('Authorization', 'Client-ID c2f926d5f0b58e1');
              const data = new FormData(); // eslint-disable-line no-undef
              data.append('image', img);
              xhr.send(data);
              xhr.addEventListener('load', () => {
                const response = JSON.parse(xhr.responseText);
                resolve(response);  

              });
              xhr.addEventListener('error', () => {
                const error = JSON.parse(xhr.responseText);
                reject(error);
              });
            },
          )
    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.setState({ photoUploadLoading: true });
        this.props.form.validateFields((err, values) => {
          if (!err) {

                if(this.state.imgFile) {
                    this.uploadImage(this.state.imgFile)
                        .then(response => {
                            
                            this.setState({
                                src:response.data.link,
                                photoUploadLoading: false,
                            })
                            this.props.dispatch({
                                type: 'profile/updateProfile',
                                payload: {
                                    avatar: this.state.src,
                                    userid: this.props.userid,
                                    displayName: values.displayName,
                                    email: values.email,
                                    birthday: values.birthday,
                                    gender: values.gender,
                                    country: values.country,
                                    bio: values.bio,
                                }
                            })
                            // console.log("call here")
                            this.props.dispatch({
                                type: 'user/updateProfilePic',
                            })
                        })
                }else{
                    this.props.dispatch({
                        type: 'profile/updateProfile',
                        payload: {
                            avatar: this.props.avatar,
                            userid: this.props.userid,
                            displayName: values.displayName,
                            email: values.email,
                            birthday: values.birthday,
                            gender: values.gender,
                            country: values.country,
                            bio: values.bio,
                        }
                    })
                    
                           this.props.dispatch({
                                type: 'user/updateProfilePic',
                            })
                    this.setState({ photoUploadLoading: false });
                }
 
            }
            
    });
}

componentWillMount(){
    
    this.setState({
        src: this.props.avatar
    })
}

    render() {

        const { getFieldDecorator } = this.props.form;

        
        if(!this.props.editable){
            return (
                <Card bordered={false} lg={12} sm={12} style={{marginBottom:'10px'}}>
                    <Row Row type="flex" justify="end" style={{paddingBottom:40}}>
                        <Col>
                            <Tooltip title="Edit Profile">
                                <Button type="primary" icon="edit" onClick={this.props.editOnClick} />
                            </Tooltip>
                        </Col>
                    </Row>
                    <Row style={{marginBottom: '10px',}} type="flex" justify="center" align="top">
                        <Col>
                            <Avatar shape="square" style={{width:'200px',height:'200px'}} src={this.props.avatar} />
                        </Col>
                    </Row>
                    <Row type="flex" justify="center" align="top">
                        <Col style={{textAlign: 'center'}}>
                            <div className={styles.displayDiv}>
                                <Tooltip title="Display Name">
                                    <span className={styles.displayTextName}>
                                        {this.props.name}
                                    </span>
                                </Tooltip>
                            </div>

                            {this.props.email !== ''?[
                                <div className={styles.displayDiv}>
                                    <Tooltip title="Email">
                                        <FontAwesome.FaEnvelope className={styles.displayIcon} />
                                        <span className={styles.displayText}>
                                            {this.props.email}
                                        </span>
                                    </Tooltip>
                                </div>
                            ]:[
                                null
                            ]}


                            {new Date(this.props.birthday).getFullYear() !== '0001'?[
                                <div className={styles.displayDiv}>
                                    <Tooltip title="Birth Date">
                                        <FontAwesome.FaCalendar className={styles.displayIcon} />
                                        <span className={styles.displayText}>
                                           {new Date(this.props.birthday).toLocaleDateString("en-US")}
                                        </span>
                                    </Tooltip>
                                </div>
                            ]:[
                                null
                            ]}
 

                            {this.props.gender!==''?[
                                <div className={styles.displayDiv}>
                                    <Tooltip title="Gender">
                                            <FontAwesome.FaTransgenderAlt className={styles.displayIcon} />
                                            <span className={styles.displayText}>
                                                {this.props.gender}
                                            </span>
                                    </Tooltip>
                                </div>
                            ]:[
                                null
                            ]}

 

                            {this.props.country.length!== 0?[
                                <div className={styles.displayDiv}>
                                    <Tooltip title="Country">
                                        <FontAwesome.FaGlobe className={styles.displayIcon} />
                                            <span className={styles.displayText}>
                                                {this.props.country[0]}/{this.props.country[1]}
                                            </span>
                                    </Tooltip>
                                </div>
                            ]:[
                                null
                            ]}
                            {this.props.bio!==''?[
                                <div className={styles.displayDiv}>
                                    <Tooltip title="Bio">
                                    <FontAwesome.FaInfoCircle className={styles.displayIcon} />
                                            <span className={styles.displayText}>
                                                {this.props.bio!==''?this.props.bio:"-"}
                                            </span>
                                    </Tooltip>
                                </div>
                            ]:[
                                null
                            ]}

                        </Col>
                    </Row> 
                </Card >
            )
        }else{
            return (
                <Card bordered={false} lg={12} sm={12} style={{marginBottom:'10px'}}>
                <Spin spinning={this.state.photoUploadLoading}>
                <Form onSubmit={this.handleSubmit} className="login-form">
                    <FormItem>
                    <Row  type="flex" justify="end" style={{paddingBottom:40}}>
                        <Col style={{paddingRight:3}} >
                            <Tooltip title="Save Profile">
                                <Button type="primary" icon="save"  htmlType="submit" />
                            </Tooltip>
                        </Col>
                        <Col>
                            <Tooltip title="Rollback">
                                <Button type="danger" icon="close" onClick={()=>{        
                                    this.props.dispatch({
                                        type: 'profile/eidtStateFalse'
                                    })}} />
                            </Tooltip>
                        </Col>
                    </Row>
                    </FormItem>
                    <FormItem>
                    <Row  type="flex" justify="center" align="top" >
                        <Col >
                            <div style={{height: '200px',width: '200px' }}>
                                {this.state.cropEnable ? (
                                    <Avatar 
                                        onClick={()=>{
                                            this.setState({cropEnable:false})
                                        }}
                                        shape="square" 
                                        style={{width:'200px',height:'200px'}} 
                                        src={this.state.src} />
                                ):(
                                    <Cropper
                                        ref='cropper'
                                        src={this.state.src}
                                        style={{ height: '200px', width: '100%' }}
                                        aspectRatio={1 / 1}
                                        preview=".img-preview"
                                        guides={false} >
                                    </Cropper>
                                )}
                                    <div style={{paddingTop:15}}>
                                        <Button type="primary"  className={styles.fileContainer}>
                                            <FontAwesome.FaCamera size={20} /> 
                                            <input type="file" name="myfile" onChange={this._onChange}/>
                                            &nbsp;Upload
                                        </Button>
                                    </div>

                                {!this.state.cropEnable ? (
                                    <div>
                                    <Button style={{float:'right'}} onClick={this._cropImage} >                                        <FontAwesome.FaCrop size={20}/>
                                        &nbsp;Crop
                                    </Button>
                                    </div>
                                ):null}
                                
                            </div>
                        </Col>
                    </Row>
                    </FormItem>
                    
                    <Row type="flex" justify="center" align="top">
                        <Col style={{textAlign: 'center'}}>
                            <FormItem>
                            {getFieldDecorator('displayName', {
                                    initialValue: this.props.name,
                                    rules: [{ required: true, message: 'Display Name is not empty!' }],
                                })(
                                <Input 
                                    maxLength="15"
                                    className={styles.displayTextNameEdit}
                                    placeholder="Display Name"/>
                            )}
                            </FormItem>
                            <FormItem>
                            {getFieldDecorator('email', {
                                    initialValue: this.props.email,
                                })(
                                <Input
                                    disabled
                                    style={{border:'white'}}
                                    placeholder="Email" />
                                )}
                            </FormItem>
                            <FormItem>
                            {getFieldDecorator('birthday', {
                                    initialValue:new Date(this.props.birthday).getFullYear() !== '0001'?moment(this.props.birthday, 'YYYY-MM-DD'):null,
                                })(
                                <DatePicker 
                                    className={styles.displayDateEdit}
                                    placeholder="Birth Date"/>
                                )}
                            </FormItem>
                            {this.props.gender !== ""?
                                <FormItem>
                            {getFieldDecorator('gender', 
                                {
                                    initialValue: this.props.gender                           
                                }
                                )(
                                <Select
                                    placeholder="Gender">
                                        <Option value="Male">Male</Option>
                                        <Option value="Female">Female</Option>
                                    </Select>
                            )}
                            </FormItem>:
                            <FormItem>
                            {getFieldDecorator('gender', 
                                {                   
                                }
                                )(
                                <Select
                                    // className={styles.displaySelectEdit}
                                    placeholder="Gender">
                                        <Option value="Male">Male</Option>
                                        <Option value="Female">Female</Option>
                                    </Select>
                            )}
                            </FormItem>
                            }
                            
                            
                            <FormItem>
                            {getFieldDecorator('country', {
                                    initialValue: this.props.country,
                                })(
                            <Cascader
                                style={{textAlign:'left'}}
                                options={countryOptions}
                                placeholder="Country" />
                                )}
                            </FormItem>
                            <FormItem>
                            {getFieldDecorator('bio', {
                                    initialValue: this.props.bio,
                                })(
                                <TextArea
                                    className={styles.displayTextAreaEdit} 
                                    placeholder="Bio"
                                    autosize={{ minRows: 2, maxRows: 6 }}  />
                                )}
                            </FormItem>
                        </Col>
                    </Row>
                </Form>
                </Spin>
            </Card >
            )
        }
    }
}


const ProfileCardForm = Form.create()(ProfileCard);


function mapStateToProps(state) {
    return {
      src: state.avatar,
    };
  }
connect(mapStateToProps)(ProfileCardForm);



function dataURLtoFile(dataurl, filename) {
    var arr = dataurl.split(','), mime = arr[0].match(/:(.*?);/)[1],
        bstr = atob(arr[1]), n = bstr.length, u8arr = new Uint8Array(n);
    while(n--){
        u8arr[n] = bstr.charCodeAt(n);
    }
    return new File([u8arr], filename, {type:mime});
}

















    // apply = (file) => {
    //     console.log(file)
    //     return new Promise(
    //         (resolve, reject) => {
    //           const xhr = new XMLHttpRequest(); // eslint-disable-line no-undef
    //           xhr.open('POST', 'https://api.imgur.com/3/image');
    //           xhr.setRequestHeader('Authorization', 'Client-ID c2f926d5f0b58e1');
    //           const data = new FormData(); // eslint-disable-line no-undef
    //           data.append('image', file);
    //           xhr.send(data);
    //           xhr.addEventListener('load', () => {
    //             const response = JSON.parse(xhr.responseText);
    //             console.log(response)
    //             this.setState({
    //                 imgUrl: response.data.link,
    //             })
    //             resolve(response);
    //           });
    //           xhr.addEventListener('error', () => {
    //             const error = JSON.parse(xhr.responseText);
    //             reject(error);
    //           });
    //         },
    //       );
    // }







var user = {
    basicInfo: {
      name: "Jane Doe",
      gender: "Female",
      birthday: "April 3, 1990",
      location: "Los Angeles, CA",
      photo: profile,
      bio: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat fugit quia pariatur est saepe necessitatibus, quibusdam reiciendis ratione voluptate atque in qui provident rem repellat soluta. Blanditiis repellat velit eligendi."
    }
}
  
  
  class Avatara extends React.Component {
    render() {
      var image = this.props.image,
          style = {
            width: this.props.width || 50,
            height: this.props.height || 50
          }; 
      
      if (!image) return null;
      
      return (
       <div className="avatar" style={style}>
             <img src={this.props.image} /> 
        </div>
      );
    }
  }
  
  class MainPanel extends React.Component {
    render() {
      var info = this.props.info;
      if (!info) return null;
      
      return (
       <div>
          <div className="top">
              <Avatara 
                 image={info.photo} 
                 width={100}
                 height={100}
              /> 
              <h2 >{info.name}</h2>
              <h3>{info.location}</h3>
            
            <hr />
              <p>{info.gender} | {info.birthday}</p>
          </div>
          
          <div className="bottom">
            <h4>Biography</h4>
            <p>{info.bio}</p>
          </div>
        </div>
      );
    }
  }
  
