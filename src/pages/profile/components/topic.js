import {Component} from 'react'
import {Pagination, List, Card ,Avatar,Spin,Icon} from 'antd';
import { connect } from 'dva';
import { routerRedux } from 'dva/router';
import styles from './index.css'
const { Meta } = Card;

@connect(({ profile,loading}) => ({
  profile,
  loading: loading.effects['profile/fetchOwnTopicList'],
}))
export default class OwnTopic extends Component {
    constructor(props) {
      super(props)
      this.state = {
        selectedRadio: '1',
        current: 1,
      }
    }

    paginatiOnChange = (e) =>{
      console.log(e);
      this.setState({
        current: e,
      });
      console.log("paginatiOnChange",e-1,this.state.selectedRadio)
      this.props.dispatch({
        type: 'profile/fetchOwnTopicList',
        payload: {
          query: e-1,
        }
      })
    }

    cardOnClick = (id,e) => {
      this.props.dispatch(routerRedux.push(`edit-topic/${id}`));
    }
    UNSAFE_componentWillMount(){
      this.props.dispatch({
        type: 'profile/fetchOwnTopicList',
        payload: {
          query: 0,
        }
      })
    }

    render() {
      const {loading, profile} = this.props;
      console.log('profile',profile)

        return (
          <div className={styles.normal}>
               {loading? 
               [
                 <div style={{textAlign: 'center'}}>
                     <Spin size="large" />
                 </div>
               ]:[
                 
                  <List
                    dataSource={profile.topicList}
                    grid={{ gutter: 16, xs: 1, sm: 2, md: 3, lg: 4, xl: 5, xxl: 6 }}
                    size="large"
                    renderItem={item => (
                        <List.Item key={item.uuid}>
                            <Card 
                                bodyStyle={{padding:5}}
                                cover={<img alt="example" src={item.TitleImg!=""?item.TitleImg:"https://i.imgur.com/xUoxD1s.png"} />}
                                actions={[<Icon type="edit" onClick={(e) => this.cardOnClick(item.Uuid, e)}/>]}>
                                {item.Title}
                            </Card>
                        </List.Item>
                    )}
                  />
              ]
              }
              <Pagination pageSize={6} current={this.state.current} onChange={this.paginatiOnChange} total={profile.topicCount} />;
              {/* <Pagination pageSize={6}   total={profile.topicCount} onChange={this.paginatiOnChange}   /> */}
          </div>
         )
    }
}


