import request from '../../../utils/request';
import {GetToken} from '../../../utils/authority';
import api from '../../../api'
export function fetchProfile() {
    var token = GetToken()
    
    var obj = {
        method: 'GET',
        headers: {
          'Authorization':`token ${token}`
        }
    }
    
    return request(`${api}/profile`,obj)
}

export function updateProfile(params) {
    var token = GetToken()

    var body = {
        "userid": params.userid,
        "avatar": params.avatar,
        "displayName": params.displayName,
        "email": params.email,
        "birthday": params.birthday,
        "gender": params.gender,
        "country": params.country,
        "bio": params.bio,
    }
    var obj = {  
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Origin': '',
        'Authorization':`token ${token}`
      },
      body:JSON.stringify(body)
    }
  
    return request(`${api}/updateProfile`, obj);
}

export function fetchOwnTopicList(params) {
    var token = GetToken()
    
    var obj = {
        method: 'GET',
        headers: {
          'Authorization':`token ${token}`
        }
    }
    return request(`${api}/ownTopicList/${params.query}`,obj)
}
