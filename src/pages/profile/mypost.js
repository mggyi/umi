import { Component } from 'react'
import { Card,Tabs } from 'antd'
import Topic from './components/topic'

const TabPane = Tabs.TabPane;

function callback(key) {
  console.log(key);
}

// @connect(({topic, home,loading}) => ({
//     home,topic,
//     loading: loading.models.home,
// }))
export default class MyPost extends Component {
    constructor(props) {
        super(props)
    }

    componentDidMount() {
        
    }

    render() {

        return (
            <Card bordered={false} style={{marginBottom: 10}}>
                <Tabs onChange={callback}>
                    <TabPane tab="My Topic" key="1">
                        <Topic/>
                    </TabPane>
                    <TabPane tab="My Question" key="2">
                        {/* <Topic/> */}
                    </TabPane>
                </Tabs>
            </Card>
        )
    }

}