import {fetchProfile,updateProfile,fetchOwnTopicList} from '../services/profile'
export default {

    namespace: 'profile',
  
    state: {
        isEditing: false,
        name: '',
        avatar:'',
        email: '',
        birthday: '',
        gender: '',
        country: '',
        bio: '',
        userid:'',
        Post:'',
        Ask:'',
        Answer:'',
        topicList: [],
        topicCount: 0,
        questionList: [],
    },
  
    subscriptions: {
      setup({ dispatch, history }) {
      },
    },
  
    effects: {
      *fetchProfile(_, { call, put }) {
            const response = yield call(fetchProfile)
            
            yield put({ 
                type: 'save',
                payload: {
                    res: response.data,
                }
            });
      },
      *updateProfile({payload},{call,put}){
            const response = yield call(updateProfile,payload)
            
            yield put({ 
                type: 'save',
                payload: {
                    res: response.data,
                }
            });
      },
      *fetchOwnTopicList({payload},{call,put}){
        const response = yield call(fetchOwnTopicList,payload)
        yield put({ 
            type: 'savetopic',
            payload: {
                res: response.data,
            }
        });
    },
    },
  
    reducers: {
      save(state, {payload}) {
        return { ...state,
            name:payload.res.profile.displayName,
            avatar:payload.res.profile.avatar,
            email: payload.res.profile.email,
            birthday: payload.res.profile.birthday,
            gender: payload.res.profile.gender,
            country: payload.res.profile.country,
            bio: payload.res.profile.bio,
            userid:payload.res.profile.userid,
            Post:payload.res.profile.Post,
            Ask:payload.res.profile.Ask,
            Answer:payload.res.profile.Answer,
            isEditing: false,
        };
      },
      savetopic(state,{payload}) {
        return { ...state,
            topicList: payload.res.topiclist,
            topicCount: payload.res.count,
        };
      },
      add(state) { return state + 1 },
      eidtState(state){
        return{...state,
            isEditing:true,
        }
      },
      eidtStateFalse(state){
        return{...state,
            isEditing:false,
        }
      }
    },
  
};
  