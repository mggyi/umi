import { defineMessages } from 'react-intl';

const messages = defineMessages({
    footer: {
      id: 'layouts.BasicLayout.footer',
      defaultMessage: 'Ant design pro.',
    },
    profile: {
        id: "layouts.Header.Profile",
      defaultMessage: "ကိုယ်ရေးအကျဉ်",
    },
});

export default messages;