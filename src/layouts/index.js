
// import React from 'react';
import styles from './index.css';
// import Header from './Header';
// import withRouter from 'umi/withRouter';


import React from 'react';
import pathToRegexp from 'path-to-regexp';
import { IntlProvider } from 'react-intl';
import withRouter from 'umi/withRouter';
import dynamic from 'umi/dynamic';
import { connect } from 'dva';
import { LocaleProvider } from 'antd';
import zhCN from 'antd/lib/locale-provider/zh_CN';
import Redirect from 'umi/redirect';
import { translationMessages } from  './../i18n';
import {isLogin} from './../utils/authority.js'
import BasicLayout from './BasicLayout';
import UserLayout from './UserLayout';
import Helmet from "react-helmet";
import DocumentMeta from 'react-document-meta';

function mapStateToProps({ global }) {
  return {
    locale: global.locale,
  };
}

const app = (messages) => withRouter(
  connect(mapStateToProps)(({ history,children, location, locale }) => {
    if (!process.env.PRODUCTION) {
    }
    if (process.env.PRODUCTION) {
    }

    var chkLogin = isLogin()    
    const { pathname } = location;
    let LayoutComponent = BasicLayout;
    
    if (pathToRegexp('/login(.*)').test(pathname)) {
      if( chkLogin){
        return <Redirect to="/" />
      }else{
        LayoutComponent = UserLayout;  
      }
    } else {
      LayoutComponent = BasicLayout;
    }
    
    const meta = {
      title: 'DroidFo',
      meta: {
        property: {
          'og:title': 'Welcome from droidfo.tk',
          'og:url': 'https://www.droidfo.tk/',
          'og:image': "https://www.android.com/static/2016/img/one/global/a1_hero_xiaomi_1x.png",
          'og:description': 'droidfo is an andorid forum for end users and developers',
        }
      }
    };

    return (
      <div className="application">
      {/* <Parent> */}
      <Helmet title = {meta.title} meta={meta.meta} />
        {/* <meta property="og:url"          content="http://www.nytimes.com/2015/02/19/arts/international/when-great-minds-dont-think-alike.html" />
        <meta property="og:type"               content="article" />
        <meta property="og:title"              content="When Great Minds Don’t Think Alike" />
        <meta property="og:description"        content="How much does culture influence creative thinking?" />
        <meta property="og:image"              content="http://static01.nyt.com/images/2015/02/19/arts/international/19iht-btnumbers19A/19iht-btnumbers19A-facebookJumbo-v2.jpg" />

        <meta name="twitter:card" content="summary" />
        <meta name="twitter:site" content="@nytimesbits" />
        <meta name="twitter:creator" content="@nickbilton" />
        <meta property="og:url" content="http://bits.blogs.nytimes.com/2011/12/08/a-twitter-for-my-sister/" />
        <meta property="og:title" content="A Twitter for My Sister" />
        <meta property="og:description" content="In the early days, Twitter grew so quickly that it was almost impossible to add new features because engineers spent their time trying to keep the rocket ship from stalling." />
        <meta property="og:image" content="http://graphics8.nytimes.com/images/2011/12/08/technology/bits-newtwitter/bits-newtwitter-tmagArticle.jpg" />
      </Helmet> */}
      {/* <Child> */}
      <IntlProvider
        locale={locale}
        key={locale}
        messages={messages[locale]}
      >
        <LocaleProvider locale={locale === 'zh' ? zhCN : {}}>
          <LayoutComponent 
              isLogin ={chkLogin}
              location={location}>
                {children}
          </LayoutComponent>
        </LocaleProvider>
      </IntlProvider>
      {/* </Child> */}
      {/* </Parent> */}
      </div>
    );
  })
);


// function Layout({ children, location }) {
//   return (
//     <div className={styles.normal}>
//       <Header location={location} />
//       <div className={styles.content}>
//         <div className={styles.main}>
//           {children}
//         </div>
//       </div>
//     </div>
//   );
// }

// export default withRouter(Layout);


export default dynamic(async () => {
  if (!window.Intl) {
    await import('intl');
    await Promise.all([
      import('intl/locale-data/jsonp/en'),
      import('intl/locale-data/jsonp/zh'),
    ]);
  }

  return app(translationMessages);
});