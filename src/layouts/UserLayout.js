import React, { Fragment } from 'react';
import Link from 'umi/link';
import ReactDOM from 'react-dom';
import DocumentTitle from 'react-document-title';
import { Icon,Layout } from 'antd';
import logo from './../assets/logo.svg';
import GlobalFooter from './../components/GlobalFooter';
import { defineMessages, FormattedMessage } from 'react-intl';
import styles from './UserLayout.less';
import LocaleToggle from  '../containers/LocaleToggle';
import PropTypes from 'prop-types';
import svg from './../assets/circle.svg'

import LiveBackground from './../components/LiveBackground'

// import ticker from 'rc-tween-one/lib/ticker';
// import TweenOne from 'rc-tween-one';
// import { enquireScreen } from 'enquire-js';

const { Footer} = Layout;
// const links = [{
//   key: 'help',
//   title: 'helps',
//   href: '',
// }, {
//   key: 'privacy',
//   title: 'privacy',
//   href: '',
// }, {
//   key: 'terms',
//   title: 'terms',
//   href: '',
// }];

// const copyright = <Fragment>Copyright <Icon type="copyright" /> 2018 蚂蚁金服体验技术部出品</Fragment>;

const messages = defineMessages({
  footer: {
    id: 'layouts.BasicLayout.footer',
    defaultMessage: 'Ant design pro.',
  },
});

class UserLayout extends React.PureComponent {
  static propTypes = {
    image: PropTypes.string,
    w: PropTypes.number,
    h: PropTypes.number,
    pixSize: PropTypes.number,
    pointSizeMin: PropTypes.number,
  };

  static defaultProps = {
    // image: 'https://zos.alipayobjects.com/rmsportal/gsRUrUdxeGNDVfO.svg',
    image: svg,
    w: 300,
    h: 300,
    pixSize: 20,
    pointSizeMin: 10,
  };

  constructor(props) {
    super(props);
    this.state = {};
    this.interval = null;
    this.gather = true;
    this.intervalTime = 8000;
  }

//   componentDidMount() {
//     this.dom = ReactDOM.findDOMNode(this);
//     this.createPointData();
//   }

//   componentWillUnmount() {
//     ticker.clear(this.interval);
//     this.interval = null;
//   }

//   onMouseEnter = () => {
//     // !this.gather && this.updateTweenData();
//     if (!this.gather) {
//       this.updateTweenData();
//     }
//     this.componentWillUnmount();
//   };

//   onMouseLeave = () => {
//     // this.gather && this.updateTweenData();
//     if (this.gather) {
//       this.updateTweenData();
//     }
//     this.interval = ticker.interval(this.updateTweenData, this.intervalTime);
//   };

//   setDataToDom(data, w, h) {
//     this.pointArray = [];
//     const number = this.props.pixSize;
//     for (let i = 0; i < w; i += number) {
//       for (let j = 0; j < h; j += number) {
//         if (data[((i + j * w) * 4) + 3] > 150) {
//           this.pointArray.push({ x: i, y: j });
//         }
//       }
//     }
//     const children = [];
//     this.pointArray.forEach((item, i) => {
//       const r = Math.random() * this.props.pointSizeMin + this.props.pointSizeMin;
//       const b = Math.random() * 0.4 + 0.1;
//       children.push((
//         <TweenOne className={styles.pointwrapper} key={i} style={{ left: item.x, top: item.y }}>
//           <TweenOne
//             className={styles.point}
//             style={{
//               width: r,
//               height: r,
//               opacity: b,
//               backgroundColor: `rgb(${Math.round(Math.random() * 95 + 160)},255,255)`,
//             }}
//             animation={{
//               y: (Math.random() * 2 - 1) * 10 || 5,
//               x: (Math.random() * 2 - 1) * 5 || 2.5,
//               delay: Math.random() * 1000,
//               repeat: -1,
//               duration: 3000,
//               yoyo: true,
//               ease: 'easeInOutQuad',
//             }}
//           />

//         </TweenOne>
//       ));
//     });
//     this.setState({
//       children,
//       boxAnim: { opacity: 0, type: 'from', duration: 800 },
//     }, () => {
//       this.interval = ticker.interval(this.updateTweenData, this.intervalTime);
//     });
//   }

//   createPointData = () => {
//     const { w, h } = this.props;
//     const canvas = document.getElementById('canvas');
//     const ctx = canvas.getContext('2d');
//     ctx.clearRect(0, 0, w, h);
//     canvas.width = this.props.w;
//     canvas.height = h;
//     const img = new Image();
//     img.onload = () => {
//       ctx.drawImage(img, 0, 0, img.width, img.height, 0, 0, w, h);
//       const data = ctx.getImageData(0, 0, w, h).data;
//       this.setDataToDom(data, w, h);
//       this.dom.removeChild(canvas);
//     };
//     img.crossOrigin = 'anonymous';
//     img.src = this.props.image;
//   };

//   gatherData = () => {
//     const children = this.state.children.map(item =>
//       React.cloneElement(item, {
//         animation: {
//           x: 0,
//           y: 0,
//           opacity: 1,
//           scale: 1,
//           delay: Math.random() * 500,
//           duration: 800,
//           ease: 'easeInOutQuint',
//         },
//       }));
//     this.setState({ children });
//   };

//   disperseData = () => {
//     const rect = this.dom.getBoundingClientRect();
//     const sideRect = this.sideBox.getBoundingClientRect();
//     const sideTop = sideRect.top - rect.top;
//     const sideLeft = sideRect.left - rect.left;
//     const children = this.state.children.map(item =>
//       React.cloneElement(item, {
//         animation: {
//           x: Math.random() * rect.width - sideLeft - item.props.style.left,
//           y: Math.random() * rect.height - sideTop - item.props.style.top,
//           opacity: Math.random() * 0.4 + 0.1,
//           scale: Math.random() * 2.4 + 0.1,
//           duration: Math.random() * 500 + 500,
//           ease: 'easeInOutQuint',
//         },
//       }));

//     this.setState({
//       children,
//     });
//   };

//   updateTweenData = () => {
//     this.dom = ReactDOM.findDOMNode(this);
//     this.sideBox = ReactDOM.findDOMNode(this.sideBoxComp);
//     ((this.gather && this.disperseData) || this.gatherData)();
//     this.gather = !this.gather;

// };



  getPageTitle() {
    let title = 'Nyein Chan Aung';
    return title;
  }

    render() {
        const { children } = this.props;
        
        return (
          <DocumentTitle title={this.getPageTitle()}>

          <div className={styles.logogatherdemowrapper}>
        {/* <canvas id="canvas" />

        <TweenOne
          animation={this.state.boxAnim}
          className={styles.rightside}
          onMouseEnter={this.onMouseEnter}
          onMouseLeave={this.onMouseLeave}
          ref={(c) => {
            this.sideBoxComp = c;
          }}
        >
          {this.state.children}
        </TweenOne> */}
          {/* <LiveBackground/> */}
          <div className={styles.container}>
              <div className={styles.content}>

              
                <div className={styles.top}>
                  <div className={styles.header}>
                    <Link to="/">
                       <img alt="logo" className={styles.logo} src={logo} /> 
                      <span className={styles.title}>Android Forums</span>
                    </Link>
                  </div>
                  <div className={styles.desc}>No.1 Android forum in the world!</div>
                </div>
                {children}
              </div>
              <Footer style={{ padding: 0 }}>
                  <GlobalFooter
                  links={[
                    {
                      key: 'Pro Nca',
                      title: 'Pro Nca',
                      href: 'https://nyienchanaung.github.io',
                      blankTarget: true,
                    },
                    {
                      key: 'github',
                      title: <Icon type="github" />,
                      href: 'https://github.com/nyienchanaung/nyienchanaung.github.io',
                      blankTarget: true,
                    },
                    {
                      key: 'Nca Design',
                      title: 'Nca Design',
                      href: 'https://nyienchanaung.github.io',
                      blankTarget: true,
                    },
                  ]}
                  copyright={
                    <Fragment>
                      Copyright <Icon type="copyright" /> 2018{' '}
                      <FormattedMessage
                        {...messages.footer}
                      />
                      &nbsp;
                      <LocaleToggle />
                    </Fragment>
                  }
                />
              </Footer>
            </div>
            </div>

          </DocumentTitle>
        )
    }
}

export default UserLayout;





