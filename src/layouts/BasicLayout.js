import React , { Fragment } from 'react';
import DocumentTitle from 'react-document-title';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { enquireScreen, unenquireScreen  } from 'enquire-js';
import { ContainerQuery } from 'react-container-query';
import {Card, Layout, Icon } from 'antd';
import { TransitionGroup, CSSTransition } from "react-transition-group";
import {FormattedMessage } from 'react-intl';
import messages from './../translations/message'
import LocaleToggle from  '../containers/LocaleToggle';
import GlobalHeader from '../components/GlobalHeader';
import GlobalFooter from '../components/GlobalFooter';
import SiderMenu from '../components/SiderMenu';
import Breadcrumb from '../components/Breadcrumbs';
import MetaTags from 'react-meta-tags';
import { routerRedux } from 'dva/router';
import { connect } from 'dva';
// import Media from "react-media";

import logo from '../assets/favicon.png';
import styles from './BasicLayout.less'

const {  Header, Footer } = Layout;


const query = {
  'screen-xs': {
    maxWidth: 575,
  },
  'screen-sm': {
    minWidth: 576,
    maxWidth: 767,
  },
  'screen-md': {
    minWidth: 768,
    maxWidth: 991,
  },
  'screen-lg': {
    minWidth: 992,
    maxWidth: 1199,
  },
  'screen-xl': {
    minWidth: 1200,
  },
};

// const childQuery = {
//   'width-larger-than-767': {
//     paddingTop: 200,
//   }
// }




let isMobile;
enquireScreen(b => {
  isMobile = b;
});

@connect(({ user, global, loading }) => ({
  currentUser: user.currentUser,
  collapsed: global.collapsed,
  fetchingNotices: loading.effects['global/fetchNotices'],
  notices: global.notices,
}))
export default class BasicLayout extends React.PureComponent {
  static childContextTypes = {
    login: PropTypes.bool,
  };
  state = {
    isMobile,
    boxShadow: '0 3px 10px 0 #bfbfbf'
  };

  getChildContext() {
    return {
      login: this.props.isLogin,
    };
  }

  listenScrollEvent = e => {
    if (window.scrollY > 1) {
      this.setState({boxShadow: '0 3px 10px 0 #bfbfbf'})
    } else {
      this.setState({boxShadow: '0 3px 10px 0 #bfbfbf'})
    }
  }
  
  getPageTitle() {
    let title = 'Android Forums';
    return title;
  }
  // getChildContext() {
  //   return {
  //     url: "http://www.nytimes.com/2015/02/19/arts/international/when-great-minds-dont-think-alike.html",
  //     type: "article",
  //     title: "When Great Minds Don’t Think Alike",
  //     description: "http://static01.nyt.com/images/2015/02/19/arts/international/19iht-btnumbers19A/19iht-btnumbers19A-facebookJumbo-v2.jpg"
  //   };
  // }

  componentDidMount() {
    document.description = "aa"
    window.addEventListener('scroll', this.listenScrollEvent)
    this.enquireHandler = enquireScreen(mobile => {
      this.setState({
        isMobile: mobile,
      });
    });
    this.props.dispatch({
      type: 'user/fetchCurrent',
    });
    
  }
  componentDidUpdate(prevProps) {
    if (this.props.location !== prevProps.location) {
      window.scrollTo(0, 0);
    }
  }
  componentWillUnmount() {
    unenquireScreen(this.enquireHandler);
  }

  dropDownMenuOnClick = (e) => {
    if(e.key === 'logout'){
      this.props.dispatch({
        type: 'user/logout',
      });
    }
    if(e.key === 'newtopic'){
      this.props.dispatch(routerRedux.push('/newtopic'));
    }
    if(e.key === 'profile'){
      this.props.dispatch(routerRedux.push('/profile'));
    }
    if(e.key === 'setting'){
      this.props.dispatch(routerRedux.push('/setting'));
    }
    
  }


  render() {
    const { location: { pathname } } = this.props;
    const layoutStyle = {
      marginLeft : this.state.isMobile? 0: 200,
      background: '#fff',
      paddingTop: 90,
      paddingLeft: 30,
      paddingRight: 30
    }
    const {
      isLogin,
      currentUser,
      // collapsed,
      // fetchingNotices,
      // notices,
      location,
      children,
    } = this.props;
    console.log("IsLogin", isLogin)
    console.log("BasicLayout",this.props)
    const layout = (
      <Layout style={{background:'#fff'}}>
          <Header 
            // style={{boxShadow: this.state.boxShadow}} 
            className={styles.header}
            >
            <GlobalHeader
              logo={logo}
              isLogin = {isLogin}
              currentUser={currentUser}
              // fetchingNotices={fetchingNotices}
              // notices={notices}
              // collapsed={collapsed}
              isMobile={this.state.isMobile}
              // onNoticeClear={this.handleNoticeClear}
              // onCollapse={this.handleMenuCollapse}
              onMenuClick={this.dropDownMenuOnClick}
              // onNoticeVisibleChange={this.handleNoticeVisibleChange}
            />
          </Header>
          <SiderMenu
            
            pathname = {pathname}
            logo={logo}
            isMobile={this.state.isMobile}
          />


        <Layout style={layoutStyle} >
        
            <Card 
              bordered={false}
              style={{ padding:0,margin:0,background:'#fff'}} 
              bodyStyle={{padding:7,margin:0}}>
              <Breadcrumb/>
            </Card>
            {/* <TransitionGroup>
              <CSSTransition key={location.key} 
                classNames="fade"

                timeout={300}> */}
               {children}
              {/* </CSSTransition>
            </TransitionGroup> */}
            <Footer style={{ padding: 0 ,background:'white'}}>
            <GlobalFooter
              links={[
                {
                  key: 'Pro 首页',
                  title: 'Pro 首页',
                  href: 'http://pro.ant.design',
                  blankTarget: true,
                },
                {
                  key: 'github',
                  title: <Icon type="github" />,
                  href: 'https://github.com/ant-design/ant-design-pro',
                  blankTarget: true,
                },
                {
                  key: 'Ant Design',
                  title: 'Ant Design',
                  href: 'http://ant.design',
                  blankTarget: true,
                },
              ]}
              copyright={
                <Fragment>
                  Copyright <Icon type="copyright" /> 2018{' '}
                  <FormattedMessage
                    {...messages.footer}
                  />
                  &nbsp;
                  <LocaleToggle />
                </Fragment>
              }
            />
          </Footer>
        </Layout> 
      </Layout>

    );
    const meta = {
      title: 'Some Meta Title',
      description: 'I am a description, and I can create multiple tags',
      canonical: 'http://example.com/path/to/page',
      meta: {
        charset: 'utf-8',
        property:{

        },
        name: {
          keywords: 'react,meta,document,html,tags'
        }
      }
    };
    return (
//       <div class="wrapper">
//       <MetaTags>
//       <title>{this.getPageTitle()}</title>
//       <meta property="og:url"                content="http://www.nytimes.com/2015/02/19/arts/international/when-great-minds-dont-think-alike.html" />
// <meta property="og:type"               content="article" />
// <meta property="og:title"              content="When Great Minds Don’t Think Alike" />
// <meta property="og:description"        content="How much does culture influence creative thinking?" />
// <meta property="og:image"              content="https://i.imgur.com/ogA7aom.png" />
//       </MetaTags>

        <ContainerQuery query={query}>
          {params => <div className={classNames(params)}>{layout}</div>}
        </ContainerQuery>

      
    )
  }
}
// </DocumentMeta>
   // <DocumentMeta {...meta}>
      {/* // <DocumentTitle title={this.getPageTitle()} description= "http://static01.nyt.com/images/2015/02/19/arts/international/19iht-btnumbers19A/19iht-btnumbers19A-facebookJumbo-v2.jpg"> */}

          // <div>

          //         Copyright1 <Icon type="copyright" /> 2018{' '}
          //         <FormattedMessage
          //           {...messages.footer}
          //         />
          //         &nbsp;
          //         <LocaleToggle />

          // </div>