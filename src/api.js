var api 
if(process.env.PRODUCTION) {
    api = "https://android-forum-go-backend.herokuapp.com"
} else {
    api = "http://localhost:8080"
}
export default api;