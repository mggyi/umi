var CryptoJS = require("crypto-js")
var secretKey = "dsfsdfwervertjdskyuisarhqweuityw7ergb2134324jktf87s";


export function setLocalStorage(authority) {
    localStorage.setItem('id', CryptoJS.AES.encrypt(authority.token,secretKey));
}

export function setSessionStorage(id) {
    sessionStorage.setItem('current',id);
}

export function getSessionStorage() {
    return sessionStorage.getItem('current');
}

export function getLocalStorage() {
    var token = localStorage.getItem('id');
    if(!token){
        return null
    }else{
        var bytes  = CryptoJS.AES.decrypt(token, secretKey);
        var plaintext = bytes.toString(CryptoJS.enc.Utf8);
        return plaintext
    }
}

export function isLogin() {
    // if(getSessionStorage()){
        
    //     return true
    // }else 
    if(getLocalStorage()){
        
        return true
    }else{
        
        return false
    }
}

export function logout() {
    // sessionStorage.clear();
    localStorage.clear();
}

