
import { Route } from 'react-router-dom';
import Redirect from 'umi/redirect';
// import { connect } from 'dva';
import {isLogin} from '../utils/authority.js'
const Auth =  (args) => {
  
  const { render, ...rest } = args;


  // var token = isLogin()
  // console.log('token',token)
  // var chkLogin = token !== ''  ? true: false;
    if(isLogin()){ 
      return (<Route {...rest} render={props =>
                  <div>{render(props)}</div>
                }
            />)
    }else{
        return <Redirect to={`/login?query=${args.location.pathname.substr(1)}`} />
    }
}

export default Auth;