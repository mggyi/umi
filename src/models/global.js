// import { DEFAULT_LOCALE } from './../common/constants';
import { routerRedux } from 'dva/router';
export default {

  namespace: 'global',

  state: {
    locale: 'en',
  },

  subscriptions: {
    setup({ dispatch, history }) {
            // Subscribe history(url) change, trigger `load` action if pathname is `/`
            return history.listen(({ pathname, search }) => {
              if (typeof window.ga !== 'undefined') {
                window.ga('send', 'pageview', pathname + search);
              }
              if(pathname==='/' || pathname === "/topic"){
                dispatch({
                  type:"init"
                })
              }
            });
    },
  },

  effects: {
    *fetch({ payload }, { call, put }) {
      yield put({ type: 'save' });
    },
    *init({payload},{put}){
        yield put(routerRedux.push('/home/topic'))
    }
  },

  reducers: {
    save(state, action) {
      return { ...state, ...action.payload };
    },
    localeToggle(state, { payload }) {
      return{...state,locale:payload}
    },
  },

};
