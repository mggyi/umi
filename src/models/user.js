import {
  userNameCheck,
  SendFacebookIdToken,
  SendGoogleIdToken,
  Request_oauth_token,
  login,queryCurrent,
  register} from './../services/global.js'
  import { routerRedux } from 'dva/router';
import { setLocalStorage ,setSessionStorage} from './../utils/authority.js';
import {logout} from '../utils/authority';
import { message } from 'antd';


export default {

  namespace: 'user',

  state: {
     userNameStatus: '',
     status:'',
     list: [],
     token:'',
     message:'',
     currentUser: {},
     toauth_token: '',
  },


  effects: {
    *login({ payload }, { call, put }) {
      const response = yield call (login, payload)
      if(response.data.message !== "ok"){
        message.error(response.data.message);
      }else{
        message.success('User has successfully login!');
      } 
      yield put ({
        type: 'redulogin',
        payload: {
          res: response,
        }
      })

      // yield call(setSessionStorage,response.data)

      if(payload.remember === true){
        yield call(setLocalStorage,response.data)
      }

      if(payload.query === undefined){
        yield put(routerRedux.push('/list'));        
      }else{
        yield put(routerRedux.push(`/${payload.query}`));
      }

    },
    *gLogin({ payload },{ call,put }) {
      
      // const response = yield call (login, payload)
      const response = yield call (SendGoogleIdToken,payload)
      
      if(response.data.message !== "ok"){
        message.error(response.data.message);
      }else{
        message.success('User has successfully login!');
      }
      
      yield put ({
        type: 'redulogin',
        payload: {
          res: response,
        }
      })
      // yield call(setSessionStorage,response.data)
      if(payload.remember === true){
        yield call(setLocalStorage,response.data)
      }
      if(payload.query === undefined){
        yield put(routerRedux.push('/list'));        
      }else{
        yield put(routerRedux.push(`/${payload.query}`));
      }
    },
    *fLogin ({payload}, {call,put}) {
      const response = yield call (SendFacebookIdToken, payload)
      
      if(response.data.message !== "ok"){
        message.error(response.data.message);
      }else{
        message.success('User has successfully login!');
      }
      
      yield put ({
        type: 'redulogin',
        payload: {
          res: response,
        }
      })
      // yield call(setSessionStorage,response.data)
      if(payload.remember === true){
        yield call(setLocalStorage,response.data)
      }
      if(payload.query === undefined){
        yield put(routerRedux.push('/list'));        
      }else{
        yield put(routerRedux.push(`/${payload.query}`));
      }
    },
    *register({payload},{call,put}) {
      
      const response = yield call (register, payload)
      console.log("Register=>",response)
      yield put ({
        type: 'reduregister',
        payload: {
          res: response,
        }
      })
      // yield call(setSessionStorage,response.data)
      yield put(routerRedux.push('/list'));

    },
    *logout(_, { call, put }) {
      
      yield call(logout);
      yield put(routerRedux.push('/login'));
    },
    *fetchtoauth({payload},{call,put}) {
      const response = yield call (Request_oauth_token,payload)
      
      yield put ({
        type: 'redufetchtoauth',
        payload: {
          res: response,
        }
      })
    },
    *userNameCheck({payload},{call,put}) {
      const response = yield call (userNameCheck,payload)
      
      yield put ({
        type: 'reduUserNameCheck',
        payload: {
          res: response,
        }
      })
    },
    // *updateProfilePic(_,{call,put }) {
      
    //   const response = yield call (fetchProfilePic)
      
    //   yield put ({
    //     type: 'reduUpdateProfilePic',
    //     payload: {
    //       res: response,
    //     }
    //   })
    // },
    *fetchCurrent(_,{call,put}){
      console.log("fetchCurrent")
      const response = yield call (queryCurrent)

      console.log("Response---",response)
      yield put ({
        type: 'saveCurrentUser',
        payload: {
          res: response,
        }
      })
    }

  },
  
  reducers: {
    saveCurrentUser(state, action) {
      return {
        ...state,
        currentUser: action.payload.res.data.currentUser,
      };
    },
    reduUserNameCheck (state,action) {
      return { ...state,
        userNameStatus: action.payload.res.data.userNameAvaliable === true? "success":"error"
      }
    },
    redulogin(state,action) {
      
      return { ...state,
        token:action.payload.res.data.token,
        message:action.payload.res.data.message,
        currentUser: action.payload.res.data.currentUser,
      };
    },

    reduregister(state,action) {
      
      return { ...state,
        token:action.payload.res.data.token,
        message:action.payload.res.data.message,
        currentUser: action.payload.res.data.currentUser,
      };
    },

    fetchtoauth(state,action) {
      return { ...state,
        toauth_token: action.payload.res
      };
    },

    reduUpdateProfilePic(state,action) {
      return { ...state,
        message:action.payload.res.data.message,
        currentUser: action.payload.res.data.currentUser,
      };
    },
  },



};




// currentUser: {
//   name: 'Nyein Chan Aung',
//   avatar:
//     'https://gw.alipayobjects.com/zos/rmsportal/BiazfanxmamNRoxxVxka.png',
//   userid: '00000001',
//   notifyCount: 1,
// },