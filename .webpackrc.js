export default {
  "publicPath": "/static/",

  "proxy": {
    "/api": {
      "target": "https://android-forum-go-backend.herokuapp.com/",
      "changeOrigin": true,
      "pathRewrite": { "^/api" : "" }
    },
  },
  "define": {
    "process.env.PRODUCTION": false,
  },
  "theme": './src/theme.js',
  "env": {
    "development": {
      "extraBabelPlugins": [
        "dva-hmr"
      ]
    }
  }
}








