

import React from 'react';
import pathToRegexp from 'path-to-regexp';
import { IntlProvider } from 'react-intl';
import withRouter from 'umi/withRouter';
import dynamic from 'umi/dynamic';
import { connect } from 'dva';
import { LocaleProvider } from 'antd';
import zhCN from 'antd/lib/locale-provider/zh_CN';
import Redirect from 'umi/redirect';
import { translationMessages } from  './../i18n';
import {isLogin} from './../utils/authority.js'

import BasicLayout from '../src/layouts/BasicLayout';
import UserLayout from './UserLayout';



function mapStateToProps({ global }) {
  return {
    locale: global.locale,
  };
}

const app = (messages) => withRouter(
  connect(mapStateToProps)(({ history,children, location, locale }) => {
    if (!process.env.PRODUCTION) {


    }
    if (process.env.PRODUCTION) {

    }

    var chkLogin = isLogin()    
    
    const { pathname } = location;
    
    let LayoutComponent = BasicLayout;
    
    if (pathToRegexp('/login(.*)').test(pathname)) {
      if( chkLogin){
        return <Redirect to="/" />
      }else{
        LayoutComponent = UserLayout;  
      }
    } else {
      LayoutComponent = BasicLayout;
    }
    

    return (
      <IntlProvider
        locale={locale}
        key={locale}
        messages={messages[locale]}
      >
        <LocaleProvider locale={locale === 'zh' ? zhCN : {}}>
          <LayoutComponent 
              isLogin ={chkLogin}
              location={location}>
                {children}
          </LayoutComponent>
        </LocaleProvider>
      </IntlProvider>
    );
  })
);

export default dynamic(async () => {
  if (!window.Intl) {
    await import('C:/Users/home/AppData/Local/Microsoft/TypeScript/2.9/node_modules/@types/intl');
    await Promise.all([
      import('intl/locale-data/jsonp/en'),
      import('intl/locale-data/jsonp/zh'),
    ]);
  }

  return app(translationMessages);
});
