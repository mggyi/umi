


import React, { Component } from 'react';
import { connect } from 'dva';
import { Input } from 'antd';
import PageHeaderLayout from '../../../layouts/PageHeaderLayout';
// import Topic from './topic'
// import styles from './index.css';

@connect(({match}) => ({
  match,
}))
export default class SearchList extends Component {
  constructor(props){
    super(props)
    this.state = {
      tabActiveKey:'topic',
    }
  }

  

  render() {
    
    const tabList = [
      {
        key: 'topic',
        tab: 'Topic',
      },
      {
        key: 'ask',
        tab: 'Ask',
      },
      {
        key: 'unanswered',
        tab: 'Unanswered',
      },
    ];

    const mainSearch = (
      <div style={{ textAlign: 'center' }}>
        <Input.Search
          placeholder="keyword"
          size="large"
          onSearch={()=>{alert("ok")}}
          style={{ maxWidth: 522 }}
        />
      </div>
    );

    const { match,  location } = this.props;
    return (
      <PageHeaderLayout
        title="Home"
        content={mainSearch}
        tabList={tabList}
        tabActiveKey={this.props.tabActiveKey}
        onTabChange={this.props.handleTabChange}
      >
        {this.props.children}

      </PageHeaderLayout>
    );
  }
}


