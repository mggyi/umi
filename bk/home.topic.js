import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'dva';
import moment from 'moment';
import {Spin, Card,  List, Tag, Icon, Avatar,  Button,Row,Col } from 'antd';
import styles from './topic.less';
import { routerRedux } from 'dva/router';
import './index.css'
import api from '../../../api'
import reqwest from 'reqwest';
import Media from "react-media";

import {
  Editor,
  Block,
  createEditorState,
  rendererFn,
} from 'medium-draft';
import {
  setRenderOptions,
  blockToHTML,
  entityToHTML,
  styleToHTML,
} from 'medium-draft/lib/exporter';

const { Meta } = Card;
const newBlockToHTML = (block) => {
  const blockType = block.type;
  if (block.type === Block.ATOMIC) {
    if (block.text === 'E') {
      return {
        start: '<figure class="md-block-atomic md-block-atomic-embed">',
        end: '</figure>',
      };
    } else if (block.text === '-') {
      return <div className="md-block-atomic md-block-atomic-break"><hr/></div>;
    }
  }
  return blockToHTML(block);
};
  
const newEntityToHTML = (entity, originalText) => {
  if (entity.type === 'embed') {
    return (
      <div>
        <a>
        </a>
      </div>
    );
  }
  return entityToHTML(entity, originalText);
};

const AtomicSeparatorComponent = (props) => (<hr />);
  
const AtomicBlock = (props) => {
  const { blockProps, block } = props;
  const content = blockProps.getEditorState().getCurrentContent();
  const entity = content.getEntity(block.getEntityAt(0));
  const data = entity.getData();
  const type = entity.getType();
  if (blockProps.components[type]) {
    const AtComponent = blockProps.components[type];
    return (
      <div className={`md-block-atomic-wrapper md-block-atomic-wrapper-${type}`}>
        <AtComponent data={data} />
      </div>
    );
  }
  return <p>Block of type <b>{type}</b> is not supported.</p>;
};
  
 
class AtomicEmbedComponent extends React.Component {
  static propTypes = {
    data: PropTypes.object.isRequired,
  }

  constructor(props) {
    super(props);

    this.state = {
      showIframe: false,
    };

    this.enablePreview = this.enablePreview.bind(this);
  }

  componentDidMount() {
    this.renderEmbedly();
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.showIframe !== this.state.showIframe && this.state.showIframe === true) {
      this.renderEmbedly();
    }
  }

  getScript() {
    const script = document.createElement('script');
    script.async = 1;
    script.src = '//cdn.embedly.com/widgets/platform.js';
    script.onload = () => {
      window.embedly();
    };
    document.body.appendChild(script);
  }

  renderEmbedly() {
    if (window.embedly) {
      window.embedly();
    } else {
      this.getScript();
    }
  }

  enablePreview() {
    this.setState({
      showIframe: true,
    });
  }

  render() {
    const { url } = this.props.data;
    const innerHTML = `<div><a class="embedly-card" href="${url}" data-card-controls="0" data-card-theme="dark">
      <photo class="shine" style="width:200px;height:150px"></photo>
    </a></div>`;
    return (
      <div className="md-block-atomic-embed">
        <div dangerouslySetInnerHTML={{ __html: innerHTML }} />
      </div>
    );
  }
}

@connect(({topic, home,loading}) => ({
  home,topic,
  loading: loading.models.home,
}))
export default class TopicList extends React.Component{
  constructor(props){
    super(props)
    this.state = {
      count : 0,
      loading: true,
      loadingMore: false,
      showLoadingMore: true,
      topicList: [],  
    }
    this.exporter = setRenderOptions({
      styleToHTML,
      blockToHTML: newBlockToHTML,
      entityToHTML: newEntityToHTML,
    });

  }
  


  componentDidMount(){
    window.addEventListener("scroll", this.handleScroll);
    console.log("reload")
    this.getData(0,(res) => {
      console.log('mount',res)
      this.setState({
        loading: false,
        topicList: res.topicList,
      });
    });
    this.setState((prevState) => {
      return {count: prevState.count + 1};
    });
  }

  handleScroll = () => {
    const windowHeight = "innerHeight" in window ? window.innerHeight : document.documentElement.offsetHeight;
    const body = document.body;
    const html = document.documentElement;
    const docHeight = Math.max(body.scrollHeight, body.offsetHeight, html.clientHeight,  html.scrollHeight, html.offsetHeight);
    const windowBottom = windowHeight + window.pageYOffset;
    if (windowBottom >= docHeight) {
      this.onSearchLoadMore()
    }
  }

  componentWillUnmount() {
    window.removeEventListener("scroll", this.handleScroll);
  }

  componentDidUpdate(prevProps){

    if(this.props.location.query !== prevProps.location.query){
      this.getData(0,(res) => {
        console.log('mount',res)
        this.setState({
          loading: false,
          topicList: res.topicList,
        });
      });
      this.setState({
        count:1,
      })
    }
  }

  getData = (q,callback) => {
    const search = this.props.location.query.search === undefined?'':this.props.location.query.search
    const tag = this.props.location.query.tag === undefined?'':this.props.location.query.tag
    reqwest({
      url: `${api}/getlatestpost/${search}/${tag}/${q}`,
      type: 'json',
      crossOrigin: true,
      method: 'get',
      contentType: 'application/json',
      success: (res) => {
        console.log('myres',res)
        callback(res);
      },
    });
  }

  onLoadMore = () => {
    this.setState((prevState) => {
      return {count: prevState.count + 1};
    });
    this.setState({
      loadingMore: true,
    });
    this.getData(this.state.count,(res) => {
      const topicList = this.state.topicList.concat(res.topicList);
      this.setState({
        topicList,
        loadingMore: false,
      }, () => {
        window.dispatchEvent(new Event('resize'));
      });
    });
  }

  onSearchLoadMore = () => {

    console.log("OnloadMore",this.state.count)
    this.setState({
      loadingMore: true,
    });
    this.getData(this.state.count,(res) => {
      const topicList = this.state.topicList.concat(res.topicList);
      this.setState({
        topicList,
        loadingMore: false,
      }, () => {
        window.dispatchEvent(new Event('resize'));
      });
    });
    this.setState((prevState) => {
      return {count: prevState.count + 1};
    });
  }

  strip = (post) =>
  {
    var editorState = createEditorState(post)
   
    var text;
    //  = editorState.getCurrentContent().getPlainText();
    var currentContent = editorState.getCurrentContent();
    const eHTML = this.exporter(currentContent);
    var tmp = document.createElement("DIV");
    tmp.innerHTML = eHTML;
    var rt = tmp.textContent || tmp.innerText || "";
    var words=rt.split(" ");
    if(words.length>1){
        text=words.splice(0,20).join(" ");
    }
    return text
  }

  rendererFn(setEditorState, getEditorState) {
    const atomicRenderers = {
      embed: AtomicEmbedComponent,
      separator: AtomicSeparatorComponent,
    };
    const rFnOld = rendererFn(setEditorState, getEditorState);
    const rFnNew = (contentBlock) => {
      const type = contentBlock.getType();
      switch(type) {
        case Block.ATOMIC:
          return {
            component: AtomicBlock,
            editable: false,
            props: {
              components: atomicRenderers,
              getEditorState,
            },
          };
        default: return rFnOld(contentBlock);
      }
    };
    return rFnNew;
  }

render(){
  const IconText = ({ type, text }) => (
    <span>
      <Icon type={type} style={{ marginRight: 8 }} />
      {text}
    </span>
  );

  const ListContent = ({ data: {Post,Uuid } }) => {
    return(
      
      <div 
        className={styles.description} style={{paddingLeft:10}}
      >{this.strip(Post)}<b><a onClick={()=>{ 
                          this.props.dispatch(
                            routerRedux.push(`/topic/${Uuid}`)
                          )}}>...more</a></b></div>


    )
  }
  const ListExtra = ({ data: {User, CreateAt} }) => {
    return(
      <div style={{paddingTop:10,paddingLeft:10}}>
          <Card  bordered={false} style={{background:'transparent'}} bodyStyle={{padding:'0'}}>
            <Meta
              avatar={<Avatar shape="square" size="small" src={User[0].avatar} style={{padding:0}} />}
              title={ <a onClick={()=>{
                this.props.dispatch(
                  routerRedux.push(`/users/${User[0].uuid}`)
                )
              }}
              ><><p className="date">{User[0].displayName}&nbsp; <Icon type="clock-circle-o" />&nbsp;{moment(CreateAt).format('YYYY-MM-DD HH:mm')}</p></>
            </a>}/>
          </Card>  
      </div>

    )
  }


  const { loading, loadingMore, showLoadingMore, topicList } = this.state;
  const loadMore = showLoadingMore ? (
    <div style={{ textAlign: 'center', marginTop: 12, height: 32, lineHeight: '32px' }}>
      {loadingMore && <Spin />}
      {!loadingMore && <Button onClick={this.onLoadMore}>loading more</Button>}
    </div>
  ) : null;

  return (
    <Row>
      <Col xs={24} sm={24} md={24} lg={18}>
      <Card
      style={{marginTop: 24 ,paddingTop:20}}
      bordered={false}
      bodyStyle={{ padding: '0' }}>
        {/* <Button  icon="plus-circle-o" 
        onClick={()=>{
          this.props.dispatch(
            routerRedux.push('/newtopic')
          );
        }}>New Topic</Button> */}

          <List
             size="large"
             loading={loading}
             rowKey="id"
             itemLayout="vertical"
             loadMore={loadMore}
             dataSource={topicList}
             renderItem={item => (
              <List.Item 
                style={{padding:0,margin:0,border:'none'}}
                key={item.id}>
                <Media query="(max-width: 968px)">
                    {matches =>
                           matches ? (
                            <Row>
                              <Col span={24}>
                                {item.TitleImg != "" ?
                                  <Card style={{width:'100%'}} bodyStyle={{padding:0,margin:0}} cover={
                                  
                                    <div style={{position: 'relative',
                                                textAlign: 'center',
                                                color: 'white'}}>
                                        <img src={item.TitleImg} alt="Snow" style={{width:'100%'}}/>
                                        <div style={{background: 'rgba(0,0,0,0.75)',padding:8, position: 'absolute',bottom: 8,left: 16,}}>
                                          <IconText size="small" type="star" text={180} />&nbsp;&nbsp;
                                          <IconText size="small" type="like" text={200} />&nbsp;&nbsp;
                                          <IconText size="small" type="message" text={160} />&nbsp;
                                        </div>
                                        
                                      </div>
                                    }>
                                  </Card>
                                :null
                                }
                              </Col>
                              <Col>
                                <ListContent data={item} />
                                <ListExtra  data={item} />
                              </Col>
                            </Row>
                           ):(
                             <Row>
                               <Col style={{maxWidth:200}} span={12}>
                               {item.TitleImg != "" ?
                                 <Card bordered={true} bodyStyle={{padding:0,margin:0}} style={{marginRight:10,border:'1px solid #939393'}} cover={
                                    <div style={{position: 'relative',
                                              textAlign: 'center',
                                              color: 'white'}}>
                                      <img src={item.TitleImg} alt="Snow" style={{width:'100%'}}/>
                                      <div style={{width:'100%',textAlign: 'center', background: 'rgba(0,0,0,0.75)', position: 'absolute',bottom: 0}}>
                                        <IconText size="small" type="star" text={180} />&nbsp;&nbsp;
                                        <IconText size="small" type="like" text={200} />&nbsp;&nbsp;
                                        <IconText size="small" type="message" text={160} />&nbsp;
                                      </div>
                                      
                                    </div>
                                  }>
                                 </Card>
                                 :null
                               }
                               </Col>
                               <Col>
                                    <a onClick={()=>{ 
                                        this.props.dispatch(
                                          routerRedux.push(`/topic/${item.Uuid}`)
                                        )}}><h4>{item.Title}</h4></a>
                                    <div style={{height:65}}>
                                      <ListContent data={item} />
                                    </div>
                                    <ListExtra  data={item} />
                               </Col>
                            </Row>
                           )
                    }
                 </Media>
              </List.Item>



              //  <List.Item
              //    key={item.id}
              //    actions={[
              //      <IconText size="small" type="star-o" text={180} />,
              //      <IconText size="small" type="like-o" text={200} />,
              //      <IconText size="small" type="message" text={160} />,
              //    ]}
              //   //  extra={<div className={styles.listItemExtra}/>}
              //  >

              //     {/* <List.Item.Meta
              //      title={(
              //        <a className={styles.listItemMetaTitle} 
              //           onClick={()=>{ 
              //             this.props.dispatch(
              //               routerRedux.push(`/topic/${item.Uuid}`)
              //             )}}><h4>{item.Title}</h4></a>
              //         )}/> */}

              //   <Media query="(max-width: 968px)">
              //           {matches =>
              //             matches ? (
              //               <Row>
              //               <Col span={24}>
              //                 {item.TitleImg != "" ?
              //                   <Card style={{width:'100%'}} bodyStyle={{padding:0,margin:0}} cover={<img alt="example" src={item.TitleImg} />}>
              //                   </Card>
              //                 :null
              //                 }
              //                 <Col  className={styles.listContentCol}>
              //                 <div className={styles.listContent}>
                                
              //                     <a className={styles.listItemMetaTitle} 
              //                       onClick={()=>{ 
              //                         this.props.dispatch(
              //                           routerRedux.push(`/topic/${item.Uuid}`)
              //                         )}}><h4>{item.Title}</h4></a>
                                
              //                   <ListContent data={item} />
              //                   <ListExtra  data={item} />
              //                   </div>
              //                 </Col>
              //               </Col>
              //               </Row>
              //             ) : (
              //               <>
              //               <Row>
              //                 {item.TitleImg != "" ?
              //                 <Col style={{maxWidth:200}} span={12}>
              //                   <Card bordered={true} bodyStyle={{padding:0,margin:0}} style={{marginLeft:10,border:'1px solid #939393'}} cover={<img alt="example" src={item.TitleImg} />}>
              //                   </Card>
              //                 </Col>
              //                   :null
              //                 }
              //                 <Col span={12}>
              //                   <a className={styles.listItemMetaTitle} 
              //                     onClick={()=>{ 
              //                       this.props.dispatch(
              //                         routerRedux.push(`/topic/${item.Uuid}`)
              //                       )}}><h4>{item.Title}</h4></a>
              //                   <ListContent style={{paddingTop:80,paddingLeft:20}} data={item} />
              //                 </Col>
              //               </Row>
              //               {/* <Row>
              //                 <ListExtra style={{paddingTop:80,paddingLeft:20}} data={item} />
              //               </Row> */}
              //               </>
              //             )
              //           }
              //   </Media>
              //  </List.Item>
             )}
           />

  </Card>
      </Col>
      <Col xs={0} sm={0} md={0}lg={6}>
             <div style={{width:'100%', background:'pink' ,height:'100vh'}}>
             abc
             </div>
      </Col>
    </Row>
    
  );
}
}


function isEmpty(o){
  for(var i in o){
      if(o.hasOwnProperty(i)){
          return false;
      }
  }
  return true;
}

function isLink(o){
  var bol = isEmpty(o)
  if(!bol) {
    for(var i in o){
      console.log('i',i)
    }
  }
  return bol
}
// function ImageCard(props) {
//   var epost;

//   if(!isLink(props.post.entityMap)){
//     console.log("isLink",)
//     epost = {
//         "blocks" : [ 
//             {
//                 "key" : "2q2j8",
//                 "text" : "E",
//                 "type" : "atomic",
//                 "depth" : 0.0,
//                 "inlineStyleRanges" : [],
//                 "entityRanges" : [ 
//                     {
//                         "offset" : 0.0,
//                         "length" : 1.0,
//                         "key" : 0.0
//                     }
//                 ],
//                 "data" : {}
//             }
//         ],
//         "entityMap" : props.post.entityMap
//     }
//   }else{
//     var data = props.post.blocks.filter(x => {return x.text ===""})
//     console.log("data...",data.length)
//     if(data.length !== 0) {
//       epost ={
//         "blocks" : [ 
//             {
//                 "inlineStyleRanges" : [],
//                 "entityRanges" : [],
//                 "data" : {
//                     "src" : `${data[0].data.src}`
//                 },
//                 "key" : "2d9r8",
//                 "text" : "",
//                 "type" : "atomic:image",
//                 "depth" : 0.0
//             }, 
//             {
//                 "key" : "d1jep",
//                 "text" : "photo post",
//                 "type" : "unstyled",
//                 "depth" : 0.0,
//                 "inlineStyleRanges" : [],
//                 "entityRanges" : [],
//                 "data" : {}
//             }
//         ],
//         "entityMap" : {}
//       }
//       return (
//         <Editor
//         style={{padding:0}}

//         ref={props.ref}
//         editorState= {createEditorState(epost)}
//         editorEnabled={false}
//         rendererFn={props.rendererFn}
//       />
//       );
//     }
//     return null;
//   }
//   return (
//     <Editor
//       style={{padding:0}}
//       ref={props.ref}
//       editorState= {createEditorState(epost)}
//       editorEnabled={false}
//       rendererFn={props.rendererFn}
//     />
//   );
  

// }


function TagList(props){
  const listItems = props.tag != null? props.tag.map((num) =>
    <Tag><b>{num}</b></Tag>
  ):null;
  if(props.tag != null) {
    return <div>{listItems}</div>
  }else{
    return <div></div>
  }  
}