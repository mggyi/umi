import React from 'react';
import { connect } from 'dva';
import moment from 'moment';
import {Spin, Card,  List, Tag, Icon, Avatar,  Button } from 'antd';
import styles from './topic.less';
import { routerRedux } from 'dva/router';

@connect(({ home,loading}) => ({
  home,
  loading: loading.models.home,
}))
export default class IndexPage extends React.PureComponent{
  componentDidMount(){
    this.props.dispatch({
			type: 'home/fetchTopiclist',
		})
  }

  state = {
    count : 0,
    loading: true,
    loadingMore: false,
    showLoadingMore: true,
    data: [],
  }


  getData = (callback) => {
    // this.setState((prevState) => {
    //   return {count: prevState.count + 1};
    // });

    // console.log(this.state.count)
    // reqwest({
    //   url: fakeDataUrl,
    //   type: 'json',
    //   crossOrigin: true,
    //   method: 'get',
    //   contentType: 'application/json',
    //   success: (res) => {
    //     console.log(res)
    //     callback(res);
    //   },
    // });
  }
  onLoadMore = () => {
    this.setState({
      loadingMore: true,
    });
    this.getData((res) => {
      const data = this.state.data.concat(res.topicList);

      this.setState({
        data,
        loadingMore: false,
      }, () => {
        // Resetting window's offsetTop so as to display react-virtualized demo underfloor.
        // In real scene, you can using public method of react-virtualized:
        // https://stackoverflow.com/questions/46700726/how-to-use-public-method-updateposition-of-react-virtualized
        window.dispatchEvent(new Event('resize'));
      });
    });
  }



render(){
  console.log('this.props',this.props)
  const IconText = ({ type, text }) => (
    <span>
      <Icon type={type} style={{ marginRight: 8 }} />
      {text}
    </span>
  );

  const ListContent = ({ data: {Post, User,content, CreateAt, avatar, owner, href } }) => {
    return(
      <div className={styles.listContent}>
      <div className={styles.description}>{Post}</div>
      <div className={styles.extra}>
        <Avatar src={User[0].avatar} size="small" /><a href={`/users/${User[0].uuid}`}>{User[0].displayName}</a> Published in <a href={href}>{href}</a>
        <em>{moment(CreateAt).format('YYYY-MM-DD HH:mm')}</em>
      </div>
    </div>
    )
  }


  // const loadMore = home.topiclist.length > 0 ? (
  //   <div style={{ textAlign: 'center', marginTop: 16 }}>
  //     <Button onClick={this.fetchMore} style={{ paddingLeft: 48, paddingRight: 48 }}>
  //       {loading ? <span><Icon type="loading" /> Loading...</span> : 'load more'}
  //     </Button>
  //   </div>
  // ) : null;


    const { loading, loadingMore, showLoadingMore, data } = this.props;
    console.log('this.props.home.topiclist',this.props)

    const loadMore = showLoadingMore ? (
      <div style={{ textAlign: 'center', marginTop: 12, height: 32, lineHeight: '32px' }}>
        {loadingMore && <Spin />}
        {!loadingMore && <Button onClick={this.onLoadMore}>loading more</Button>}
      </div>
    ) : null;

  return (
    <Card
      style={{ marginTop: 24 ,paddingTop:20}}
      bordered={true}
      bodyStyle={{ padding: '0' }}>
        <Button icon="plus-circle-o" onClick={()=>{
          this.props.dispatch(routerRedux.push('/newtopic'));
        }}>New Topic</Button>


             <List
             size="large"
             loading={loading}
             rowKey="id"
             itemLayout="vertical"
             loadMore={loadMore}
             dataSource={this.props.home.topiclist}
             renderItem={item => (
               <List.Item
                 key={item.id}
                 actions={[
                   <IconText type="star-o" text={180} />,
                   <IconText type="like-o" text={200} />,
                   <IconText type="message" text={160} />,
                 ]}
                 extra={<div className={styles.listItemExtra} />}
               >
                 <List.Item.Meta
                   title={(
                     <a className={styles.listItemMetaTitle} href={`/topic/${item.Uuid}`}>{item.Title}</a>
                   )}
                   description={<TagList  tag={item.Tag}/>}
                 />
                 <ListContent data={item} />
               </List.Item>
             )}
           />     
  </Card>
  );
}
}


function TagList(props){
  const listItems = props.tag != null? props.tag.map((num) =>
    <Tag>{num}</Tag>
  ):null;
  if(props.tag != null) {
    return <div>{listItems}</div>
  }else{
    return <div></div>
  }  
}