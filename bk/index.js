import React from 'react'
import Topic from './topic'
// import { connect } from 'dva';
import Ask from './ask'
import Unanswered from './unanswered'
import SearchList from './home'


export default class IndexPage extends React.Component {
    constructor(props){
        super(props)
        this.state = {
          tabActiveKey:'topic',
        }
    }

    handleTabChange = key => {
        console.log(key)
        this.setState({
          tabActiveKey: key
        })
    }
    
    render() {
        let ChildrenComponent = Topic;
        if(this.state.tabActiveKey ==='topic') {
            ChildrenComponent = Topic;
        }else if(this.state.tabActiveKey ==='ask'){
            ChildrenComponent = Ask;
        }else {
            ChildrenComponent = Unanswered;
        }
        return (
            <SearchList 
                 tabActiveKey={this.state.tabActiveKey}
                 handleTabChange={this.handleTabChange}>
                <ChildrenComponent/>
            </SearchList>
        )
    }
}