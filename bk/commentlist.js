import React, {Component} from 'react'
import PropTypes from 'prop-types';
import QueueAnim from 'rc-queue-anim';

import {Card,List,Avatar, Button, Spin,Collapse,Row,Col} from 'antd'
import { connect } from 'dva';
import {
    Editor,
    Block,
    createEditorState,
    rendererFn,
} from 'medium-draft';
import {
    setRenderOptions,
    blockToHTML,
    entityToHTML,
    styleToHTML,
  } from 'medium-draft/lib/exporter';
import 'draft-js/dist/Draft.css';
import 'medium-draft/lib/index.css';
import Moment from 'react-moment';


const Panel = Collapse.Panel;
const AtomicSeparatorComponent = (props) => (<hr />);
  
const AtomicBlock = (props) => {
  const { blockProps, block } = props;
  const content = blockProps.getEditorState().getCurrentContent();
  const entity = content.getEntity(block.getEntityAt(0));
  const data = entity.getData();
  const type = entity.getType();
  if (blockProps.components[type]) {
    const AtComponent = blockProps.components[type];
    return (
      <div className={`md-block-atomic-wrapper md-block-atomic-wrapper-${type}`}>
        <AtComponent data={data} />
      </div>
    );
  }
  return <p>Block of type <b>{type}</b> is not supported.</p>;
};
  
 
class AtomicEmbedComponent extends React.Component {
  static propTypes = {
    data: PropTypes.object.isRequired,
  }

  constructor(props) {
    super(props);

    this.state = {
      showIframe: false,
    };

    this.enablePreview = this.enablePreview.bind(this);
  }

  componentDidMount() {
    this.renderEmbedly();
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.showIframe !== this.state.showIframe && this.state.showIframe === true) {
      this.renderEmbedly();
    }
  }

  getScript() {
    const script = document.createElement('script');
    script.async = 1;
    script.src = '//cdn.embedly.com/widgets/platform.js';
    script.onload = () => {
      window.embedly();
    };
    document.body.appendChild(script);
  }

  renderEmbedly() {
    if (window.embedly) {
      window.embedly();
    } else {
      this.getScript();
    }
  }

  enablePreview() {
    this.setState({
      showIframe: true,
    });
  }
  // 
// Embedded ― ${url}
  render() {
    const { url } = this.props.data;
    const innerHTML = 
    `<div><a class="embedly-card" href="${url}" 
    data-card-controls="0" data-card-theme="dark"
    >
    <photo class="shine"></photo>
    </a></div>`;
    return (
      <div className="md-block-atomic-embed">
        <div dangerouslySetInnerHTML={{ __html: innerHTML }} />
      </div>
    );
  }
}

@connect(({ topic,loading }) => ({
    topic,
    loading: loading.effects['topic/fetchComment'],
}))
export default class CommentList extends Component {
    state = {
        count : 1,
        loadingMore: false,
        showLoadingMore: true,
        data: [],
        commentBoxShow: false,
    }
    onLoadMore = () => {
        this.setState({
          loadingMore: true,
        });
        this.setState((prevState) => {
            return {count: prevState.count + 1};
        });
        this.props.dispatch({
            type: 'topic/fetchComment',
            payload: {
                postId : this.props.postId,
                query : this.state.count * 3
            }
        })
        this.setState({
            loadingMore: false,
          });
      }

    componentDidMount(){
        this.setState((prevState) => {
            return {count: prevState.count + 1};
        });
        this.props.dispatch({
            type: 'topic/fetchComment',
            payload: {
                postId : this.props.postId,
                query : 3
            }
        })

    }

  rendererFn(setEditorState, getEditorState) {
    const atomicRenderers = {
      embed: AtomicEmbedComponent,
      separator: AtomicSeparatorComponent,
    };
    const rFnOld = rendererFn(setEditorState, getEditorState);
    const rFnNew = (contentBlock) => {
      const type = contentBlock.getType();
      switch(type) {
        case Block.ATOMIC:
          return {
            component: AtomicBlock,
            editable: false,
            props: {
              components: atomicRenderers,
              getEditorState,
            },
          };
        default: return rFnOld(contentBlock);
      }
    };
    return rFnNew;
  }

  replyOnClick = () => {
    this.setState({
      commentBoxShow: !this.state.commentBoxShow
    });    
}

    render () {
        const {loading,topic:{Comment=[]}} = this.props;
        const {  loadingMore, showLoadingMore, data } = this.state;
        const loadMore = showLoadingMore ? (
          <div style={{ textAlign: 'center', marginTop: 12, height: 32, lineHeight: '32px' }}>
            {loadingMore && <Spin />}
            {!loadingMore && <Button size="small" onClick={this.onLoadMore}>more</Button>}
          </div>
        ) : null;
        console.log('commentList123',Comment,loading)
        return(
            <Card bodyStyle={{padding:10,margin:0}} style={{width:'100%',marginTop:10,marginBottom:10,borderTop:'3px solid red',borderLeft:'none',borderRight:'none',borderBottom:'none'}}>
                {Comment!== null ?[
              
                     <List
                        className="demo-loadmore-list"
                        loading={loading}
                        itemLayout="horizontal"
                        loadMore={loadMore}
                        dataSource={Comment}
                        renderItem={item => (
                          <>
                              <Card  style={{borderTop:'none',borderRight:'none',borderLeft:'none',borderBottom:'1px solid #d9d9d9'}}>
                                  <Card.Meta
                                      avatar={<Avatar shape="square" size="small" src={item.User[0].avatar} />}
                                      title={
                                        <span>
                                          {item.User[0].displayName}
                                          <small style={{paddingLeft:5,color:'#bfbfbf'}}>
                                            <Moment fromNow ago>{item.CreateAt}</Moment> ago
                                          </small>
                                        </span>                                      
                                      }
                                      description={
                                      <Editor
                                          placeholder=""
                                          ref={(e) => {this._editor = e;}}
                                          editorState={createEditorState(item.Comment)}
                                          editorEnabled={false}
                                          rendererFn={this.rendererFn}
                                      />}
                                      />
                                      <Row  type="flex" justify="end">
                                        <Col>
                                          <a onClick={this.replyOnClick}>reply</a>
                                        </Col>
                                      </Row>

                                    
                                    <QueueAnim className="demo-content">
                                      {this.state.commentBoxShow ? [
                                        <div className="demo-thead" key="a">
                                          {/* {this.context.login?<Comment  postId={this.props.match.params.id}/> :null} */}
                                          <h1>Hello1</h1>
                                        </div>,
                                        <div className="demo-thead" key="b">
                                          {/* <CommentList postId={this.props.match.params.id}/> */}
                                          <h1>Hello2</h1>
                                        </div>
                                      ] : null}
                                    </QueueAnim>





                              </Card>
                            </>
                        )}
                    />

                ]:[null]}
            </Card>
        )
    }
}
