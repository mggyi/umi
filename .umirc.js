export default {
  "copy": [
    {
      "from": "_redirects",
      "to": "../",
    },
    {
      "from": "googlef07ce0d0e8b8e940.html",
      "to": "../",
    },
    {
      "from": "robots.txt",
      "to": "../",
    },
    {
      "from": "sitemap.xml",
      "to": "../",
    }
  ],
  plugins: [
    'umi-plugin-dva',
    ['umi-plugin-routes', {
      exclude: [
        /models/,
        /services/,
      ],
    }],
  ],
  
  pages: {
    '/list': { Route: '/src/routes/PrivateRoute.js' },
    '/profile': { Route: '/src/routes/PrivateRoute.js' },
    '/chgpswd': { Route: '/src/routes/PrivateRoute.js'},
    '/newtopic' : { Route: '/src/routes/PrivateRoute.js'}
  },
 }



